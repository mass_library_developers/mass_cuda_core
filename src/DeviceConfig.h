#pragma once

#include <map>
#include <any>
#include <cassert>
#include <unordered_set>
#include "iostream"

#include "cudaUtil.h"
#include "Logger.h"
#include "MassException.h"
#include "CudaEventTimer.h"
#include "Place.h"
#include "Agent.h"

namespace mass
{

	// forward declaration
	class Place;
	class Agent;

	// PlaceArray stores place pointers and
	struct PlaceArray
	{
		int qty;   // Number of Place objects
		int nDims; // Number of dimensions of the Places array
		int *dims; // Size of each dimension of the Places array

		dim3 kernelDims[2]; // Block and thread dimensions
		int majorType;		// Major type of the Place objects, ROW or COL

		// Place V2 attributes
		Place *d_place;			   // Pointer to the Place array on GPU
		int *d_attributeTags;	   // Pointer to the attribute tags array on GPU
		void **d_attributeDevPtrs; // Pointer to the attribute pointers array on GPU
		size_t *d_attributePitch;  // Pointer to the attribute pitch array on GPU
		size_t nAttributes;		   // Number of attributes
	};

	// AgentArray stores agent pointers and state pointers on GPU
	struct AgentArray
	{
		int placesHandle; // Handle of the Places array that the Agents will be mapped to
		int nLiveAgents;  // Number of live Agent objects
		int nMaxAgents;	  // Number of max Agent objects
		dim3 dims[2];	  // Block and thread dimensions

		// Agent V2 attributes
		Agent *d_agent;			   // Pointer to the Agent array on GPU
		int *d_attributeTags;	   // Pointer to the attribute tags array on GPU
		void **d_attributeDevPtrs; // Pointer to the attribute pointers array on GPU
		size_t *d_attributePitch;  // Pointer to the attribute pitch array on GPU
		size_t nAttributes;		   // Number of attributes
	};

	/**
	 *  This class represents a GPU.
	 */
	class DeviceConfig
	{
		friend class Dispatcher;

	public:
		DeviceConfig();
		DeviceConfig(int device);

		virtual ~DeviceConfig();
		void freeDevice();

		/**
		 * @brief Load data from host to device.
		 *
		 * @param destination The pointer to the destination on device.
		 * @param source The pointer to the source on host.
		 * @param bytes The number of bytes to be copied.
		 */
		void load(void *&destination, const void *source, size_t bytes);

		/**
		 * @brief Unload data from device to host.
		 *
		 * @param destination The pointer to the destination on host.
		 * @param source The pointer to the source on device.
		 * @param bytes The number of bytes to be copied.
		 */
		void unload(void *destination, void *source, size_t bytes);

		/**
		 * @brief Return the pointer to the device Places array.
		 *
		 * @param handle The handle of the device Places array.
		 *
		 * @return The pointer to the device Places array.
		 */
		Place *getDevPlaces(int handle);

		/**
		 * @brief Return the number of Places in the device Places array.
		 *
		 * @param handle The handle of the device Places array.
		 *
		 * @return The number of Place in the device Places array.
		 */
		int countDevPlaces(int handle);

		/**
		 * @brief Return the number of dimensions of the Place collection.
		 *
		 * @param handle The handle of the device Places array.
		 *
		 * @return The number of dimensions of the Place collection.
		 */
		int getPlacesNumDims(int handle);

		/**
		 * @brief Return the size of each dimension of the Place collection.
		 *
		 * @param handle The handle of the device Places array.
		 *
		 * @return The size of each dimension of the Place collection.
		 */
		int *getPlacesDims(int handle);

		/**
		 * @brief Return the block and thread dimentions for the Place collection.
		 *
		 * @param handle The handle of the device Places array.
		 *
		 * @return The block and thread dimentions for the Place collection.
		 */
		dim3 *getPlacesKernelDims(int handle);

		/**
		 * @brief Return the major type of the Place collection.
		 *
		 * @param handle The handle of the device Places array.
		 *
		 * @return The major type of the Place collection.
		 */
		int getPlaceMajorType(int handle);

		/**
		 * @brief Return the pointer to the device Agents array.
		 *
		 * @param handle The handle of the device Agents array.
		 *
		 * @return The pointer to the device Agents array.
		 */
		Agent *getDevAgents(int handle);

		/**
		 * @brief Get the number of alive agents in the agent array.
		 *
		 * @param handle The handle of the agent array.
		 *
		 * @return The number of alive agents in the agent array.
		 */
		int countAliveAgents(int handle);

		/**
		 * @brief Get the user-defined maximum number of agents.
		 *
		 * @param handle The handle of the agent array.
		 *
		 * @return The user-defined maximum number of agents.
		 */
		int getMaxAgents(int handle);

		/**
		 * @brief Returns block and thread dimentions for the Agent collection
		 * based on the total number of elements beloging to the collection.
		 *
		 * @param handle The handle of the agent array.
		 *
		 * @return The block and thread dimentions for the Agent collection
		 * (currently using 1D dimention only).
		 */
		dim3 *getAgentsKernelDims(int agentHandle);

		template <typename PlaceType>
		/**
		 * @brief Instantiate Place objects (a Places Array) on GPU.
		 *
		 * @tparam PlaceType The type of Place object to be instantiated.
		 * @tparam PlaceStateType The type of PlaceState object to be instantiated.
		 * @param handle The handle of the Places array.
		 * @param dimensions The number of dimensions of the Places array.
		 * @param size The size of each dimension of the Places array.
		 * @param qty The number of Place objects to be instantiated.
		 * @param majorType The major type of the Places array, ROW or COL.
		 *
		 * @return The pointer to the Places array on GPU.
		 */
		void instantiatePlaces(int handle, int dimensions, int size[], int qty, int majorType);

		template <typename AgentType>
		/**
		 * @brief Instantiate Agent objects (an Agent Array) on GPU.
		 *
		 * @tparam AgentType The type of Agent object to be instantiated.
		 * @param handle The handle of the Agent array.
		 * @param nAgents The number of Agent objects to be instantiated.
		 * @param placesHandle The handle of the Places array that the Agents will
		 * be mapped to.
		 * @param maxAgents The maximum number of Agent objects to be spawned in
		 * the future.
		 *
		 * @return The pointer to the Agent array on GPU.
		 */
		void instantiateAgents(int handle, int nAgents, int placesHandle, int maxAgents);

		std::function<void()> cleanUpFunction;

		/**
		 * @brief Create CUDA streams for the device
		 * to enable concurrent kernel execution.
		 * (Currently is not used)
		 *
		 * @param nStreams The number of CUDA streams to be created.
		 */
		void createCudaStreams(int nStreams);

		/**
		 * @brief Destroy all CUDA streams for the device.
		 */
		void destroyCudaStreams();

		/**
		 * Used to determine the type of the object
		 * that the function is being called on.
		 * Because we have functions that are used for both
		 * Places and Agents.
		 */
		enum ObjectType
		{
			PLACE,
			AGENT
		};

		/**
		 * Update all pointers related to Object attributes to device. This function
		 * must be called after user set customized attributes for Object objects.
		 * When user set customized attributes for Object objects, the pointers to
		 * attributes related arrays are stored on the host locally, this function
		 * is used to update these pointers to device.
		 *
		 * @param object The type of the object, which can be either Places or Agents.
		 * @param handle The handle of the Object instance.
		 * @param d_attributeTags The pointer to the attribute tags array on device.
		 * @param d_attributeDevPtrs The pointer to the attribute pointers array on device.
		 * @param d_attributePitch The pointer to the attribute pitch array on device.
		 * @param nAttributes The number of attributes.
		 */
		void finalizeAttributes(ObjectType object, int handle, int *d_attributeTags, void **d_attributeDevPtrs,
								size_t *d_attributePitch, size_t nAttributes);

		/**
		 * Determine if the attributes an Object instance has been finalized.
		 *
		 * @param object The type of the object, which can be either Places or Agents.
		 * @param handle The handle of the Places instance.
		 */
		bool isAttributesFinalized(ObjectType object, int handle);

		/**
		 * Free all attributes related pointers for an Object instance.
		 *
		 * @param object The type of the object, which can be either Places or Agents.
		 * @param handle The handle of the Places instance.
		 */
		void clearAttributes(ObjectType object, int handle);

	private:
		int deviceNum;
		std::map<int, PlaceArray> devPlacesMap; // map of Place arrays on device
		std::map<int, AgentArray> devAgentsMap; // map of Agent arrays on device

		// Vector to store created CUDA streams
		std::vector<cudaStream_t> cudaStreams;

		size_t freeMem;
		size_t allMem;

		void deletePlaces(int handle);
		void deleteAgents(int handle);
	};
	// end class

	/**
	 * @brief Helper function to get the ideal block and thread dimensions
	 * based on the number of elements to be processed.
	 *
	 * @param objectType The type of the object, which can be either Places or Agents.
	 * @param qty The number of elements to be processed.
	 *
	 * @return A pair of block (first) and thread dimensions (second).
	 */
	inline __host__ std::pair<dim3, dim3> getIdealDims(DeviceConfig::ObjectType objectType, int qty)
	{
		int numBlocks = 0, numThreads = 0;

		// If user has defined the block size, use it
		if (objectType == DeviceConfig::AGENT)
		{
#ifdef AGENT_BLOCK_SIZE
			numThreads = AGENT_BLOCK_SIZE;
#endif
		}
		else if (objectType == DeviceConfig::PLACE)
		{
#ifdef PLACE_BLOCK_SIZE
			numThreads = PLACE_BLOCK_SIZE;
#endif
		}

		// If the block size is not defined, calculate the ideal block size
		if (numThreads == 0)
		{
			// If the total number of elements is lte the minimum block size
			if (qty <= MIN_BLOCK_SIZE)
			{
				// Set the number of threads to the temp ideal number of threads
				numThreads = MIN_BLOCK_SIZE;
			}
			// If the total number of elements is greater than the
			// number of minimum multiprocessors * maximum block size
			// Reason:
			// At least 1 block per multiprocessor on a total of
			// 64 multiprocessors is recommended by NVIDIA
			else if (qty >= MIN_MULTIPROCESSORS * MAX_BLOCK_SIZE)
			{
				// Set the number of threads to the maximum block size
				numThreads = MAX_BLOCK_SIZE;
			}
			// Otherwise, set the number of threads to the ideal block size
			// The ideal block size is roughly set
			// user should test and adjust it for their specific use case
			else
			{
				if (qty > MAX_BLOCK_SIZE)
				{
					numThreads = MAX_BLOCK_SIZE;
				}
				else
				{
					// Set the number of threads to the ideal block size
					numThreads = IDEAL_BLOCK_SIZE;
				}
			}
		}

		// Get the ideal number of blocks
		numBlocks = qty / numThreads;
		if (qty % numThreads != 0)
		{
			++numBlocks;
		}

		// Set the dimensions
		dim3 threadDim(numThreads);
		dim3 blockDim(numBlocks);

		return std::make_pair(blockDim, threadDim);
	}

	inline void getRandomPlaceIdxs(int idxs[], int nPlaces, int nAgents)
	{
		// int curAllocated = 0;

		// if (nAgents > nPlaces) {
		// 	for (int i=0; i<nPlaces; i++) {
		// 		for (int j=0; j<nAgents/nPlaces; j++) {
		// 			idxs[curAllocated] = i;
		// 			curAllocated ++;
		// 		}
		// 	}
		// }

		// // Allocate the remaining agents randomly:
		// std::unordered_set<int> occupied_places;

		// while (curAllocated < nAgents) {
		// 	unsigned int randPlace = rand() % nPlaces; //random number from 0 to nPlaces
		// 	if (occupied_places.count(randPlace)==0) {
		// 		occupied_places.insert(randPlace);
		// 		idxs[curAllocated] = randPlace;
		// 		curAllocated++;
		// 	}
		// }

		/*
		Above is the old code that was used to allocate agents randomly. However, upon
		reviewing the design documentation, the agents should be distributed uniformly
		over GPUs/Places. Therefore, the new code is below:
		We will now simply distribute agents to all places as evenly as possible.
		This not only simplifies the code, but also makes it more efficient.
		*/
		int placeLocationIdx = 0;

		for (int i = 0; i < nAgents; i++)
		{
			idxs[i] = placeLocationIdx;
			placeLocationIdx = (placeLocationIdx + 1) % nPlaces;
		}
	}

	template <typename PlaceType>
	__global__ void instantiatePlaceArrayKernel(Place *places, int qty)
	{
		unsigned idx = getGlobalIdx_1D_1D();

		if (idx < qty)
		{
			new (&places[idx]) PlaceType(idx);
		}
	}

	template <typename PlaceType>
	void DeviceConfig::instantiatePlaces(int handle, int dimensions, int size[], int qty, int mayjorType)
	{

		logger::debug("Entering DeviceConfig::instantiatePlaces");

		if (devPlacesMap.count(handle) > 0)
		{
			throw MassException("The handle is already taken");
			// return nullptr;
		}

		// create places tracking
		PlaceArray p;
		p.d_attributeTags = nullptr;
		p.d_attributeDevPtrs = nullptr;
		p.d_attributePitch = nullptr;

		p.qty = qty; // size
		p.majorType = mayjorType;

		// Save the number of dimensions and the size of each dimension
		p.nDims = dimensions;
		p.dims = new int[dimensions];
		for (int i = 0; i < dimensions; i++)
		{
			p.dims[i] = size[i];
		}

		// Upload the qty into the device
		cudaMemcpyToSymbol(nPlaces, &qty, sizeof(int));
		CHECK();

		// Calculate the kernel dimensions
		std::pair<dim3, dim3> dims = getIdealDims(PLACE, qty);
		dim3 blockDim = dims.first;
		dim3 threadDim = dims.second;
		p.kernelDims[0] = blockDim;
		p.kernelDims[1] = threadDim;

		std::unique_ptr<CudaEventTimer> timer = std::unique_ptr<CudaEventTimer>(new CudaEventTimer());
		timer->startTimer();

		// Initialize the Place array on device
		Place *d_place = nullptr;
		CATCH(cudaMalloc((void **)&d_place, qty * sizeof(PlaceType)));
		instantiatePlaceArrayKernel<PlaceType><<<blockDim, threadDim>>>(d_place, qty);
		CHECK();

		timer->stopTimer();
		logger::debug("Time to allocate memory for places: %f ms", timer->getElapsedTime());
		logger::debug("Finished instantiation kernel");

		p.d_place = d_place;
		devPlacesMap[handle] = p;
		logger::debug("Finished DeviceConfig::instantiatePlaces");
	}

	template <typename AgentType>
	/**
	 * @brief The kernel function to instantiate Agent objects on GPU.
	 *
	 * It first spawns the number of agents specified by nAgents stored
	 * in agents array, then it creates placeholder objects
	 * for future agent spawning (if maxAgents > nAgents).
	 *
	 * @tparam AgentType The type of Agent object to be instantiated.
	 * @param agents The array to store Agent objects.
	 * @param nAgents The number of agents to be instantiated.
	 * @param maxAgents The maximum number of agents to be instantiated and
	 * 				to be spawned in the future.
	 * @param nPlaces The number of Places in the Places array on the device.
	 */
	__global__ void instantiateAgentsKernel(Agent *agents, int nAgents, int maxAgents,
											Place *places, int nPlaces)
	{
		unsigned idx = getGlobalIdx_1D_1D();

		if (idx < maxAgents)
		{
			new (&agents[idx]) AgentType(idx);
		}
	}

	template <typename AgentType>
	void DeviceConfig::instantiateAgents(int handle, int nAgents, int placesHandle, int maxAgents)
	{
		logger::debug("Entering DeviceConfig::instantiateAgents");

		// If the handle is already taken, return nullptr
		if (devAgentsMap.count(handle) > 0)
		{
			throw MassException("The handle is already taken");
			// return nullptr;
		}

		// Create places tracking
		AgentArray a;
		a.placesHandle = placesHandle;
		a.d_attributeTags = nullptr;
		a.d_attributeDevPtrs = nullptr;
		a.d_attributePitch = nullptr;
		a.nLiveAgents = nAgents; // Size
		// If the user did not specify the maximum number of agents,
		// we allocate twice the number of Agent as the maximum Agents
		if (maxAgents == 0)
		{
			a.nMaxAgents = nAgents * 2;
		}
		else
		{
			a.nMaxAgents = maxAgents;
		}

		// Get kernel dimensions
		std::pair<dim3, dim3> dims = getIdealDims(AGENT, a.nMaxAgents);
		dim3 blockDim = dims.first;
		dim3 threadDim = dims.second;
		a.dims[0] = blockDim;
		a.dims[1] = threadDim;

		// Initialize the Agent array on device
		Agent *d_agent = nullptr;
		CATCH(cudaMalloc((void **)&d_agent, a.nMaxAgents * sizeof(AgentType)));
		Place *places = getDevPlaces(placesHandle);
		int nPlaces = countDevPlaces(placesHandle);

		logger::debug("Launching Agent instantiation kernel");
		instantiateAgentsKernel<AgentType><<<blockDim, threadDim>>>(d_agent, nAgents, a.nMaxAgents, places, nPlaces);
		CHECK();
		logger::debug("Finished Agent instantiation kernel");

		a.d_agent = d_agent;
		devAgentsMap[handle] = a;
		logger::debug("Finished DeviceConfig::instantiateAgents");
	}

} // end namespace
