
#pragma once

#include <iostream>
#include <fstream> // ofstream
#include <stddef.h>
#include <map>

#include "Dispatcher.h"
#include "Places.h"
#include "Place.h"
#include "Agents.h"
#include "Agent.h"
#include "CudaEventTimer.h"
#include "PlaceAttributes.h"
#include "AgentAttributes.h"

namespace mass
{

	class Mass
	{
		friend class Places;
		friend class Agents;

	public:
		/**
		 *  Initializes the MASS environment using the GPU resource with
		 *  the highest compute capability discovered on the machine.
		 *  Must be called prior to all other MASS methods.
		 */
		static void init();

		/**
		 *  Shuts down the MASS environment, releasing all resources.
		 */
		static void finish();

		/**
		 *  Gets the places object for this handle.
		 *  @param handle an int that uniquely identifies a places collection.
		 *  @return NULL if not found.
		 */
		static Places *getPlaces(int handle);

		/**
		 *  Gets the number of Places collections in this simulation.
		 */
		static int getNumPlaces();

		/**
		 * Creates a Places instance with the provided parameters.
		 *
		 * @param handle The unique handle for this Places collection.
		 * @param dimensions The number of dimensions of the grid of places.
		 * @param size The size of the grid of places.
		 * @param majorType The major type of the grid of places.
		 * It can be either `Place::ROW_MAJOR` or `Place::COL_MAJOR`.
		 * This  will be used to calculate the offset from relative
		 * coordinates when updating neighbors of a place.
		 *
		 * @return The Places instance created.
		 */
		template <typename PlaceType>
		static Places *createPlaces(int handle, int dimensions, int size[], int majorType);
		/**
		 * Creates a Places instance with the provided parameters,
		 * using the default major type `Place::COL_MAJOR`.
		 *
		 * @param handle The unique handle for this Places collection.
		 * @param dimensions The number of dimensions of the grid of places.
		 * @param size The size of the grid of places.
		 *
		 * @return The Places instance created.
		 */
		template <typename PlaceType>
		static Places *createPlaces(int handle, int dimensions, int size[])
		{
			return createPlaces<PlaceType>(
				handle, dimensions, size, Place::MemoryOrder::COL_MAJOR);
		}

		/**
		 * Creates a Agents instance with the provided parameters.
		  @param argument is the argument passed to each Agents constructor fucntion.
				It should be a void * to a contiguos space of arguments.
		  @param argSize is the size of the contiguous space of arguments specified in argument
		  @param nAgents is the initial number of agents instantiated in the system
		  @param placesHandle is the handle of the Places collection over which to instantiate the Agents collection.
		  @param maxAgents is the maximum number of agents that can be present in the system thoughout the whole simulation.
				If the maxAgents parameter is omitted the library set it to the default value of nAgents*2.
		  @param placeIdxs is the array of size nAgents, specifying the index of a place for where each of the Agents
				should be instantiated. If placeIdxs parameter is omitted, the libary randomly distributes agents over the grid of places.
		 */
		template <typename AgentType>
		static Agents *createAgents(int handle, int nAgents, int placesHandle, int maxAgents = 0, int *placeIdxs = NULL);

		/**
		 * Creates an Agents instance with the provided parameters.
		 */
		template <typename AgentType>
		static Agents *createAgents(int handle, int nAgents, Places *places, int maxAgents = 0, int *placeIdxs = NULL);

	private:
		static std::map<int, Places *> placesMap;
		static std::map<int, Agents *> agentsMap;
		static Dispatcher *dispatcher; /**< The object that handles communication with the GPU(s). */
	};

	template <typename P>
	Places *Mass::createPlaces(int handle, int dimensions, int size[], int majorType)
	{
		logger::debug("Entering Mass::createPlaces");
		if (dimensions != 2)
		{
			logger::warn("The current version of MASS CUDA only supports the 2D dimensionality");
		}
		// create an API object for this Places collection
		Places *places = new Places(handle, dimensions, size, dispatcher);
		placesMap[handle] = places;
		logger::debug("Created Places API with handle: %d", handle);

		// perform actual instantiation of user classes
		dispatcher->instantiatePlaces<P>(handle, dimensions, size, places->numElements, majorType);
		logger::debug("Instantiated device Places with handle: %d", handle);

		// Set out pre-defined attributes
		places->setAttribute<int>(PlacePreDefinedAttr::NEIGHBORS, MAX_NEIGHBORS, -1);
		places->setAttribute<Place *>(PlacePreDefinedAttr::NEIGHBOR_PTRS, MAX_NEIGHBORS, nullptr);
		places->setAttribute<int>(PlacePreDefinedAttr::NEIGHBOR_OFFSETS, MAX_NEIGHBORS, 0);
		places->setAttribute<Agent *>(PlacePreDefinedAttr::AGENTS, MAX_AGENTS, nullptr);
		places->setAttribute<size_t>(PlacePreDefinedAttr::AGENT_POPS, 1, 0);
		places->setAttribute<Agent *>(PlacePreDefinedAttr::POTENTIAL_AGENTS, MAX_POTENTIAL_AGENTS, nullptr);
		places->setAttribute<size_t>(PlacePreDefinedAttr::POTENTIAL_AGENT_POPS, 1, 0);
		logger::debug("Set pre-defined attributes for Places API with handle: %d", handle);

		places->finalizeAttributes();
		logger::debug("Finalized attributes for Places API with handle: %d", handle);

		logger::debug("Exiting Mass::createPlaces");
		return places;
	}

	template <typename AgentType>
	Agents *Mass::createAgents(int handle, int nAgents, int placesHandle, int maxAgents, int *placeIdxs)
	{
		logger::debug("Entering Mass::createAgents");

		// create an API object for this Agents collection
		Agents *agents = new Agents(handle, nAgents, maxAgents, dispatcher, placesHandle);
		agentsMap[handle] = agents;
		logger::debug("Created Agents API with handle: %d", handle);

		// perform actual instantiation of user classes
		dispatcher->instantiateAgents<AgentType>(handle, nAgents, placesHandle, maxAgents);
		logger::debug("Instantiated device Agents with handle: %d", handle);

		// Set out pre-defined attributes
		agents->setAttribute<Place *>(AgentPreDefinedAttr::RESIDE_PLACE, 1, nullptr);
		agents->setAttribute<bool>(AgentPreDefinedAttr::IS_ALIVE, 1, false);
		agents->setAttribute<Place *>(AgentPreDefinedAttr::DEST_PLACE, 1, nullptr);
		agents->setAttribute<int>(AgentPreDefinedAttr::N_CHILDREN, 1, 0);
		agents->setAttribute<Place *>(AgentPreDefinedAttr::CHILDREN_DEST_PLACE, 1, nullptr);
		logger::debug("Set pre-defined attributes for Agents API with handle: %d", handle);

		agents->finalizeAttributes();
		logger::debug("Finalized attributes for Agents API with handle: %d", handle);

		// Agents require an extra step: linking them to their residing places,
		// and set to alive.
		// We cannot perform this step in the instantiateAgents method because
		// the attributes of the Places collection may not have been finalized yet.
		agents->initializePreDefinedAttributes(placeIdxs);
		logger::debug("Initialized pre-defined attributes for Agents API with handle: %d", handle);

		logger::debug("Exiting Mass::createAgents");
		return agents;
	}

	template <typename AgentType>
	Agents *Mass::createAgents(int handle, int nAgents, Places *places, int maxAgents, int *placeIdxs)
	{
		int placesHandle = places->getHandle();
		return createAgents<AgentType>(handle, nAgents, placesHandle, maxAgents, placeIdxs);
	}

} /* namespace mass */
