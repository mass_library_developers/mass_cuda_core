

#include <stdarg.h>
#include "Places.h"

using namespace std;

namespace mass
{

	Places::~Places()
	{
		delete[] dimensions;

		// Creat the dev pointers for the attributes
        for (int i = 0; i < attributeTags.size(); i++)
        {
            CATCH(cudaFree(attributeDevPtrs[i]));
        }
	}

	int Places::getDimensions()
	{
		return numDims;
	}

	int *Places::getSize()
	{
		return dimensions;
	}

	int Places::getNumPlaces()
	{
		return numElements;
	}

	int Places::getHandle()
	{
		return handle;
	}

	void Places::callAll(int functionId, void *argument, int argSize)
	{
		logger::debug(
			"Entering callAll(int functionId, void *argument, int argSize)");
		dispatcher->callAllPlaces(handle, functionId, argument, argSize);
	}

	void Places::exchangeAll(std::vector<int *> *destinations)
	{
		dispatcher->exchangeAllPlaces(handle, destinations);
	}

	void Places::exchangeAll(std::vector<int *> *destinations, int functionId, void *argument, int argSize)
	{
		dispatcher->exchangeAllPlaces(handle, destinations, functionId, argument, argSize);
	}

	void Places::exchangeAll(int nNeighbors)
	{
		dispatcher->exchangeAllPlaces(handle, nNeighbors);
	}

	void Places::exchangeAll(int nNeighbors, int functionId, void *argument, int argSize)
	{
		dispatcher->exchangeAllPlaces(handle, nNeighbors, functionId, argument, argSize);
	}

	Place *Places::getDevPointer()
	{
		return dispatcher->getPlacesDevPtr(handle);
	}

	int Places::getRowMajorIdx(int *indices)
	{
		// a single X will pass over y*z elements,
		// a single Y will pass over z elements, and a Z will pass over 1 element.
		// each dimension will be removed from numElements before calculating the
		// size of each index's "step"
		int multiplier = (int)numElements;
		int rmi = 0; // accumulater for row major index

		for (int i = 0; i < numDims; i++)
		{
			multiplier /= dimensions[i]; // remove dimension from multiplier
			int idx = indices[i];		 // get an index and check validity
			if (idx < 0 || idx >= dimensions[i])
			{
				throw MassException("The indices provided are out of bounds");
			}
			rmi += multiplier * idx; // calculate step
		}

		return rmi;
	}

	vector<int> Places::getIndexVector(int rowMajorIdx)
	{
		vector<int> indices; // return value

		for (int i = numDims - 1; i >= 0; --i)
		{
			int idx = rowMajorIdx % dimensions[i];
			rowMajorIdx /= dimensions[i];
			indices.insert(indices.begin(), idx);
		}

		return indices;
	}

	int Places::relativeIndexToOffset(int *relativeIndex, int numDimensions, int *dimensions, int majorType)
	{
		// Check that the dimension of the relative index is the same as the dimension of the place
		if (relativeIndex == NULL || dimensions == NULL || numDimensions == 0)
		{
			throw MassException("The relative index or dimensions array is NULL or the number of dimensions is 0");
		}
		// Calculate the offset
		if (majorType == Place::MemoryOrder::ROW_MAJOR)
		{
			int offset = 0;
			int product = 1;
			for (int i = numDimensions - 1; i >= 0; i--)
			{
				offset += relativeIndex[i] * product;
				if (i > 0)
				{
					product *= dimensions[i - 1];
				}
			}
			return offset;
		}
		else if (majorType == Place::MemoryOrder::COL_MAJOR)
		{
			int offset = 0;
			int product = 1;
			for (int i = 0; i < numDimensions; i++)
			{
				offset += relativeIndex[i] * product;
				if (i < numDimensions - 1)
				{
					product *= dimensions[i + 1];
				}
			}
			return offset;
		}
		else
		{
			throw MassException("The major type is not supported, \
			please use either Place::MemoryOrder::ROW_MAJOR or Place::MemoryOrder::COL_MAJOR");
		}
	}

	const std::vector<int> &Places::getAttributeTags() const
	{
		return attributeTags;
	}

	const std::vector<void *> &Places::getAttributeDevPtrs() const
	{
		return attributeDevPtrs;
	}

	const std::vector<size_t> &Places::getAttributePitch() const
	{
		return attributePitch;
	}

	void Places::finalizeAttributes()
	{
		this->dispatcher->finalizeAttributes(
			DeviceConfig::ObjectType::PLACE, 
			handle, &attributeTags, &attributeDevPtrs, &attributePitch);
	}

	Places::Places(int handle, int dimensions, int size[], Dispatcher *d)
	{
		this->handle = handle;
		this->dispatcher = d;
		this->dimensions = size;
		this->numDims = dimensions;
		this->numElements = 1;
		for (int i = 0; i < dimensions; ++i)
		{
			this->numElements *= size[i];
		}
	}

} /* namespace mass */
