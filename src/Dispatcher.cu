#include <sstream>
#include <algorithm> // array compare
#include <iterator>

#include "Dispatcher.h"
#include "cudaUtil.h"
#include "Logger.h"
#include "MassException.h"

#include <thrust/device_ptr.h>
#include <thrust/sort.h>

#include "DeviceConfig.h"
#include "Place.h"
#include "Places.h"

// using constant memory to optimize the performance of exchangeAllPlacesKernel():
__constant__ int offsets_device[MAX_NEIGHBORS];

using namespace std;

namespace mass
{
    __global__ void callAllPlacesKernel(Place *ptrs, int nptrs, int functionId,
                                        void *argPtr)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < nptrs)
        {
            ptrs[idx].callMethod(functionId, argPtr);
        }
    }

    __global__ void callAllAgentsKernel(Agent *ptrs, int nPtrs, int functionId,
                                        void *argPtr)
    {

        int idx = getGlobalIdx_1D_1D();

        if (idx < nPtrs)
        {
            // If the Agent is alive, call the method
            if (ptrs[idx].isAlive())
            {
                ptrs[idx].callMethod(functionId, argPtr);
            }
        }
    }

    /**
     * neighbors is converted into a 1D offset of relative indexes before calling this function
     */
    __global__ void exchangeAllPlacesKernel(Place *ptrs, int nptrs, int nNeighbors)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < nptrs)
        {
            int *neighbors = ptrs[idx].getNeighbors();
            Place **neighborsPtr = ptrs[idx].getNeighborsPtr();

            for (int i = 0; i < nNeighbors; ++i)
            {
                int j = idx + offsets_device[i];
                if (j >= 0 && j < nptrs)
                {
                    // Update the neighbor index array
                    neighbors[i] = j;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = &ptrs[j];
                }
                else
                {
                    // Update the neighbor index array
                    neighbors[i] = -1;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = nullptr;
                }
            }
        }
    }

    __global__ void exchangeAllPlacesKernel(Place *ptrs, int nptrs, int nNeighbors, int functionId,
                                            void *argPtr)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < nptrs)
        {
            int *neighbors = ptrs[idx].getNeighbors();
            Place **neighborsPtr = ptrs[idx].getNeighborsPtr();

            for (int i = 0; i < nNeighbors; ++i)
            {
                int j = idx + offsets_device[i];
                if (j >= 0 && j < nptrs)
                {
                    // Update the neighbor index array
                    neighbors[i] = j;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = &ptrs[j];
                }
                else
                {
                    // Update the neighbor index array
                    neighbors[i] = -1;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = nullptr;
                }
            }

            ptrs[idx].callMethod(functionId, argPtr);
        }
    }

    __global__ void exchangeAllPlacesWithLocalOffsetsKernel(Place *ptrs, int nptrs, int nNeighbors)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < nptrs)
        {
            // Get the NEIGHBOR_OFFSETS attribute
            int *offsets = ptrs[idx].getNeighborOffsets();
            // Get the NEIGHBORS attribute
            int *neighbors = ptrs[idx].getNeighbors();
            // Get the NEIGHBOR_PTRS attribute
            Place **neighborsPtr = ptrs[idx].getNeighborsPtr();

            for (int i = 0; i < nNeighbors; i++)
            {
                // Compute the neighbor index
                int j = idx + offsets[i];

                // If the index is valid
                if (j >= 0 && j < nptrs)
                {
                    // Update the neighbor index array
                    neighbors[i] = j;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = &ptrs[j];
                }
                else
                {
                    // Update the neighbor index array
                    neighbors[i] = -1;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = nullptr;
                }
            }
        }
    }

    __global__ void exchangeAllPlacesWithLocalOffsetsKernel(Place *ptrs, int nptrs, int nNeighbors, int functionId,
                                                            void *argPtr)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < nptrs)
        {
            // Get the NEIGHBOR_OFFSETS attribute
            int *offsets = ptrs[idx].getNeighborOffsets();
            // Get the NEIGHBORS attribute
            int *neighbors = ptrs[idx].getNeighbors();
            // Get the NEIGHBOR_PTRS attribute
            Place **neighborsPtr = ptrs[idx].getNeighborsPtr();

            for (int i = 0; i < nNeighbors; i++)
            {
                // Compute the neighbor index
                int j = idx + offsets[i];

                // If the index is valid
                if (j >= 0 && j < nptrs)
                {
                    // Update the neighbor index array
                    neighbors[i] = j;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = &ptrs[j];
                }
                else
                {
                    // Update the neighbor index array
                    neighbors[i] = -1;
                    // Update the neighbor pointer array
                    neighborsPtr[i] = nullptr;
                }
            }

            ptrs[idx].callMethod(functionId, argPtr);
        }
    }

    __global__ void resolveMigrationConflictsKernel(Place *ptrs, int nptrs)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < nptrs)
        {
            ptrs[idx].resolveMigrationConflicts();
        }
    }

    __global__ void updateAgentLocationsKernel(Agent *ptrs, int nptrs)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < nptrs && ptrs[idx].isAlive())
        {
            // Get the destination place of the Agent
            Place **destination = ptrs[idx].getAttribute<Place *>(AgentPreDefinedAttr::DEST_PLACE, 1);

            if (*destination != nullptr)
            {
                // To prevent the agent from moving to the same place
                if (ptrs[idx].getPlace() == *destination)
                {
                    // printf("Agent %lu is NOT moving to Place %lu because it's already there\n",
                    //        ptrs[idx].getIndex(), (*destination)->getIndex());
                    return;
                }
                // printf("Agent %lu is moving to Place %lu\n", ptrs[idx].getIndex(), (*destination)->getIndex());

                // Get the AGENT attribute of the destination place
                Agent **acceptedAgents = (*destination)->getAttribute<Agent *>(PlacePreDefinedAttr::AGENTS, MAX_AGENTS);
                // printf("Dest Place accepted Agent: %lu\n", (*acceptedAgents)->getIndex());

                // Check that the destination Place is actually accepting the Agent

                // If the MAX_AGENTS is 1, the acceptedAgents is a pointer to the
                // pointer pointing to the agent
                if (MAX_AGENTS == 1)
                {
                    if (*acceptedAgents == &ptrs[idx])
                    {
                        // Remove agent from the old place:
                        ptrs[idx].getPlace()->removeAgent(&ptrs[idx]);
                        // Update the agent's place:
                        ptrs[idx].setPlace(*destination);
                    }
                }
                // If the MAX_AGENTS is greater than 1, the acceptedAgents is a pointer to the
                // array of pointers pointing to the agents
                else
                {
                    for (int i = 0; i < MAX_AGENTS; i++)
                    {
                        if (acceptedAgents[i] == &ptrs[idx])
                        {
                            // Remove agent from the old place:
                            ptrs[idx].getPlace()->removeAgent(&ptrs[idx]);
                            // Update the agent's place:
                            ptrs[idx].setPlace(*destination);

                            break;
                        }
                    }
                }

                // Clear the destination place of the Agent after movement
                *destination = nullptr;
            }
        }
    }

    __global__ void countAvailableIndexKernel(Agent *ptrs, int maxAgents, int *availableIndexes)
    {
        int idx = getGlobalIdx_1D_1D();

        if (idx < maxAgents && !ptrs[idx].isAlive())
        {
            // printf("Agent array idx [%d] is available\n", idx);
            availableIndexes[idx] = idx;
        }
    }

    __global__ void spawnAgentsKernel(
        Agent *ptrs, int maxAgents, int *availableIndexesArray, int numAvailableIndexes, int *startIdx)
    {
        int idx = getGlobalIdx_1D_1D();

        // printf("Idx %d, received info: maxAgents = %d, numAvailableIndexes = %d, startIdx = %d\n", idx, maxAgents, numAvailableIndexes, *startIdx);

        if (idx < maxAgents)
        {
            // Get the number of children to spawn
            // a.k.a. the N_CHILDREN attribute
            int numChildren = *(ptrs[idx].getAttribute<int>(AgentPreDefinedAttr::N_CHILDREN, 1));

            // If the agent is alive and has children to spawn:
            if ((ptrs[idx].isAlive()) && (numChildren > 0))
            {

                // Find a spot in Agents array:
                int idxStart = atomicAdd(startIdx, numChildren);
                // printf("Agent %lu at startIdx %d\n", ptrs[idx].getIndex(), idxStart);

                // If there is not enough space in the array,
                // we do not spawn the children and simply return
                if (idxStart + numChildren > numAvailableIndexes)
                {
                    return;
                }

                for (int i = 0; i < numChildren; i++)
                {
                    int childIdx = availableIndexesArray[idxStart + i];
                    // Instantiate with proper index
                    ptrs[childIdx].setAlive();
                    // printf("Agent %lu is set to alive\n", ptrs[childIdx].getIndex());

                    // Link the Agent with the Place
                    // Get parent Agent's child's destination place, a.k.a. the DEST_PLACE attribute
                    Place **destPlace = ptrs[idx].getAttribute<Place *>(AgentPreDefinedAttr::CHILDREN_DEST_PLACE, 1);

                    // Add the child Agent to the destination place
                    bool added = (*destPlace)->addAgent(&ptrs[childIdx]);
                    // And we only change the residence of the agent if the addAgent() operation is successful
                    /**
                     * @note by Warren:
                     * Therefore, if the addAgent() operation is failed, the residence of the agent will not be changed.
                     * This is how we currently handle the Agent spawning, and user is responsible for checking if the
                     * spwned agents are at the desired place.
                     */
                    // Set child Agent's place to the parent Agent's destination place
                    if (added)
                    {
                        ptrs[childIdx].setPlace(*destPlace);
                    }
                }

                // restore Agent spawning data:
                ptrs[idx].clearSpawningData();
            }
        }
    }

    Dispatcher::Dispatcher()
    {
        initialized = false;
        neighborhood = NULL;
    }

    struct DeviceAndMajor
    {
        DeviceAndMajor(int device, int major)
        {
            this->device = device;
            this->major = major;
        }
        int device;
        int major;
    };

    /**
     * Helper function to sort devices by compute capability in descending order.
     *
     * @param i DeviceAndMajor
     * @param j DeviceAndMajor
     *
     * @return true if i.major > j.major
     */
    bool compareDeviceComputeCap(DeviceAndMajor i, DeviceAndMajor j) { return (i.major > j.major); }

    void Dispatcher::init()
    {
        if (!initialized)
        {
            initialized = true;
            logger::debug(("Initializing Dispatcher"));
            int gpuCount = 2;
            // cudaGetDeviceCount(&gpuCount);

            if (gpuCount == 0)
            {
                throw MassException("No GPU devices were found.");
            }

            vector<DeviceAndMajor> devices;
            for (int d = 0; d < gpuCount; d++)
            {
                cudaDeviceProp deviceProp;
                cudaGetDeviceProperties(&deviceProp, d);

                logger::debug("Device %d has compute capability %d.%d", d,
                              deviceProp.major, deviceProp.minor);

                DeviceAndMajor deviceAndMajor = DeviceAndMajor(d, deviceProp.major);
                devices.push_back(deviceAndMajor);
            }

            // Sort devices by compute capability in descending order:
            std::sort(devices.begin(), devices.end(), compareDeviceComputeCap);

            // Pick the device with the highest compute capability for simulation:
            // Mark by Warren: this line of code makes the MASS run on single GPU
            // To make it run on multiple GPUs, we need to change this line of code
            // and make the Dispatcher class to be able to handle multiple GPUs
            deviceInfo = new DeviceConfig(devices[0].device);
        }

        logger::debug("Exiting Dispatcher::init()");
    }

    Dispatcher::~Dispatcher()
    {
        logger::debug("Freeing deviceConfig");
        deviceInfo->freeDevice();
    }

    int Dispatcher::getNumAliveAgents(int agentHandle)
    {
        logger::debug("Entering Dispatcher::getNumAgents");
        return deviceInfo->countAliveAgents(agentHandle);
    }

    Place *Dispatcher::getPlacesDevPtr(int handle)
    {
        return deviceInfo->getDevPlaces(handle);
    }

    void Dispatcher::callAllPlaces(int placeHandle, int functionId, void *argument, int argSize)
    {
        if (initialized)
        {
            logger::debug("Dispatcher::callAllPlaces: Calling all on places[%d]. Function id = %d", placeHandle, functionId);

            // load any necessary arguments
            void *argPtr = NULL;
            if (argument != NULL)
            {
                deviceInfo->load(argPtr, argument, argSize);
            }

            logger::debug("Dispatcher::callAllPlaces: Calling callAllPlacesKernel");

            dim3 *dims = deviceInfo->getPlacesKernelDims(placeHandle);

            callAllPlacesKernel<<<dims[0], dims[1]>>>(
                deviceInfo->getDevPlaces(placeHandle), deviceInfo->countDevPlaces(placeHandle),
                functionId, argPtr);
            CHECK();

            if (argPtr != NULL)
            {
                logger::debug("Dispatcher::callAllPlaces: Freeing device args.");
                cudaFree(argPtr);
            }

            logger::debug("Exiting Dispatcher::callAllPlaces()");
        }
    }

    bool Dispatcher::updateNeighborhood(int handle, vector<int *> *vec)
    {
        logger::debug("Inside Dispatcher::updateNeighborhood");

        neighborhood = vec;
        int nNeighbors = vec->size();
        logger::debug("______new nNeighbors=%d", nNeighbors);

        int *offsets = new int[MAX_NEIGHBORS];

        int nDims = deviceInfo->getPlacesNumDims(handle);
        int *dimensions = deviceInfo->getPlacesDims(handle);
        // logger::debug("nDims=%d", nDims);
        // for(int i = 0; i < nDims; i++) {
        //     logger::debug("dimensions[%d]=%d", i, dimensions[i]);
        // }

        // calculate an offset for each neighbor in vec
        for (int j = 0; j < vec->size(); ++j)
        {
            int *indices = (*vec)[j];
            int offset = Places::relativeIndexToOffset(indices, nDims, dimensions, deviceInfo->getPlaceMajorType(handle));

            offsets[j] = offset;
            logger::debug("neighbor idx: [%d, %d], offsets[%d] = %d", indices[0], indices[1], j, offsets[j]);
        }

        // Fill the blank with 0
        for (int j = vec->size(); j < MAX_NEIGHBORS; ++j)
        {
            offsets[j] = 0;
        }

        // Now copy offsets to the GPU:
        cudaMemcpyToSymbol(offsets_device, offsets, sizeof(int) * MAX_NEIGHBORS);
        CHECK();

        delete[] offsets;
        logger::debug("Exiting Dispatcher::updateNeighborhood");
        return true;
    }

    void Dispatcher::exchangeAllPlaces(int handle, std::vector<int *> *destinations)
    {
        logger::debug("Inside Dispatcher::exchangeAllPlaces");

        if (destinations == NULL)
        {
            throw MassException("Dispatcher::exchangeAllPlaces: destinations is NULL.");
        }

        // logger::debug("Destination is not the same as the neighborhood, updating neighborhood");
        updateNeighborhood(handle, destinations);
        logger::debug("Size of destinations: %d", destinations->size());

        Place *ptrs = deviceInfo->getDevPlaces(handle);
        int nptrs = deviceInfo->countDevPlaces(handle);
        dim3 *dims = deviceInfo->getPlacesKernelDims(handle);

        logger::debug("Launching Dispatcher::exchangeAllPlacesKernel() with dims [%d, %d]", dims[0].x, dims[1].x);
        exchangeAllPlacesKernel<<<dims[0], dims[1]>>>(ptrs, nptrs, destinations->size());
        CHECK();
        logger::debug("Exiting Dispatcher::exchangeAllPlaces");
    }

    void Dispatcher::exchangeAllPlaces(int handle, std::vector<int *> *destinations, int functionId,
                                       void *argument, int argSize)
    {
        logger::debug("Inside Dispatcher::exchangeAllPlaces with functionId = %d as an argument", functionId);

        if (destinations == NULL)
        {
            throw MassException("Dispatcher::exchangeAllPlaces: destinations is NULL.");
        }

        if (neighborhood != destinations)
        {
            logger::debug("Destination is not the same as the neighborhood, updating neighborhood");
            updateNeighborhood(handle, destinations);
        }

        // load any necessary arguments
        void *argPtr = NULL;
        if (argument != NULL)
        {
            deviceInfo->load(argPtr, argument, argSize);
        }

        Place *ptrs = deviceInfo->getDevPlaces(handle);
        int nptrs = deviceInfo->countDevPlaces(handle);
        dim3 *dims = deviceInfo->getPlacesKernelDims(handle);

        exchangeAllPlacesKernel<<<dims[0], dims[1]>>>(ptrs, nptrs, destinations->size(), functionId, argPtr);
        CHECK();
        logger::debug("Exiting Dispatcher::exchangeAllPlaces with functionId = %d as an argument", functionId);
    }

    void Dispatcher::exchangeAllPlaces(int handle, int nNeighbors)
    {
        logger::debug("Inside Dispatcher::exchangeAllPlaces (with local offsets)");

        Place *ptrs = deviceInfo->getDevPlaces(handle);
        int nptrs = deviceInfo->countDevPlaces(handle);
        dim3 *dims = deviceInfo->getPlacesKernelDims(handle);

        logger::debug("Launching Dispatcher::exchangeAllPlacesWithLocalOffsetsKernel()");
        exchangeAllPlacesWithLocalOffsetsKernel<<<dims[0], dims[1]>>>(ptrs, nptrs, nNeighbors);
        CHECK();
        logger::debug("Exiting Dispatcher::exchangeAllPlaces (with local offsets)");
    }

    void Dispatcher::exchangeAllPlaces(int handle, int nNeighbors, int functionId,
                                       void *argument, int argSize)
    {
        logger::debug("Inside Dispatcher::exchangeAllPlaces (with local offsets) with functionId = %d as an argument", functionId);

        // load any necessary arguments
        void *argPtr = NULL;
        if (argument != NULL)
        {
            deviceInfo->load(argPtr, argument, argSize);
        }

        Place *ptrs = deviceInfo->getDevPlaces(handle);
        int nptrs = deviceInfo->countDevPlaces(handle);
        dim3 *dims = deviceInfo->getPlacesKernelDims(handle);

        exchangeAllPlacesWithLocalOffsetsKernel<<<dims[0], dims[1]>>>(ptrs, nptrs, nNeighbors, functionId, argPtr);
        CHECK();
        logger::debug("Exiting Dispatcher::exchangeAllPlaces (with local offsets) with functionId = %d as an argument", functionId);
    }

    void Dispatcher::callAllAgents(int agentHandle, int functionId, void *argument, int argSize)
    {

        logger::debug("Dispatcher::callAllAgents: Calling all on agents[%d]. Function id = %d", agentHandle, functionId);
        if (initialized)
        {
            logger::debug("Dispatcher is initialized, performing callAllAgents");

            // Load arguments if given
            void *argPtr = NULL;
            if (argument != NULL)
            {
                deviceInfo->load(argPtr, argument, argSize);
            }

            logger::debug("Dispatcher::callAllAgents: Calling callAllAgentsKernel");
            dim3 *dims = deviceInfo->getAgentsKernelDims(agentHandle);

            callAllAgentsKernel<<<dims[0], dims[1]>>>(
                deviceInfo->getDevAgents(agentHandle), deviceInfo->getMaxAgents(agentHandle),
                functionId, argPtr);
            CHECK();

            if (argPtr != NULL)
            {
                logger::debug("Dispatcher::callAllAgents: Freeing device args.");
                cudaFree(argPtr);
            }
        }
        logger::debug("Exiting Dispatcher::callAllAgents()");
    }

    void Dispatcher::callAllAgents(int agentHandle, int functionId, void *argument)
    {

        logger::debug("Dispatcher::callAllAgents: Calling all on agents[%d]. Function id = %d", agentHandle, functionId);
        if (initialized)
        {
            logger::debug("Dispatcher is initialized, performing callAllAgents");
            logger::debug("Dispatcher::callAllAgents: Calling callAllAgentsKernel");
            dim3 *dims = deviceInfo->getAgentsKernelDims(agentHandle);

            callAllAgentsKernel<<<dims[0], dims[1]>>>(
                deviceInfo->getDevAgents(agentHandle), deviceInfo->getMaxAgents(agentHandle),
                functionId, argument);
            CHECK();
        }
        logger::debug("Exiting Dispatcher::callAllAgents()");
    }

    void Dispatcher::terminateAgents(int agentHandle)
    {
        logger::debug("Inside Dispatcher::terminateAgents");

        /*
        // Get the device pointer to the agentstate
        AgentState* d_ptrs = static_cast<AgentState*>(deviceInfo->getAgentsState(agentHandle));

        // Get the size of the agentstate array
        int agentsArrSize = deviceInfo->getMaxAgents(agentHandle);

        // Create device pointers from ray device pointers
        thrust::device_ptr<AgentState> d_ptr_begin(d_ptrs);
        thrust::device_ptr<AgentState> d_ptr_end(d_ptrs + agentsArrSize);

        // Sort the agentstate by alive status and index
        thrust::sort(d_ptr_begin, d_ptr_end, AgentComparator());
        CHECK();

        // Count the number of alive agents
        int numAliveAgents = thrust::count_if(
            thrust::device, d_ptr_begin, d_ptr_end,
            AgentStateIsAliveFunctor()
        );
        logger::info("Number of alive agents after termination: %d", numAliveAgents);

        // Update the number of agents & nextIdx in the deviceInfo
        deviceInfo->devAgentsMap[agentHandle].nAgents = numAliveAgents;
        deviceInfo->devAgentsMap[agentHandle].nextIdx = numAliveAgents;
        */

        logger::debug("This function is empty, passed");

        logger::debug("Finished Dispatcher::terminateAgents");
    }

    void Dispatcher::migrateAgents(int agentHandle, int placeHandle)
    {
        logger::debug("Inside Dispatcher::migrateAgents");

        Place *p_ptrs = deviceInfo->getDevPlaces(placeHandle);
        dim3 *placeDims = deviceInfo->getPlacesKernelDims(placeHandle);

        logger::debug("Launching Dispatcher:: resolveMigrationConflictsKernel()");
        resolveMigrationConflictsKernel<<<placeDims[0], placeDims[1]>>>(p_ptrs, deviceInfo->countDevPlaces(placeHandle));
        CHECK();
        logger::debug("Finished Dispatcher:: resolveMigrationConflictsKernel()");

        Agent *a_ptrs = deviceInfo->getDevAgents(agentHandle);
        dim3 *agentDims = deviceInfo->getAgentsKernelDims(agentHandle);

        logger::debug("Launching Dispatcher:: updateAgentLocationsKernel()");
        updateAgentLocationsKernel<<<agentDims[0], agentDims[1]>>>(a_ptrs, getNumMaxAgents(agentHandle));
        CHECK();
        logger::debug("Finished Dispatcher:: updateAgentLocationsKernel()");

        logger::debug("Finished Dispatcher::migrateAgents");
    }

    void Dispatcher::spawnAgents(int handle)
    {
        logger::debug("Inside Dispatcher::spawnAgents");
        Agent *a_ptrs = deviceInfo->getDevAgents(handle);
        dim3 *dims = deviceInfo->getAgentsKernelDims(handle);
        int maxAgents = deviceInfo->getMaxAgents(handle);

        // Find the available indexes of the agents array to boost the performance of the spawnAgentsKernel
        int *availableIndexesArray = new int[maxAgents];
        size_t size_availableIndexesArray = sizeof(int) * maxAgents;
        CATCH(cudaMalloc(&availableIndexesArray, size_availableIndexesArray));
        CATCH(cudaMemset(availableIndexesArray, -1, size_availableIndexesArray));
        logger::debug("Launching Dispatcher::countAvailableIndexKernel() with dims [%d, %d]", dims[0].x, dims[1].x);
        countAvailableIndexKernel<<<dims[0], dims[1]>>>(a_ptrs, maxAgents, availableIndexesArray);
        CHECK();
        logger::debug("Finished Dispatcher::countAvailableIndexKernel()");

        // Reduce the availableIndexesArray to get the number of available indexes
        // No need to manage the memory, thrust will do it for us
        thrust::device_ptr<int> dev_availableIndexPtr(availableIndexesArray);
        thrust::device_ptr<int> new_end;
        new_end = thrust::remove(dev_availableIndexPtr, dev_availableIndexPtr + maxAgents, -1);
        int nAvailableIndexes = new_end - dev_availableIndexPtr;
        logger::debug("Number of available indexes: %d", nAvailableIndexes);
        logger::debug("Number of max agents: %d", maxAgents);

        // Pass this information to spawnAgentsKernel to boost the performance
        int *nAliveAgents = new int(maxAgents - nAvailableIndexes);
        int *startIdx;
        int *h_startIdx = new int(0);
        CATCH(cudaMalloc(&startIdx, sizeof(int)));
        // CATCH(cudaMemset(startIdx, 0, sizeof(int)));
        CATCH(cudaMemcpy(startIdx, h_startIdx, sizeof(int), H2D));
        int *availableIndexesArrayReduced = new int[nAvailableIndexes];
        cudaMalloc(&availableIndexesArrayReduced, sizeof(int) * nAvailableIndexes);
        cudaMemcpy(availableIndexesArrayReduced, availableIndexesArray, sizeof(int) * nAvailableIndexes, D2D);
        CHECK();

        // Launch the spawnAgentsKernel
        logger::debug(
            "Calculated number of alive agents: %d, Agent object stored: %d",
            *nAliveAgents, deviceInfo->countAliveAgents(handle));
        logger::debug("Launching Dispatcher::spawnAgentsKernel()");
        spawnAgentsKernel<<<dims[0], dims[1]>>>(
            a_ptrs, maxAgents, availableIndexesArrayReduced, nAvailableIndexes, startIdx);
        CHECK();
        logger::debug("Finished Dispatcher::spawnAgentsKernel()");

        // Copy back the startIdx to calculate the new number of agents
        CATCH(cudaMemcpy(h_startIdx, startIdx, sizeof(int), D2H));
        logger::debug("New number of agents: %d", *h_startIdx + *nAliveAgents);
        if (*h_startIdx > nAvailableIndexes)
        {
            throw MassException("Trying to spawn more agents than the maximun set for the system");
        }

        deviceInfo->devAgentsMap[handle].nLiveAgents += *h_startIdx;
        // deviceInfo->devAgentsMap[handle].nextIdx += *h_startIdx;

        // Clean up
        delete nAliveAgents;
        delete h_startIdx;
        CATCH(cudaFree(availableIndexesArray));
        CATCH(cudaFree(availableIndexesArrayReduced));
        CATCH(cudaFree(startIdx));

        logger::debug("Finished Dispatcher::spawnAgents");
    }

    int Dispatcher::getNumMaxAgents(int agentHandle)
    {
        return deviceInfo->getMaxAgents(agentHandle);
    }

    __global__ void updateDevAttributesKernel(Place *d_place, int *attributeTags, void **attributeDevPtrs, size_t *attributePitch, int nPlaces, int nAttributes)
    {
        int idx = getGlobalIdx_1D_1D();
        if (idx < nPlaces)
        {
            d_place[idx].updateAllAttributes(attributeTags, attributeDevPtrs, attributePitch, nAttributes);
        }
    }

    __global__ void updateDevAttributesKernel(Agent *d_agent, int *attributeTags, void **attributeDevPtrs, size_t *attributePitch, int nAgents, int nAttributes)
    {
        int idx = getGlobalIdx_1D_1D();
        if (idx < nAgents)
        {
            d_agent[idx].updateAllAttributes(attributeTags, attributeDevPtrs, attributePitch, nAttributes);
        }
    }

    void Dispatcher::finalizeAttributes(DeviceConfig::ObjectType objectType,
                                        int handle,
                                        std::vector<int> *attributeTags,
                                        std::vector<void *> *attributeDevPtrs,
                                        std::vector<size_t> *attributePitch)
    {
        logger::debug("Inside Dispatcher::finalizeAttributes");

        // Validate object type
        if (objectType == DeviceConfig::PLACE)
        {
            logger::debug("Dispatcher::finalizeAttributes: Finalizing attributes for Places");
        }
        else if (objectType == DeviceConfig::AGENT)
        {
            logger::debug("Dispatcher::finalizeAttributes: Finalizing attributes for Agents");
        }
        else
        {
            throw MassException("Invalid objectType");
        }

        // Validate the input
        if (attributeTags->size() != attributeDevPtrs->size() || attributeTags->size() != attributePitch->size())
        {
            throw MassException("The size of the attributeTags, attributeDevPtrs, and attributePitch vectors must be the same");
        }

        // Check if this place has been finalized before
        // If it has, we need to free the memory before allocating new memory
        if (deviceInfo->isAttributesFinalized(objectType, handle))
        {
            logger::debug("Attributes have been finalized before. Freeing memory");
            deviceInfo->clearAttributes(objectType, handle);
        }

        // Allocate memory for these vectors
        size_t nAttributes = attributeTags->size();
        int *d_attributeTags;
        void **d_attributeDevPtrs;
        size_t *d_attributePitch;

        CATCH(cudaMalloc(&d_attributeTags, sizeof(int) * nAttributes));
        CATCH(cudaMalloc(&d_attributeDevPtrs, sizeof(void *) * nAttributes));
        CATCH(cudaMalloc(&d_attributePitch, sizeof(size_t) * nAttributes));

        // Copy data from vector to device
        CATCH(cudaMemcpy(d_attributeTags, attributeTags->data(), sizeof(int) * nAttributes, H2D));
        CATCH(cudaMemcpy(d_attributeDevPtrs, attributeDevPtrs->data(), sizeof(void *) * nAttributes, H2D));
        CATCH(cudaMemcpy(d_attributePitch, attributePitch->data(), sizeof(size_t) * nAttributes, H2D));

        // Store the device pointers to the deviceInfo PlaceArray
        this->deviceInfo->finalizeAttributes(objectType, handle, d_attributeTags, d_attributeDevPtrs, d_attributePitch, nAttributes);

        logger::debug("DeviceConfig finished finalizing places. Launching updateDevAttributesKernel()");

        if (objectType == DeviceConfig::PLACE)
        {
            logger::debug("Dispatcher::finalizeAttributes: Launching updateDevAttributesKernel for Places");
            // Update the attributes of the Place objects on the device
            Place *d_place = deviceInfo->getDevPlaces(handle);
            dim3 *dims = deviceInfo->getPlacesKernelDims(handle);
            updateDevAttributesKernel<<<dims[0], dims[1]>>>(d_place, d_attributeTags, d_attributeDevPtrs, d_attributePitch, deviceInfo->countDevPlaces(handle), nAttributes);
            CHECK();
        }
        else if (objectType == DeviceConfig::AGENT)
        {
            logger::debug("Dispatcher::finalizeAttributes: Launching updateDevAttributesKernel for Agents");
            // Update the attributes of the Agent objects on the device
            Agent *d_agent = deviceInfo->getDevAgents(handle);
            dim3 *dims = deviceInfo->getAgentsKernelDims(handle);
            updateDevAttributesKernel<<<dims[0], dims[1]>>>(d_agent, d_attributeTags, d_attributeDevPtrs, d_attributePitch, deviceInfo->getMaxAgents(handle), nAttributes);
            CHECK();
        }

        logger::debug("Finished Dispatcher::finalizePlaces");
    }

    __global__ void initializeAgentPreDefinedAttributesKernel(
        Agent *agents, int nAgents, Place *places, int nPlaces)
    {
        int idx = getGlobalIdx_1D_1D();

        // If the index of Agent < nAgents
        if (idx < nAgents)
        {
            // Calculate the index of Place where the Agent will be mapped to
            int placeIdx = idx % nPlaces;
            // Link Agent with Place
            agents[idx].setPlace(&places[placeIdx]);
            // Link Place with Agent
            places[placeIdx].addAgent(&agents[idx]);

            // Set the Agent to be alive
            agents[idx].setAlive();
        }
    }

    __global__ void initializeAgentPreDefinedAttributesKernel(
        Agent *agents, int nAgents, Place *places, int nPlaces, int *placeIdxs)
    {
        int idx = getGlobalIdx_1D_1D();

        // If the index of Agent < nAgents
        if (idx < nAgents)
        {
            // Calculate the index of Place where the Agent will be mapped to
            int placeIdx = placeIdxs[idx];

            // Only set the Agent to be alive if the placeIdx is valid
            if (placeIdx >= 0 && placeIdx < nPlaces)
            {
                // Link Agent with Place
                agents[idx].setPlace(&places[placeIdx]);
                // Link Place with Agent
                places[placeIdx].addAgent(&agents[idx]);

                // Set the Agent to be alive
                agents[idx].setAlive();
            }
        }
    }

    void Dispatcher::initializePreDefinedAttributes(DeviceConfig::ObjectType objectType, int handle, int *placeIdxs)
    {
        logger::debug("Inside Dispatcher::initializePreDefinedAttributes");

        // Validate object type
        if (objectType == DeviceConfig::PLACE)
        {
            logger::debug("Dispatcher::initializePreDefinedAttributes: Modifying attributes for Places");
            logger::debug("However, we don't need to modify attributes for Places, simply pass");
        }
        else if (objectType == DeviceConfig::AGENT)
        {
            logger::debug("Dispatcher::initializePreDefinedAttributes: Modifying attributes for Agents");
        }
        else
        {
            throw MassException("Invalid objectType");
        }

        /**
         * No more validation here, because this function is being used internally.
         *
         * Currently, we only use this function on Agents.
         */

        if (objectType == DeviceConfig::AGENT)
        {
            // Get the data needed to launch the kernel
            AgentArray a = deviceInfo->devAgentsMap[handle];
            PlaceArray p = deviceInfo->devPlacesMap[a.placesHandle];

            dim3 *dims = deviceInfo->getAgentsKernelDims(handle);

            // Launch the kernel
            // If the placeIdxs is NULL, we will use the default kernel,
            // which map Agent to Place uniformly
            if (placeIdxs == NULL)
            {
                logger::debug("Launching initializeAgentPreDefinedAttributesKernel with default mapping");
                initializeAgentPreDefinedAttributesKernel<<<dims[0], dims[1]>>>(
                    a.d_agent, a.nLiveAgents, p.d_place, p.qty);
                CHECK();
            }
            else
            {
                logger::debug("Launching initializeAgentPreDefinedAttributesKernel with custom mapping");

                // Allocate memory for the placeIdxs array and copy the data to the device
                int *d_placeIdxs;
                size_t size_placeIdxs = sizeof(int) * a.nLiveAgents;
                CATCH(cudaMalloc(&d_placeIdxs, size_placeIdxs));
                CATCH(cudaMemcpy(d_placeIdxs, placeIdxs, size_placeIdxs, H2D));

                initializeAgentPreDefinedAttributesKernel<<<dims[0], dims[1]>>>(
                    a.d_agent, a.nLiveAgents, p.d_place, p.qty, d_placeIdxs);
                CHECK();

                // Free the memory
                CATCH(cudaFree(d_placeIdxs));
            }
        }
    }

} // namespace mass
