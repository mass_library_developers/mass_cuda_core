#include <curand.h>

#include "DeviceConfig.h"
#include "cudaUtil.h"
#include "Logger.h"
#include "MassException.h"
#include "string.h"

using namespace std;

namespace mass
{

	DeviceConfig::DeviceConfig() : deviceNum(-1)
	{
		freeMem = 0;
		allMem = 0;
		logger::warn("DeviceConfig::NoParam constructor");
	}

	DeviceConfig::DeviceConfig(int device) : deviceNum(device)
	{
		logger::debug("DeviceConfig constructor");
		CATCH(cudaSetDevice(deviceNum));
		CATCH(cudaMemGetInfo(&freeMem, &allMem));
		// CATCH(cudaDeviceSetLimit(cudaLimitMallocHeapSize, allMem * 3 / 4));
		devPlacesMap = map<int, PlaceArray>{};
		devAgentsMap = map<int, AgentArray>{};
	}

	DeviceConfig::~DeviceConfig()
	{
		logger::debug("deviceConfig destructor");

		if (cleanUpFunction)
		{
			cleanUpFunction();
		}
	}

	void DeviceConfig::freeDevice()
	{
		logger::debug("deviceConfig free");

		// Delete agents:
		for (auto it = devAgentsMap.cbegin(); it != devAgentsMap.cend(); ++it)
		{
			deleteAgents(it->first);
		}
		devAgentsMap.clear();

		// Delete places:
		for (auto it = devPlacesMap.cbegin(); it != devPlacesMap.cend(); ++it)
		{
			deletePlaces(it->first);
		}
		devPlacesMap.clear();

		CATCH(cudaDeviceReset());

		logger::debug("Done with deviceConfig freeDevice().");
	}

	void DeviceConfig::load(void *&destination, const void *source, size_t bytes)
	{
		CATCH(cudaMalloc((void **)&destination, bytes));
		CATCH(cudaMemcpy(destination, source, bytes, H2D));
		CATCH(cudaMemGetInfo(&freeMem, &allMem));
	}

	void DeviceConfig::unload(void *destination, void *source, size_t bytes)
	{
		CATCH(cudaMemcpy(destination, source, bytes, D2H));
		CATCH(cudaFree(source));
		CATCH(cudaMemGetInfo(&freeMem, &allMem));
	}

	int DeviceConfig::countDevPlaces(int handle)
	{
		if (devPlacesMap.count(handle) != 1)
		{
			throw MassException("Handle not found.");
		}
		return devPlacesMap[handle].qty;
	}

	int DeviceConfig::getPlacesNumDims(int handle)
	{
		if (devPlacesMap.count(handle) != 1)
		{
			throw MassException("Handle not found.");
		}
		return devPlacesMap[handle].nDims;
	}

	int *DeviceConfig::getPlacesDims(int handle)
	{
		if (devPlacesMap.count(handle) != 1)
		{
			throw MassException("Handle not found.");
		}
		return devPlacesMap[handle].dims;
	}

	Place *DeviceConfig::getDevPlaces(int handle)
	{
		return devPlacesMap[handle].d_place;
	}

	dim3 *DeviceConfig::getPlacesKernelDims(int handle)
	{
		return devPlacesMap[handle].kernelDims;
	}

	int DeviceConfig::getPlaceMajorType(int handle)
	{
		return devPlacesMap[handle].majorType;
	}

	Agent *DeviceConfig::getDevAgents(int handle)
	{
		return devAgentsMap[handle].d_agent;
	}

	int DeviceConfig::countAliveAgents(int handle)
	{
		return devAgentsMap[handle].nLiveAgents;
	}

	int DeviceConfig::getMaxAgents(int handle)
	{
		return devAgentsMap[handle].nMaxAgents;
	}

	dim3 *DeviceConfig::getAgentsKernelDims(int handle)
	{
		return devAgentsMap[handle].dims;
	}

	void DeviceConfig::deleteAgents(int handle)
	{
		AgentArray a = devAgentsMap[handle];

		// Free agents
		CATCH(cudaFree(a.d_agent));

		// Free attributes
		if (a.d_attributeTags != nullptr)
		{
			CATCH(cudaFree(a.d_attributeTags));
		}
		if (a.d_attributeDevPtrs != nullptr)
		{
			CATCH(cudaFree(a.d_attributeDevPtrs));
		}
		if (a.d_attributePitch != nullptr)
		{
			CATCH(cudaFree(a.d_attributePitch));
		}
	}

	void DeviceConfig::deletePlaces(int handle)
	{
		PlaceArray p = devPlacesMap[handle];

		// Free host-side pointer dims
		delete[] p.dims;

		// Free Place
		CATCH(cudaFree(p.d_place));

		// Free attributes
		if (p.d_attributeTags != nullptr)
		{
			CATCH(cudaFree(p.d_attributeTags));
		}
		if (p.d_attributeDevPtrs != nullptr)
		{
			CATCH(cudaFree(p.d_attributeDevPtrs));
		}
		if (p.d_attributePitch != nullptr)
		{
			CATCH(cudaFree(p.d_attributePitch));
		}
	}

	void DeviceConfig::createCudaStreams(int nStreams)
	{
		for (int i = 0; i < nStreams; i++)
		{
			cudaStream_t stream;
			CATCH(cudaStreamCreate(&stream));
			this->cudaStreams.push_back(stream);
		}
	}

	void DeviceConfig::destroyCudaStreams()
	{
		for (int i = 0; i < this->cudaStreams.size(); i++)
		{
			CATCH(cudaStreamDestroy(this->cudaStreams[i]));
		}
	}

	void DeviceConfig::finalizeAttributes(ObjectType object, int handle, int *d_attributeTags, void **d_attributeDevPtrs, size_t *d_attributePitch, size_t nAttributes)
	{
		if (object == ObjectType::PLACE)
		{
			PlaceArray p = devPlacesMap[handle];

			// Set attributes
			p.d_attributeTags = d_attributeTags;
			p.d_attributeDevPtrs = d_attributeDevPtrs;
			p.d_attributePitch = d_attributePitch;
			p.nAttributes = nAttributes;
		}
		else if (object == ObjectType::AGENT)
		{
			AgentArray a = devAgentsMap[handle];

			// Set attributes
			a.d_attributeTags = d_attributeTags;
			a.d_attributeDevPtrs = d_attributeDevPtrs;
			a.d_attributePitch = d_attributePitch;
			a.nAttributes = nAttributes;
		}
	}

	bool DeviceConfig::isAttributesFinalized(ObjectType object, int handle)
	{
		if (object == ObjectType::PLACE)
		{
			PlaceArray p = devPlacesMap[handle];
			return p.d_attributeTags != NULL && p.d_attributeDevPtrs != NULL && p.d_attributePitch != NULL;
		}
		else if (object == ObjectType::AGENT)
		{
			AgentArray a = devAgentsMap[handle];
			return a.d_attributeTags != NULL && a.d_attributeDevPtrs != NULL && a.d_attributePitch != NULL;
		}
		else
		{
			throw MassException("Invalid object type");
		}
	}

	void DeviceConfig::clearAttributes(ObjectType object, int handle)
	{
		if (object == ObjectType::PLACE)
		{
			PlaceArray p = devPlacesMap[handle];
			CATCH(cudaFree(p.d_attributeTags));
			CATCH(cudaFree(p.d_attributeDevPtrs));
			CATCH(cudaFree(p.d_attributePitch));
			p.nAttributes = 0;
		}
		else if (object == ObjectType::AGENT)
		{
			AgentArray a = devAgentsMap[handle];
			CATCH(cudaFree(a.d_attributeTags));
			CATCH(cudaFree(a.d_attributeDevPtrs));
			CATCH(cudaFree(a.d_attributePitch));
			a.nAttributes = 0;
		}
		else
		{
			throw MassException("Invalid object type");
		}
	}

} // end Mass namespace
