#include "Agents.h"
#include "Dispatcher.h"
#include "Logger.h"

using namespace std;

namespace mass
{

    Agents::Agents(int handle, int nAgents, int maxAgents, Dispatcher *d, int placesHandle)
    {
        this->handle = handle;
        this->dispatcher = d;

        logger::debug("Entering Agents::Agents nAgents %d maxAgents %d", nAgents, maxAgents);
        if (maxAgents > nAgents)
        {
            this->numElements = maxAgents;
        }
        else
        {
            this->numElements = nAgents * 2;
        }
        logger::debug("Agents::numElements %d", this->numElements);

        this->placesHandle = placesHandle;
    }

    Agents::~Agents()
    {
        // Creat the dev pointers for the attributes
        for (int i = 0; i < attributeTags.size(); i++)
        {
            CATCH(cudaFree(attributeDevPtrs[i]));
        }
    }

    int Agents::getNumAgents()
    {
        return dispatcher->getNumAliveAgents(handle);
    }

    int Agents::getNumAgentObjects()
    {
        return dispatcher->getNumMaxAgents(handle);
    }

    int Agents::getHandle()
    {
        return handle;
    }

    void Agents::callAll(int functionId)
    {
        logger::debug("Entering Agents::callAll(int functionId)");
        callAll(functionId, NULL, 0);
    }

    void Agents::callAll(int functionId, void *argument, int argSize)
    {
        logger::debug(
            "Entering Agents::callAll(int functionId, void *argument, int argSize)");
        dispatcher->callAllAgents(handle, functionId, argument, argSize);
    }

    void Agents::callAll(int functionId, void *argument)
    {
        logger::debug(
            "Entering Agents::callAll(int functionId, void *argument) - arg already on the device.");
        dispatcher->callAllAgents(handle, functionId, argument);
    }

    void Agents::manageAll()
    {
        // Step 1: kill all agents that need killing
        dispatcher->terminateAgents(handle);

        // Step 2: migrate all agents that need migrating
        dispatcher->migrateAgents(handle, placesHandle);

        // Step 3: spawn all new agents that need spawning
        dispatcher->spawnAgents(handle);
    }

    void Agents::finalizeAttributes()
    {
        this->dispatcher->finalizeAttributes(
            DeviceConfig::ObjectType::AGENT, handle, &attributeTags, &attributeDevPtrs, &attributePitch);
    }

    const std::vector<int> &Agents::getAttributeTags() const
	{
		return attributeTags;
	}

	const std::vector<void *> &Agents::getAttributeDevPtrs() const
	{
		return attributeDevPtrs;
	}

	const std::vector<size_t> &Agents::getAttributePitch() const
	{
		return attributePitch;
	}

    void Agents::initializePreDefinedAttributes(int *placesIdx)
    {
        this->dispatcher->initializePreDefinedAttributes(DeviceConfig::ObjectType::AGENT, handle, placesIdx);
    }

} /* namespace mass */
