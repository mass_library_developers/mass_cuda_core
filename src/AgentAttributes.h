#pragma once
#include <string>

extern __constant__ size_t nAgents;

namespace mass
{
    enum AgentPreDefinedAttr {
            AGENT = 9000,            
			RESIDE_PLACE, // Size: 1, Type: Place * (The Place the Agent is in)
            IS_ALIVE, // Size: 1, Type: bool (Is the Agent alive)
            DEST_PLACE, // Size: 1, Type: Place * (The Place the Agent is going to)
            N_CHILDREN, // Size: 1, Type: int (Number of children to spawn)
            CHILDREN_DEST_PLACE, // Size: 1, Type: Place * (The Place the children are going to be spawned)
		};
} // namespace mass