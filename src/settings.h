
// Maximum number of agents allowed in one place:
#ifndef MAX_AGENTS
#define MAX_AGENTS 1
#endif

// Maximum number of neighbors each place gets data from:
#ifndef MAX_NEIGHBORS
#define MAX_NEIGHBORS 12
#endif

// The maximum dimentionality of the system. 
// E.g. for the 2D system MAX_DIMS should be >= 2
#ifndef MAX_DIMS
#define MAX_DIMS 2
#endif

// Maximum number of migration destinations for an agent from one place.
// E.g. for the system where agent can only migrate 1 cell North, South, East or West 
// N_DESTINATIONS will be 4.
#ifndef N_DESTINATIONS
#define N_DESTINATIONS 12
#endif

// Maximum number of agents that can express interest in migrating to a place.
// In another words, the length of the POTENTIAL_AGENTS array 
// and the maximum of POTENTIAL_AGENT_POPS.
#ifndef MAX_POTENTIAL_AGENTS
#define MAX_POTENTIAL_AGENTS 12
#endif
