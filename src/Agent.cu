#include "Agent.h"

namespace mass
{
    MASS_FUNCTION Agent::Agent(int index) : index(index){};

    __device__ int Agent::find(int *intArr, int n, int val) const
    {
        for (int i = 0; i < n; i++)
        {
            if (intArr[i] == val)
            {
                return i;
            }
        }
        return -1;
    }

    __device__ void Agent::setPlace(Place *place)
    {
        // Get the attribute "RESIDE_PLACE"
        Place **residePlace = getAttribute<Place *>(AgentPreDefinedAttr::RESIDE_PLACE, 1);

        // Set the new value
        *residePlace = place;
    }

    __device__ Place *Agent::getPlace() const
    {
        // Get the attribute "RESIDE_PLACE"
        Place **residePlace = getAttribute<Place *>(AgentPreDefinedAttr::RESIDE_PLACE, 1);

        // Return the value
        return *residePlace;
    }

    __device__ size_t Agent::getPlaceIndex() const
    {
        // Get the attribute "RESIDE_PLACE"
        Place **residePlace = getAttribute<Place *>(AgentPreDefinedAttr::RESIDE_PLACE, 1);

        // Get the index of the Place
        return (*residePlace)->getIndex();
    }

    __device__ size_t Agent::getIndex() const
    {
        return index;
    }

    __device__ void Agent::setAlive()
    {
        // Get the attribute "IS_ALIVE"
        bool *isAlive = getAttribute<bool>(AgentPreDefinedAttr::IS_ALIVE, 1);

        // Set the new value
        *isAlive = true;
    }

    __device__ void Agent::terminate()
    {
        // Get the attribute "IS_ALIVE"
        bool *isAlive = getAttribute<bool>(AgentPreDefinedAttr::IS_ALIVE, 1);

        // Set the new value
        *isAlive = false;

        // If the Agent currently resides in a Place, ask the Place to remove this Agent
        Place *residePlace = getPlace();
        if (residePlace != nullptr)
        {
            residePlace->removeAgent(this);
        }

        // Reset the residePlace attribute
        setPlace(nullptr);
    }

    __device__ bool Agent::isAlive() const
    {
        // Get the attribute "IS_ALIVE"
        bool *isAlive = getAttribute<bool>(AgentPreDefinedAttr::IS_ALIVE, 1);

        // Return the value
        return *isAlive;
    }

    __device__ Place *Agent::getDestPlace() const
    {
        return *(getAttribute<Place *>(AgentPreDefinedAttr::DEST_PLACE, 1));
    }

    __device__ int Agent::getNumChildren() const
    {
        return *(getAttribute<int>(AgentPreDefinedAttr::N_CHILDREN, 1));
    }
    __device__ Place *Agent::getChildrenDestPlace() const
    {
        return *(getAttribute<Place *>(AgentPreDefinedAttr::CHILDREN_DEST_PLACE, 1));
    }

    __device__ void Agent::migrate(Place *newDestPlace)
    {
        // Get the attribute "DEST_PLACE"
        Place **destPlace = getAttribute<Place *>(AgentPreDefinedAttr::DEST_PLACE, 1);

        // Set the new destination Place
        *destPlace = newDestPlace;

        // Ask the dest Place to add this Agent to its migration list
        (*destPlace)->addMigratingAgent(this);
    }

    __device__ void Agent::spawn(size_t numAgents, Place *place)
    {
        // Get the attribute "N_CHILDREN"
        // Update the number of children
        int *nChildren = getAttribute<int>(AgentPreDefinedAttr::N_CHILDREN, 1);
        *nChildren = numAgents;

        // Get the attribute "CHILDREN_DEST_PLACE"
        // Set the destination Place for the children
        Place **childrenDestPlace = getAttribute<Place *>(AgentPreDefinedAttr::CHILDREN_DEST_PLACE, 1);
        *childrenDestPlace = place;
    }

    __device__ void Agent::clearSpawningData()
    {
        // This function will clear the `N_CHILDREN` & `CHILDREN_DEST_PLACE` attributes of the current Agent.

        // Get the attribute "N_CHILDREN"
        int *nChildren = getAttribute<int>(AgentPreDefinedAttr::N_CHILDREN, 1);
        *nChildren = 0;

        // Get the attribute "CHILDREN_DEST_PLACE"
        Place **childrenDestPlace = getAttribute<Place *>(AgentPreDefinedAttr::CHILDREN_DEST_PLACE, 1);
        *childrenDestPlace = nullptr;
    }

    __device__ void Agent::updateAllAttributes(int *attributeTags, void **attributeDevPtrs, size_t *attributePitch, int nAttributes)
    {
        this->nAttributes = nAttributes;
        this->attributeTags = attributeTags;
        this->attributeDevPtrs = attributeDevPtrs;
        this->attributePitch = attributePitch;
    }

}