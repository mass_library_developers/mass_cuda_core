#pragma once

#define MASS_FUNCTION __host__ __device__
#include <cuda_runtime.h>
#include "AgentAttributes.h"
#include "Place.h"

namespace mass
{

    // forward declaration
    class Place;

    class Agent
    {

    public:
        /**
         * The default constructor.
         *
         * @param index The unique index of this Agent.
         */
        MASS_FUNCTION Agent(int index);
        MASS_FUNCTION virtual ~Agent() {}

        /**
         * Called by MASS while executing Agents.callAll(). This is intended
         * to be a switch statement where each user-implemented function is
         * mapped to a funcID, and is passed ‘args’ when called.
         *
         * @param functionId The ID of the function to be called.
         * @param arg        A pointer to the arguments passed to the function.
         */
        __device__ virtual void callMethod(int functionId, void *arg = NULL) = 0;

        /**
         * Returns the unique index of this Agent.
         */
        __device__ size_t getIndex() const;

        /**
         * Assigns this Agent to a particular Place (its place of residency).
         * 
         * @param place The Place to which the Agent is assigned.
         */
        __device__ void setPlace(Place* place);

        /**--------------------------------------------------------
         * Belows are getters for pre-defined attributes of Agent.
         --------------------------------------------------------*/

        /**
         * Gets the Place on which this Agent is located/resides.
         * 
         * @return The Place on which this Agent is located.
        */
        __device__ Place* getPlace() const;

        /**
         * Returns the index of the Place on which this agent is located/resides.
         * This function can be called from both CPU and GPU code.
         */
        __device__ size_t getPlaceIndex() const;

        /**
         * Returns true if the Agent is in an active state. Returns false if the
         * agent hasn’t been activated yet (extra inactive agents are allocated
         * at simulation start to optimize memory allocation), or if it has
         * already been terminated.
         */
        __device__ bool isAlive() const;

        /**
         * Gets the destination Place where this Agent is migrating to.
         * 
         * @return The destination Place where this Agent is migrating to.
        */
        __device__ Place *getDestPlace() const;

        /**
         * Gets the number of children that this Agent is spawning.
         * 
         * @return The number of children that this Agent is spawning.
        */
        __device__ int getNumChildren() const;

        /**
         * Gets the destination Place where the children of this Agent are spawning.
         * 
         * @return The destination Place where the children of this Agent are spawning.
        */
        __device__ Place *getChildrenDestPlace() const;

        /** ---------------------------------------------------
         * End of getters for pre-defined attributes of Place.
         ----------------------------------------------------*/

        /**
         * Sets the Agent’s status to active, thus including the Agent in all callAll() calls.
         */
        __device__ void setAlive();

        /**
         * Sets the Agent's status to inactive. The Agent's place is set to vacant and
         * the Agent is excluded from all subsequent callAll() function calls.
         */
        __device__ void terminate();

        /**
         * Current Agent express the intention to migrate to the specified place.
         * The Agent will be added to Place's potential Migrating Agents list.
         * The actual migration is performed during the subsequent Agents::manageAll() call.
         * The migration is not guaranteed if the destination place is already occupied
         * or if there is a competing agent trying to get to the same destination.
         *
         * @param newDestPlace The destination Place.
         */
        __device__ void migrate(Place *newDestPlace);

        /**
         * Spawns the specified number of new agents at the specified place.
         * The function does not accept instantiation arguments due to current limitations of the library.
         * If you need to set some parameters of the newly created Agents, please set these parameters
         * in a separate function after agent creation.
         * The actual spawning is performed during the subsequent Agents::manageAll() call.
         * The spawning is not guaranteed if the destination place is already full
         * or if there is a competing agent trying to spawn at the same place.
         *
         * @param numAgents  The number of agents to be spawned.
         * @param place The Place where the agents will be spawned.
         */
        __device__ void spawn(size_t numAgents, Place *place);

        /**
         * Clears the spawning data of the current Agent.
         * This function should be called after the spawning of new agents is completed.
         * 
         * This function will clear the `N_CHILDREN` & `CHILDREN_DEST_PLACE` attributes of the current Agent.
        */
        __device__ void clearSpawningData();

        /**
         * Get an attribute of `this Agent` by attribute tag (name set by user when adding an attribute).
         * For more infomartion about the tag and length, please refer to `Agents::setAttribute()`.
         *
         * @param tag The tag of the attribute.
         * @param length The length of the attribute (length set when adding an attribute).
         *
         * @return A pointer to the attribute, whether 1D or 2D.
         */
        template <typename T>
        __device__ T *getAttribute(int tag, int length) const;

        /**
         * Get an attribute of `another Agent` by attribute tag (name set by user when adding an attribute).
         * For more infomartion about the tag and length, please refer to `Agents::setAttribute()`.
         *
         * @param desIndex The index of the destination Agent.
         * @param tag The tag of the attribute.
         * @param length The length of the attribute (length set when adding an attribute).
         *
         * @return A pointer to the attribute, whether 1D or 2D.
         */
        template <typename T>
        __device__ T *getAttribute(size_t desIndex, int tag, int length) const;

        /**
         * Update all attributes info. This function is being used when user call `Agents::finalizeAttributes()`
         * where new attributes info will be updated from host to device, ans this function is used to update
         * the information of all attributes on device based on given info from host.
         * 
         * @param attributeTags An int array that contains the tags of all attributes.
         * @param attributeDevPtrs A void pointer array that contains the device pointers of all attributes.
         * @param attributePitch A size_t array that contains the pitch of all attributes.
         * @param nAttributes The number of attributes.
        */
        __device__ void updateAllAttributes(int *attributeTags, void **attributeDevPtrs, size_t *attributePitch, int nAttributes);

    private:
        size_t index;            // The index of this Agent in the Agents instance
        size_t nAttributes;      // The number of attributes of all Agent objects
        int *attributeTags;      // The tags of all attributes
        void **attributeDevPtrs; // The device pointers of all attributes,
                                 // each pointer in this array points to the first element of the attribute
        size_t *attributePitch;  // The pitch of all attributes, if the attribute is a 2D array,
                                 // the pitch is recorded, otherwise, it is 0

         /**
         * Finds the index of an int value in an int array.
         * 
         * @param intArr The int array to be searched.
         * @param n The length of the int array.
         * @param val The int value to be found.
         * 
         * @return The index of the int value in the int array, or -1 if not found.
        */
        __device__ int find(int *intArr, int n, int val) const;
    };

    template <typename T>
    __device__ T *Agent::getAttribute(int tag, int length) const
    {
        int i = find(attributeTags, nAttributes, tag);
        if (i == -1)
        {
            return NULL;
        }

        // If the length is greater than 0, then it is a 2D array
        // Otherwise, it is a 1D array

        // If it is a 2D array, we need to get the row first
        if (attributePitch[i] > 0)
        {
            // Get the row
            T *row = (T *)((char *)attributeDevPtrs[i] + index * attributePitch[i]);

            return row;
        }
        // If it is a 1D array, we can just return array[index]
        else
        {
            T *array = static_cast<T *>(attributeDevPtrs[i]);
            return &array[index];
        }
    }

    template <typename T>
    __device__ T *Agent::getAttribute(size_t des_index, int tag, int length) const
    {
        int i = find(attributeTags, nAttributes, tag);
        if (i == -1)
        {
            return NULL;
        }

        // If the length is greater than 0, then it is a 2D array
        // Otherwise, it is a 1D array

        // If it is a 2D array, we need to get the row first
        if (attributePitch[i] > 0)
        {
            // Get the row
            T *row = (T *)((char *)attributeDevPtrs[i] + des_index * attributePitch[i]);

            return row;
        }
        // If it is a 1D array, we can just return array[index]
        else
        {
            T *array = static_cast<T *>(attributeDevPtrs[i]);
            return &array[des_index];
        }
    }
}
