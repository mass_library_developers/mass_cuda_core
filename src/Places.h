#pragma once

#include <map>
#include <string>
#include <vector>

#include "Dispatcher.h"
#include "PlaceAttributes.h"
#include "Logger.h"
#include "MassException.h"
#include "DeviceConfig.h"

namespace mass
{

	// forward declarations
	class Dispatcher;
	class Place;
	class Place;
	class DeviceConfig;

	class Places
	{
		friend class Mass;

	public:
		/**
		 *  Destructor
		 */
		~Places();

		/**
		 *  Returns the number of dimensions in this places object. (I.e. 2D, 3D, etc...)
		 *
		 * @return the number of dimensions in this places object.
		 */
		int getDimensions();

		/**
		 *  Returns the actual dimensions. The returned array will be getDimension() elements long.
		 *
		 * @return the actual dimensions of this places object.
		 */
		int *getSize();

		/**
		 * Returns the number of Place object present in this Places collection.
		 *
		 * @return the number of Place object present in this Places collection.
		 */
		int getNumPlaces();

		/**
		 *  Returns the handle associated with this Places object.
		 *
		 * @return the handle associated with this Places object.
		 */
		int getHandle();

		/**
		 *  Executes the function by given functionId on each Place element within this Places with
		 *  the provided argument.
		 *
		 *  @param functionId the function id passed to each Place element
		 *  @param argument the argument to be passed to each Place element
		 *  @param argSize the size in bytes of the argument
		 */
		void callAll(int functionId, void *argument = NULL, int argSize = 0);

		/**
		 *  @brief This function causes all Place elements to exchange information about their neighbors.
		 *
		 *  The neighbors array in each of the places is populated with pointers to the Places in specified
		 *  in the destinations vector. The offsets to neighbors are defined in the destinations vector (a collection
		 *  of offsets from the caller to the callee place elements).
		 *
		 * @example
		 *  Example destinations vector (row-major):
		 * @code
		 *    vector<int*> destinations;
		 *    int north[2] = {-1, 0}; destinations.push_back( north );
		 *    int east[2] = {0, 1}; destinations.push_back( east );
		 *    int south[2] = {1, 0}; destinations.push_back( south );
		 *    int west[2] = {0, -1}; destinations.push_back( west );
		 * @endcode
		 *
		 * @param destinations A vector of relative indexes of neighbors.
		 */
		void exchangeAll(std::vector<int *> *destinations);

		/**
		 *  @brief This function causes all Place elements to exchange information about their neighbors and to call
		 *  the function specified with functionId on each of the places afterwards.
		 *
		 *  In addition to the fuctionality of the standard exchangeAllPlaces function specified above
		 *  it also takes functionId as a parameter and arguments to that function.
		 *  When the data is collected from the neighboring places,
		 *  the specified function is executed on all of the places with specified parameters.
		 *  The rationale behind implemening this version of exchangeAllPlaces is performance optimization:
		 *  the data cached during data collection step can be used for the data calculation and thus minimize
		 *  the number of memeory fetches and improve performance.
		 *
		 * @param destinations A vector of relative indexes of neighbors.
		 * @param functionId The ID of the user defined function.
		 * @param argument The function argument. Can be NULL.
		 * @param argSize The size of argument in bytes. If argument == NULL, argSize is not used.
		 */
		void exchangeAll(std::vector<int *> *destinations, int functionId,
						 void *argument = NULL, int argSize = 0);

		/**
		 * @brief This function causes all Place elements to exchange information about their neighbors.
		 *
		 * Unlike the standard exchangeAllPlaces function, this function uses the `NEIGHBOR_OFFSETS` attribute
		 * array to determine the neighbors of each place where the standard exchangeAllPlaces function uses
		 * the input vector of relative indexes of neighbors.
		 *
		 * The `NEIGHBOR_OFFSETS` attribute array should be set by the user before calling this function.
		 *
		 * @param nNeighbors The number of neighbors for each place.
		 */
		void exchangeAll(int nNeighbors);

		/**
		 * @brief This function causes all Place elements to exchange information about their neighbors and to call
		 *  the function specified with functionId on each of the places afterwards.
		 *
		 * Unlike the standard exchangeAllPlaces function, this function uses the `NEIGHBOR_OFFSETS` attribute
		 * array to determine the neighbors of each place where the standard exchangeAllPlaces function uses
		 * the input vector of relative indexes of neighbors.
		 *
		 * The `NEIGHBOR_OFFSETS` attribute array should be set by the user before calling this function.
		 *
		 * @param nNeighbors The number of neighbors for each place.
		 * @param functionId The ID of the user defined function.
		 * @param argument The function argument. Can be NULL.
		 * @param argSize The size of argument in bytes. If argument == NULL, argSize is not used.
		 */
		void exchangeAll(int nNeighbors, int functionId,
						 void *argument = NULL, int argSize = 0);

		/**
		 *  Returns the pointer to dev Places object.
		 */
		Place *getDevPointer();

		/**
		 * Returns the row major index of the given coordinates. For instance, in an
		 * int[3][5] Places, the element (1,3) will have the row major index of 8.
		 *
		 * @param indices an series of ints ordered rowIdx,colIdx,ZIdx,etc...
		 * that specify a single element in this places object. The number of
		 * elements must be equal to the number of dimensions in this places object.
		 * All indices must be non-negative.
		 *
		 * This is the inverse function of getIndexVector()
		 *
		 * @return an int representing the row-major index where this element is stored.
		 */
		int getRowMajorIdx(int *indices);

		/**
		 * This function will take a valid (in bounds) row-major index for this
		 * places object and return a vector that contains the row, col, z, etc...
		 * indices for the place element at the given row major index.
		 *
		 * This is the inverse function of getRowMajorIdx(...)
		 *
		 * @param rowMajorIdx the index of an element in a flattened mult-dimensional
		 * array. Must be non-negative.
		 *
		 * @return a vector<int> with the multi-dimensional indices of the element.
		 */
		std::vector<int> getIndexVector(int rowMajorIdx);

		/**
		 * @brief This function will convert a row-major relative index to an offset.
		 * This function works for 1D, 2D, 3D, etc... places objects.
		 * This function will throw an exception if the dimensions of this places object
		 * do not match the number of elements in the relativeIndex array.
		 *
		 * @param relativeIndex an array of ints ordered rowIdx,colIdx,ZIdx,etc...
		 * @param numDimensions the number of dimensions in this places object and the length of relativeIndex
		 * @param dimensions the an array of ints representing the dimensions of this places object
		 * @param majorType the type of the places object (row major or column major)
		 *
		 * @example
		 * int dimensions[2] = {10, 10};
		 * int relativeIndex[2] = {1, 0};
		 * offset = 10 * 1 + 0 * 1 = 10
		 */
		static int relativeIndexToOffset(int *relativeIndex, int numDimensions, int *dimensions, int majorType);

		/**
		 *  Add an attribute to the Place object. This function is the pre-stage
		 * of adding attributes, it only adds the attribute on the host. When all
		 * attributes have been added, call the finalizeAttributes() function to
		 * update the attributes info on the device.
		 *
		 * @param tag the attribute tag
		 * @param length the length of the attribute. For example, if the attribute is called "index", then length = 1
		 * because every Place will only have one index. If the attribute is called "position", then length can be 2
		 * because the position attribute has 2 dimensions (x,y).
		 */
		template <typename T>
		void setAttribute(unsigned int tag, unsigned int length);

		/**
		 *  Add an attribute to the Place object. This function is the pre-stage
		 * of adding attributes, it only adds the attribute on the host. When all
		 * attributes have been added, call the finalizeAttributes() function to
		 * update the attributes info on the device.
		 *
		 * @param tag the attribute tag
		 * @param length the length of the attribute. For example, if the attribute is called "index", then length = 1
		 * because every Place will only have one index. If the attribute is called "position", then length can be 2
		 * because the position attribute has 2 dimensions (x,y).
		 * @param defaultValue the default value of the attribute. This attribute of every Place will be initialized
		 * to this value.
		 */
		template <typename T>
		void setAttribute(unsigned int tag, unsigned int length, T defaultValue);

		/**
		 *  Get the attributes tags.
		 *
		 *  @return the array of attribute tags
		 */
		const std::vector<int> &getAttributeTags() const;

		/**
		 *  Get the attributes devPtrs.
		 *
		 *  @return the array of attribute device pointers
		 */
		const std::vector<void *> &getAttributeDevPtrs() const;

		/**
		 *  Get the attributes pitch.
		 *
		 *  @return the array of attribute pitch. If the attribute is 1D, then the
		 *   pitch is 0.
		 */
		const std::vector<size_t> &getAttributePitch() const;

		/**
		 *  Finalize the attributes. After adding attributes using the
		 *  setAttribute() function, this function must be called to update the
		 *  attributes info on the device. Otherwise, the attributes will not
		 *  be updated on the device.
		 */
		void finalizeAttributes();

		template <typename T>
		/**
		 * Download the attributes from the device.
		 * 
		 * @param tag the attribute tag.
		 * @param length the length of the attribute, 1 returns a 1D array, >1 returns a 2D array.
		 * 
		 * @return The pointer to the host memory with the downloaded data.
		*/
		T *downloadAttributes(int tag, unsigned int length);

	private:
		/**
		 *  Creates a Places object.
		 *
		 *  @param handle the unique identifier of this places collections
		 */
		Places(int handle, int dimensions, int size[], Dispatcher *d);

		int handle;				// User-defined identifier for this Places_Base
		Dispatcher *dispatcher; // the GPU dispatcher

		int numDims;		  // the number of dimensions for this Places_Base (i.e. 1D, 2D, 3D, etc...)
		int *dimensions;	  // dimensions of the grid in which these places are located. It must be numDims long
		unsigned numElements; // Number of Place elements

		// The vector contains only the tags (names) of the attributes
		// so that we can map the names to the index of the attribute in the vector
		std::vector<int> attributeTags;

		// The actual vector stores all the device pointers for the attributes
		// so that they can be used in the kernel by passing this vector to the kernel
		std::vector<void *> attributeDevPtrs;

		// The vector contains the pitch size for 2D attributes
		std::vector<size_t> attributePitch;
	};

	template <typename T>
	inline void Places::setAttribute(unsigned int tag, unsigned int length)
	{
		// Check if the attribute already exists
		std::vector<int>::iterator it = std::find(attributeTags.begin(), attributeTags.end(), tag);
		if (it != attributeTags.end())
		{
			// The attribute already exists
			// Log the error and return
			logger::error("Attribute with tag %d already exists in Place handle %d", tag, handle);
			throw MassException("Attribute already exists in Places");
		}

		// Use to hold the device pointer for the attribute
		void *devPtr;

		if (length < 1)
		{
			logger::error("Attribute length must be greater than 0");
			throw MassException("Attribute length must be greater than 0");
		}
		// If length is 1, them simply create the attribute device pointer
		else if (length <= 1)
		{
			// Add the attribute to the attribute map
			dispatcher->template createAttribute<T>(length, numElements, &devPtr);
			attributeTags.push_back(tag);
			attributeDevPtrs.push_back(devPtr);
			attributePitch.push_back(0);
		}
		// If length is greater than 1, then create the attribute device pointer and record
		// the pitch size for 2D attributes
		else
		{
			// Add the attribute to the attribute map
			size_t pitch;
			dispatcher->template createAttribute<T>(length, numElements, &devPtr, &pitch);
			attributeTags.push_back(tag);
			attributeDevPtrs.push_back(devPtr);
			attributePitch.push_back(pitch);
		}
	}

	template <typename T>
	inline void Places::setAttribute(unsigned int tag, unsigned int length, T defaultValue)
	{
		// Check if the attribute already exists
		std::vector<int>::iterator it = std::find(attributeTags.begin(), attributeTags.end(), tag);
		if (it != attributeTags.end())
		{
			// The attribute already exists
			// Log the error and return
			logger::error("Attribute with tag %d already exists in Place handle %d", tag, handle);
			throw MassException("Attribute already exists in Places");
		}

		// Use to hold the device pointer for the attribute
		void *devPtr;

		if (length < 1)
		{
			logger::error("Attribute length must be greater than 0");
			throw MassException("Attribute length must be greater than 0");
		}
		// If length is 1, them simply create the attribute device pointer
		else if (length <= 1)
		{
			// Add the attribute to the attribute map
			dispatcher->template createAttribute<T>(DeviceConfig::ObjectType::PLACE, handle, length, numElements, defaultValue, &devPtr);
			attributeTags.push_back(tag);
			attributeDevPtrs.push_back(devPtr);
			attributePitch.push_back(0);
		}
		// If length is greater than 1, then create the attribute device pointer and record
		// the pitch size for 2D attributes
		else
		{
			// Add the attribute to the attribute map
			size_t pitch;
			dispatcher->template createAttribute<T>(DeviceConfig::ObjectType::PLACE, handle, length, numElements, defaultValue, &devPtr, &pitch);
			attributeTags.push_back(tag);
			attributeDevPtrs.push_back(devPtr);
			attributePitch.push_back(pitch);
		}
		logger::debug("Created attribute with tag %d devPtr is %p is null? %d", tag, devPtr, devPtr == nullptr);
	}

	template <typename T>
	inline T *Places::downloadAttributes(int tag, unsigned int length){
		// Find the index of the attribute in the attributeTags vector
		std::vector<int>::iterator it = std::find(attributeTags.begin(), attributeTags.end(), tag);
		if (it == attributeTags.end())
		{
			// The attribute does not exist
			// Log the error and return
			logger::error("Attribute with tag %d does not exist in Places handle %d", tag, handle);
			throw MassException("Attribute with tag %d does not exist in Places handle %d", tag, handle);
		}

		// Get the index of the attribute in the attributeTags vector
		int index = std::distance(attributeTags.begin(), it);
		logger::debug("Places downloading tag %d at index %d", tag, index);

		// Download the attribute from the device
		return dispatcher->template downloadAttributes<T>(attributeDevPtrs[index], attributePitch[index], length, numElements);
	}

} /* namespace mass */
