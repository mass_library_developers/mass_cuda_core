#pragma once

#include <map>
#include <string>
#include <vector>

#include "Logger.h"
#include "Agent.h"
#include "DeviceConfig.h"
#include "Dispatcher.h"

namespace mass
{

    // forward declarations
    class Dispatcher;

    class Agents
    {
        friend class Mass;

    public:
        ~Agents();

        /**
         * Returns the number of alive agents present in this agents collection.
         * @return
         */
        int getNumAgents();

        /*
         * Returns the number of all agent objects present in this agents collection (some can be terminated).
         */
        int getNumAgentObjects();

        /**
         *  Returns the handle associated with this Agents object that was set at construction.
         */
        int getHandle();

        /**
         *  Executes the given functionId on each Agent element within this Places.
         *
         *  @param functionId the function id passed to each Agent element
         */
        void callAll(int functionId);

        /**
         *  Executes the given functionId on each Agent element within this Places with
         *  the provided argument.
         *
         *  @param functionId the function id passed to each Agent element
         *  @param argument the argument to be passed to each Agent element
         *  @param argSize the size in bytes of the argument
         */
        void callAll(int functionId, void *argument, int argSize);

        /**
         * Executes the given functionId on each Agent element within this Places with
         *  the provided argument. In this version, the argument should already be
         * on the device. We will not copy the argument to the device again.
         * 
         * @param functionId the function id passed to each Agent element
         * @param argument the argument to be passed to each Agent element
        */
        void callAll(int functionId, void *argument);

        /*
         * Executes agent termination / migration / spawning, which was initiated since the previous call to manageAll()
         */
        void manageAll();

        /**
         *  Add an attribute to the Place object. This function is the pre-stage
         * of adding attributes, it only adds the attribute on the host. When all
         * attributes have been added, call the finalizeAttributes() function to
         * update the attributes info on the device.
         *
         * @param tag the attribute tag
         * @param length the length of the attribute. For example, if the attribute is called "index", then length = 1
         * because every Place will only have one index. If the attribute is called "position", then length can be 2
         * because the position attribute has 2 dimensions (x,y).
         */
        template <typename T>
        void setAttribute(unsigned int tag, unsigned int length);

        /**
         *  Add an attribute to the Place object. This function is the pre-stage
         * of adding attributes, it only adds the attribute on the host. When all
         * attributes have been added, call the finalizeAttributes() function to
         * update the attributes info on the device.
         *
         * @param tag the attribute tag
         * @param length the length of the attribute. For example, if the attribute is called "index", then length = 1
         * because every Place will only have one index. If the attribute is called "position", then length can be 2
         * because the position attribute has 2 dimensions (x,y).
         * @param defaultValue the default value of the attribute. This attribute of every Place will be initialized
         * to this value.
         */
        template <typename T>
        void setAttribute(unsigned int tag, unsigned int length, T defaultValue);

        /**
		 *  Get the attributes tags.
		 *
		 *  @return the array of attribute tags
		 */
		const std::vector<int> &getAttributeTags() const;

		/**
		 *  Get the attributes devPtrs.
		 *
		 *  @return the array of attribute device pointers
		 */
		const std::vector<void *> &getAttributeDevPtrs() const;

		/**
		 *  Get the attributes pitch.
		 *
		 *  @return the array of attribute pitch. If the attribute is 1D, then the
		 *   pitch is 0.
		 */
		const std::vector<size_t> &getAttributePitch() const;

        /**
         *  Finalize the attributes. After adding attributes using the
         *  setAttribute() function, this function must be called to update the
         *  attributes info on the device. Otherwise, the attributes will not
         *  be updated on the device.
         */
        void finalizeAttributes();

        /**
         * Initializes the pre-defined attributes for this Agents instance
         * after the instantiation.
         * This is an extra step compaired to Places, because Agents has to
         * link to the Places and set to alive, which is modifying the pre-defined
         * attributes. This is not necessary for Places, because Places do not
         * require the modification of the pre-defined attributes.
         * 
         * @param placesIdx an array of Place indices that each agent belongs to,
         *  should be the same size as nAgents
         */
        void initializePreDefinedAttributes(int *placesIdx);

        template <typename T>
		/**
		 * Download the attributes from the device.
		 * 
		 * @param tag the attribute tag.
		 * @param length the length of the attribute, 1 returns a 1D array, >1 returns a 2D array.
		 * 
		 * @return The pointer to the host memory with the downloaded data.
		*/
		T *downloadAttributes(int tag, unsigned int length);

    private:
        /**
         * Creates an Agents object.
         *
         * @param handle the unique identifier of this Agents collections
         * @param nAgents the number of agents user initially wants to store in this collection
         * @param maxAgents the maximum number of agents that can be stored in this collection
         * @param d the GPU dispatcher
         * @param placesHandle the handle of the Places collection associated with this Agents
         */
        Agents(int handle, int nAgents, int maxAgents, Dispatcher *d, int placesHandle);

        int handle;             // User-defined identifier for this Agents collection
        int placesHandle;       // Places collection associated with these Agents
        size_t numElements;     // Number of agents in this collection
        Dispatcher *dispatcher; // the GPU dispatcher

        // The vector contains only the tags (names) of the attributes
        // so that we can map the names to the index of the attribute in the vector
        std::vector<int> attributeTags;

        // The actual vector stores all the device pointers for the attributes
        // so that they can be used in the kernel by passing this vector to the kernel
        std::vector<void *> attributeDevPtrs;

        // The vector contains the pitch size for 2D attributes
        std::vector<size_t> attributePitch;
    };

    template <typename T>
    inline void Agents::setAttribute(unsigned int tag, unsigned int length)
    {
        // Check if the attribute already exists
        std::vector<int>::iterator it = std::find(attributeTags.begin(), attributeTags.end(), tag);
        if (it != attributeTags.end())
        {
            // The attribute already exists
            // Log the error and return
            logger::error("Attribute with tag %d already exists in Place handle %d", tag, handle);
            throw MassException("Attribute already exists in Places");
        }

        // Use to hold the device pointer for the attribute
        void *devPtr;

        if (length < 1)
        {
            logger::error("Attribute length must be greater than 0");
            throw MassException("Attribute length must be greater than 0");
        }
        // If length is 1, them simply create the attribute device pointer
        else if (length <= 1)
        {
            // Add the attribute to the attribute map
            dispatcher->template createAttribute<T>(length, numElements, &devPtr);
            attributeTags.push_back(tag);
            attributeDevPtrs.push_back(devPtr);
            attributePitch.push_back(0);
        }
        // If length is greater than 1, then create the attribute device pointer and record
        // the pitch size for 2D attributes
        else
        {
            // Add the attribute to the attribute map
            size_t pitch;
            dispatcher->template createAttribute<T>(length, numElements, &devPtr, &pitch);
            attributeTags.push_back(tag);
            attributeDevPtrs.push_back(devPtr);
            attributePitch.push_back(pitch);
        }
    }

    template <typename T>
    inline void Agents::setAttribute(unsigned int tag, unsigned int length, T defaultValue)
    {
        // Check if the attribute already exists
        std::vector<int>::iterator it = std::find(attributeTags.begin(), attributeTags.end(), tag);
        if (it != attributeTags.end())
        {
            // The attribute already exists
            // Log the error and return
            logger::error("Attribute with tag %d already exists in Agents handle %d", tag, handle);
            throw MassException("Attribute already exists in Agents");
        }

        // Use to hold the device pointer for the attribute
        void *devPtr;

        if (length < 1)
        {
            logger::error("Attribute length must be greater than 0");
            throw MassException("Attribute length must be greater than 0");
        }
        // If length is 1, them simply create the attribute device pointer
        else if (length <= 1)
        {
            // Add the attribute to the attribute map
            dispatcher->template createAttribute<T>(DeviceConfig::ObjectType::AGENT, handle, length, numElements, defaultValue, &devPtr);
            attributeTags.push_back(tag);
            attributeDevPtrs.push_back(devPtr);
            attributePitch.push_back(0);
        }
        // If length is greater than 1, then create the attribute device pointer and record
        // the pitch size for 2D attributes
        else
        {
            // Add the attribute to the attribute map
            size_t pitch;
            dispatcher->template createAttribute<T>(DeviceConfig::ObjectType::AGENT, handle, length, numElements, defaultValue, &devPtr, &pitch);
            attributeTags.push_back(tag);
            attributeDevPtrs.push_back(devPtr);
            attributePitch.push_back(pitch);
        }

        logger::debug("Created attribute with tag %d devPtr is %p is null? %d", tag, devPtr, devPtr == nullptr);
    }

    template <typename T>
	inline T *Agents::downloadAttributes(int tag, unsigned int length){
		// Find the index of the attribute in the attributeTags vector
		std::vector<int>::iterator it = std::find(attributeTags.begin(), attributeTags.end(), tag);
		if (it == attributeTags.end())
		{
			// The attribute does not exist
			// Log the error and return
			logger::error("Attribute with tag %d does not exist in Agents handle %d", tag, handle);
			throw MassException("Attribute with tag %d does not exist in Agents handle %d", tag, handle);
		}

		// Get the index of the attribute in the attributeTags vector
		int index = std::distance(attributeTags.begin(), it);
		logger::debug("Agents downloading tag %d at index %d", tag, index);

		// Download the attribute from the device
		return dispatcher->template downloadAttributes<T>(attributeDevPtrs[index], attributePitch[index], length, numElements);
	}

} /* namespace mass */
