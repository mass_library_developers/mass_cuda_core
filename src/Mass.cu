
#include <time.h>
#include "Mass.h"
#include "Logger.h"

using namespace std;

namespace mass {

// static initialization
Dispatcher *Mass::dispatcher = new Dispatcher();
map<int, Places*> Mass::placesMap;
map<int, Agents*> Mass::agentsMap;

void Mass::init() {
	logger::debug("Initializing Mass");
	if (dispatcher == NULL) {
		dispatcher = new Dispatcher();
	}
	dispatcher->init();
}

void Mass::finish() {
	logger::debug("Finishing Mass");
	delete dispatcher;
	dispatcher = NULL;
}

Places *Mass::getPlaces(int handle) {
	return Mass::placesMap.find(handle)->second;
}

int Mass::getNumPlaces() {
	return Mass::placesMap.size();
}

} /* namespace mass */
