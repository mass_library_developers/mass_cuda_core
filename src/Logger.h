#pragma once

#include <ctime>
#include <stdarg.h>
#include <stdio.h>
#include <string>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/format.hpp>

namespace mass {
	namespace logger {
		// fmt_recurse is an overloaded version of its variadic counterpart to 
		// cover instances where there are no format arguments.
		inline std::string fmt_recurse(boost::format& message) {
			return message.str();
		}

		// fmt_recurse recursively adds the format strings to the message.
		template<typename V, typename... T>
		std::string fmt_recurse(boost::format& message, V&& arg, T&&... args) {
			message % std::forward<V>(arg);

			return fmt_recurse(message, std::forward<T>(args)...);
		}

		// sprintf constructs and returns a string from the provided format
		// string and string arguments.
		template<typename... T>
		std::string sprintf(const char* fmt, T&&... args) {
			boost::format message(fmt);

			return fmt_recurse(message, std::forward<T>(args)...);
		}

		// setLogLevel sets the global log level for the logger.
		static void setLogLevel(boost::log::trivial::severity_level log_level) {
			boost::log::core::get()->set_filter (
				boost::log::trivial::severity >= log_level
			);
		}

		// info logs an info level log message.
		template<typename... T>
		void info(std::string fmt, T&&... args) {
			BOOST_LOG_TRIVIAL(info) << sprintf(fmt.c_str(), args...);
		}

		// warn logs a warning level log message.
		template<typename... T>
		void warn(std::string fmt, T&&... args) {
			BOOST_LOG_TRIVIAL(warning) << sprintf(fmt.c_str(), args...);
		}

		// error logs an error level log message.
		template<typename... T>
		void error(std::string fmt, T&&... args) {
			BOOST_LOG_TRIVIAL(error) << sprintf(fmt.c_str(), args...);
		}

		// debug logs a debug
		template<typename... T>
		void debug(std::string fmt, T&&... args) {
			BOOST_LOG_TRIVIAL(debug) << sprintf(fmt.c_str(), args...);
		}
	};
};