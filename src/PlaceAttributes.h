#pragma once
#include <string>

extern __constant__ size_t nPlaces;

namespace mass
{
    // struct PlaceAttributeBase
    // {
    //     PlaceAttributeBase(void* devPtr, size_t typeSize, size_t length)
    //         : devPtr(devPtr), typeSize(typeSize), length(length) {}
        
    //     virtual ~PlaceAttributeBase() = default;
    //     void* devPtr;
    //     const size_t typeSize;
    //     const size_t length;
    // };

    // template <typename T>
    // struct PlaceAttribute : public PlaceAttributeBase
    // {
    //     typedef T type;
    // };

    enum PlacePreDefinedAttr {
            PLACE = 9000,            // Size: 1, Type: Place
			NEIGHBORS,		         // Size: MAX_NEIGHBORS, Type: int (Place Index)
            NEIGHBOR_PTRS,           // Size: MAX_NEIGHBORS, Type: Place * (Pointer to Place reflecting the index in NEIGHBORS)
			NEIGHBOR_OFFSETS,        // Size: MAX_NEIGHBORS, Type: int
			AGENTS,					 // Size: MAX_AGENTS, Type: Agent *
			AGENT_POPS,				 // Size: 1, Type: int
			POTENTIAL_AGENTS,		 // Size: MAX_POTENTIAL_AGENTS, Type: Agent *
			POTENTIAL_AGENT_POPS,	 // Size: 1, Type: int
		};
} // namespace mass
