#pragma once

#include <cuda_runtime.h>
#include <vector>
#include <queue>

#include "DeviceConfig.h"
#include "PlaceAttributes.h"
#include "cudaUtil.h"
#include "MassException.h"

namespace mass
{
	class Dispatcher
	{

	public:
		/**
		 *  Is the Dispatcher constructor.
		 *  The Dispatcher must be initialized prior to use.
		 */
		Dispatcher();

		/**
		 *  Is the Dispatcher initializer.
		 *  The Dispatcher locates the GPU, sets up communication links, and prepares to begin
		 *  dispatching data to and from the GPU.
		 */
		void init();

		~Dispatcher();

		/**************************************************************************
		 * These are the commands that will execute commands on places objects on
		 * the GPU.
		 **************************************************************************/

		/**
		 * Gets the pointer to the device memory of the Places object.
		 *
		 * @param handle the handle of the Places object.
		 */
		Place *getPlacesDevPtr(int placeHandle);

		/**
		 * Call the specified function on the specified places object with the given
		 * argument.
		 * @param handle the handle of the places upon which to call all
		 * @param functionId the ID of the user defined function
		 * @param argument the function argument. Can be NULL.
		 * @param argSize the size of argument in bytes. If argument == NULL, argSize is not used.
		 */
		void callAllPlaces(int placeHandle, int functionId, void *argument,
						   int argSize);

		/**
		 *  @brief This function causes all Place elements to exchange information about their neighbors.
		 *
		 *  The neighbors array in each of the places is populated with pointers to the Places in specified
		 *  in the destinations vector. The offsets to neighbors are defined in the destinations vector (a collection
		 *  of offsets from the caller to the callee place elements).
		 *
		 *  Example destinations vector:
		 *    vector<int*> destinations;<br>
		 *    int north[2] = {0, 1}; destinations.push_back( north );
		 *    int east[2] = {1, 0}; destinations.push_back( east );
		 *    int south[2] = {0, -1}; destinations.push_back( south );
		 *    int west[2] = {-1, 0}; destinations.push_back( west );
		 *
		 * @param handle The handle of the place object.
		 * @param destinations A vector of relative indexes of neighbors.
		 */
		void exchangeAllPlaces(int placeHandle, std::vector<int *> *destinations);

		/**
		 *  @brief This function causes all Place elements to exchange information about their neighbors and to call
		 *  the function specified with functionId on each of the places afterwards.
		 *
		 *  In addition to the fuctionality of the standard exchangeAllPlaces function specified above
		 *  it also takes functionId as a parameter and arguments to that function.
		 *  When the data is collected from the neighboring places,
		 *  the specified function is executed on all of the places with specified parameters.
		 *  The rationale behind implemening this version of exchangeAllPlaces is performance optimization:
		 *  the data cached during data collection step can be used for the data calculation and thus minimize
		 *  the number of memeory fetches and improve performance.
		 *
		 * @param handle The handle of the place object.
		 * @param destinations A vector of relative indexes of neighbors.
		 * @param functionId The ID of the user defined function.
		 * @param argument The function argument. Can be NULL.
		 * @param argSize The size of argument in bytes. If argument == NULL, argSize is not used.
		 */
		void exchangeAllPlaces(int handle, std::vector<int *> *destinations, int functionId,
							   void *argument, int argSize);

		/**
		 * @brief This function causes all Place elements to exchange information about their neighbors
		 * by using the neighborhood offset information stored in the PlaceState object.
		 *
		 * The difference between this function and the standard exchangeAllPlaces function is that
		 * the standard exchangeAllPlaces function requires the caller to specify the neighborhood releative
		 * indexes of neighbors, while this function uses the neighborhood offset information stored in the
		 * PlaceState object to calculate the neighborhood relative indexes of neighbors. Therefore, the
		 * `Place->NEIGHBOR_OFFSETS` attribute must be updated before calling this function.
		 *
		 * @param handle The handle of the place object.
		 * @param nNeighbors The number of neighbors.
		 */
		void exchangeAllPlaces(int handle, int nNeighbors);

		/**
		 * @brief This function causes all Place elements to exchange information about their neighbors
		 * by using the neighborhood offset information stored in the PlaceState object, and to call
		 * the function specified with functionId on each of the places afterwards.
		 *
		 * The difference between this function and the standard exchangeAllPlaces function is that
		 * the standard exchangeAllPlaces function requires the caller to specify the neighborhood releative
		 * indexes of neighbors, while this function uses the neighborhood offset information stored in the
		 * PlaceState object to calculate the neighborhood relative indexes of neighbors. Therefore, the
		 * `PlaceState->neighborOffsets` must be updated before calling this function.
		 *
		 * @param handle The handle of the place object.
		 * @param nNeighbors The number of neighbors.
		 * @param functionId The ID of the user defined function.
		 * @param argument The function argument. Can be NULL.
		 * @param argSize The size of argument in bytes. If argument == NULL, argSize is not used.
		 */
		void exchangeAllPlaces(int handle, int nNeighbors, int functionId, void *argument, int argSize);

		/**
		 * Call the specified function on the specified agents object with the given
		 * argument.
		 *
		 * @param handle the handle of the agents upon which to call all
		 * @param functionId the ID of the user defined function
		 * @param argument the function argument. Can be NULL.
		 * @param argSize the size of argument in bytes. If argument == NULL, argSize is not used.
		 */
		void callAllAgents(int agentHandle, int functionId, void *argument, int argSize);

		/**
		 * Call the specified function on the specified agents object with the given
		 * argument. In this version, the argument should already be
		 * on the device. We will not copy the argument to the device again.
		 *
		 * @param handle the handle of the agents upon which to call all
		 * @param functionId the ID of the user defined function
		 * @param argument the function argument. Can be NULL.
		 */
		void callAllAgents(int agentHandle, int functionId, void *argument);

		/** @brief Executed during manageAll() call on the collection of agents to complete the
		 * deallocation/memory management of the agents marked for termination.
		 *
		 * The agenet is marked as terminated by setting the `alive` attribute of the agent to false,
		 * and it's actually done during the agent's callAll() function. This function is called
		 * during the manageAll() function to reorganize the agent array (put alive agents at the
		 * beginning of the array and terminated agents at the end of the array) and update the
		 * `numAgents` and `nextIdx` attribute of the agent collection.
		 *
		 * @param agentHandle The handle of the agent collection.
		 */
		void terminateAgents(int agentHandle);

		/* Executed during manageAll() call on the collection of agents to complete the
		 * migration of the agents marked for migration.
		 */
		void migrateAgents(int agentHandle, int placeHandle);

		/* Executed during manageAll() call on the collection of agents to complete the
		 * agent spawning procedure for the agents that invoked the spawn() function.
		 */
		void spawnAgents(int agentHandle);

		/**
		 * @brief Get the number of alive agents in the agent collection on the device.
		 * Since we update the alive Agent on the device during the `manageAll()` process,
		 * the `deviceInfo->devAgentsMap[handle].nAgents` attribute represents the number of
		 * alive agents in the agent collection. We simply return this attribute.
		 *
		 * @param agentHandle The handle of the agent collection.
		 */
		int getNumAliveAgents(int agentHandle);

		/**
		 * @brief Get the number of total alive agents in the agent collection on the device.
		 * This attribute is updated during the `manageAll()` process.
		 * Therefore, this function is not expensive and can be used without caution.
		 *
		 * @param agentHandle The handle of the agent collection.
		 */
		int getNumMaxAgents(int agentHandle);

		/**
		 * Instantiates a new Places object (of your own type) on both the host and the device.
		 *
		 * @param handle The handle of the Places object.
		 * @param dimensions The number of dimensions of the place matrix.
		 * @param size An array contains the size of each dimension of the place matrix.
		 * @param qty The number of Place objects in this Places object.
		 * @param majorType The major type of the place matrix (e.g., ROW_MAJOR or COLUMN_MAJOR)
		 */
		template <typename P>
		void instantiatePlaces(int handle, int dimensions, int size[], int qty, int majorType);

		/**
		 * Instantiates a new Agents object (of your own type) on both the host and the device.
		 *
		 * @param handle The handle of the Agents object.
		 * @param nAgents The number of Agent to instantiate in this Agents instance.
		 * @param placesHandle The handle of the Places associated with this Agents instance.
		 * @param maxAgents The maximum number of Agent that can be created in this Agents instance at any time.
		 */
		template <typename AgentType>
		void instantiateAgents(int handle, int nAgents, int placesHandle, int maxAgents);

		/**
		 * Initializes the pre-defined attributes for the Place/Agent object.
		 * Values here are hard-coded and cannot be changed by the user. These
		 * variables are defined by us to be used in the simulation internally.
		 *
		 * @param objectType The type of the object (PLACE or AGENT).
		 * @param handle The handle of the object.
		 * @param placeIdxs An int array of size nAgents that contains the index of the Place where each Agent will be mapped.
		 */
		void initializePreDefinedAttributes(DeviceConfig::ObjectType objectType, int handle, int *placeIdxs = nullptr);

		/**
		 * Creates the attribute array on the device, and update
		 * the attributeDevPtrs to point to the device memory.
		 * This overload is used when the length of attribute is
		 * 1, and the attribute created is a 1D array.
		 *
		 * @param length The length of the attribute array (1 here).
		 * @param qty The number of elements in the attribute array.
		 * @param attributeDevPtrs The pointer to the device memory.
		 */
		template <typename T>
		void createAttribute(unsigned int length, unsigned int qty, void **attributeDevPtrs);

		/**
		 * Creates the attribute array on the device, and update
		 * the attributeDevPtrs to point to the device memory.
		 * This overload is used when the length of attribute is
		 * 1, and the attribute created is a 1D array.
		 * In addition, this overload sets the default value for
		 * the attribute.
		 *
		 * @param objectType The type of the object (PLACE or AGENT).
		 * @param handle The handle of the object.
		 * @param length The length of the attribute array (1 here).
		 * @param qty The number of elements in the attribute array.
		 * @param defaultValue The default value for the attribute.
		 * @param attributeDevPtrs The pointer to the device memory.
		 */
		template <typename T>
		void createAttribute(DeviceConfig::ObjectType objectType, int handle, unsigned int length, unsigned int qty, T defaultValue, void **attributeDevPtrs);

		/**
		 * Creates the attribute array on the device, and update
		 * the attributeDevPtrs to point to the device memory, and
		 * the pitch to be used when accessing the 2D array on the
		 * device.
		 * This overload is used when the length of attribute is
		 * greater than 1, and the attribute created is a 2D array.
		 *
		 * @param length The length of the attribute array (> 1 here).
		 * @param qty The number of elements in the attribute array.
		 * @param attributeDevPtrs The pointer to the device memory.
		 * @param pitch The pitch of the attribute array.
		 */
		template <typename T>
		void createAttribute(unsigned int length, unsigned int qty, void **attributeDevPtrs, size_t *pitch);

		/**
		 * Creates the attribute array on the device, and update
		 * the attributeDevPtrs to point to the device memory, and
		 * the pitch to be used when accessing the 2D array on the
		 * device.
		 * This overload is used when the length of attribute is
		 * greater than 1, and the attribute created is a 2D array.
		 * In addition, this overload sets the default value for
		 * the attribute.
		 *
		 * @param objectType The type of the object (PLACE or AGENT).
		 * @param handle The handle of the object.
		 * @param length The length of the attribute array (> 1 here).
		 * @param qty The number of elements in the attribute array.
		 * @param defaultValue The default value for the attribute.
		 * @param attributeDevPtrs The pointer to the device memory.
		 * @param pitch The pitch of the attribute array.
		 */
		template <typename T>
		void createAttribute(DeviceConfig::ObjectType objectType, int handle, unsigned int length, unsigned int qty, T defaultValue, void **attributeDevPtrs, size_t *pitch);

		/**
		 * Upload Place/Agent attributes data to the device.
		 *
		 * @param objectType The type of the object (PLACE or AGENT).
		 * 	Options: `DeviceConfig::PLACE` or `DeviceConfig::AGENT`.
		 * @param handle The handle of the Places instance.
		 * @param attributeTags Vector of attribute tags on the host.
		 * @param attributeDevPtrs Vector of pointers to attribute array in device memory.
		 * @param attributePitch Vector of pitch of the attribute array in device memory.
		 */
		void finalizeAttributes(DeviceConfig::ObjectType objectType, int handle, std::vector<int> *attributeTags, std::vector<void *> *attributeDevPtrs,
								std::vector<size_t> *attributePitch);

		template <typename T>
		/**
		 * Download Place/Agent attributes data from the device.
		 *
		 * @param d_attributeDevPtrs The pointer to the device memory.
		 * @param pitch The pitch of the attribute array.
		 * @param length The length of the attribute, 1 returns a 1D array, > 1 returns a 2D array.
		 * @param qty The number of elements in the attribute array.
		 * 
		 * @return The pointer to the host memory with the downloaded data.
		 */
		T *downloadAttributes(void *d_attributeDevPtrs, size_t pitch, unsigned int length, unsigned int qty);

	private:
		/**
		 * @brief This version is used to update the neighborhood
		 * of each place to the global variable `offsets_device` in
		 * Dispatcher.cu.
		 *
		 * This function first take a vector of relative indexes of neighbors
		 * as input, and then calculate the offset based on the place
		 * matrix size. The offset is then stored in the global variable
		 * `offsets_device` in Dispatcher.cu, which is used by the
		 * `exchangeAllPlacesKernel()` kernel function.
		 *
		 * @param handle The handle of the place object.
		 * @param vec A vector of relative indexes of neighbors.
		 *
		 * @return true if the neighborhood is updated successfully.
		 */
		bool updateNeighborhood(int handle, std::vector<int *> *vec);

		DeviceConfig *deviceInfo;

		bool initialized; // Whether or not the dispatcher has been initialized.
		bool deviceLoaded;

		std::vector<int *> *neighborhood; /* The previous vector of neighbors.*/

		// Keep track of the finalized Place attribute
	};
	// end class

	template <typename PlaceType>
	void Dispatcher::instantiatePlaces(int handle, int dimensions, int size[], int qty, int majorType)
	{
		logger::debug("Inside Dispatcher::instantiatePlaces");

		// Modify GPU data model
		deviceInfo->instantiatePlaces<PlaceType>(handle, dimensions, size, qty, majorType);
	}

	template <typename AgentType>
	void Dispatcher::instantiateAgents(int handle, int nAgents, int placesHandle, int maxAgents)
	{

		logger::debug("Inside Dispatcher::instantiateAgents");

		// Create GPU data model
		deviceInfo->instantiateAgents<AgentType>(handle, nAgents, placesHandle, maxAgents);
	}

	template <typename T>
	__global__ void setDefaultValueKernel(T *ptr, T defaultValue, int n)
	{
		int idx = threadIdx.x + blockIdx.x * blockDim.x;
		if (idx < n)
		{
			ptr[idx] = defaultValue;
		}
	}

	template <typename T>
	__global__ void setDefaultValue2DKernel(T *ptr, T defaultValue, int n, size_t pitch, int length)
	{
		int idx = threadIdx.x + blockIdx.x * blockDim.x;
		if (idx < n)
		{
			// Get the row of the 2D array
			T *row = (T *)((char *)ptr + idx * pitch);

			// Set the default value for each element in the row
			for (int i = 0; i < length; i++)
			{
				row[i] = defaultValue;
			}
		}
	}

	template <typename T>
	__host__ void Dispatcher::createAttribute(unsigned int length, unsigned int qty, void **attributeDevPtrs)
	{
		if (length <= 1)
		{
			T *d_attr = NULL;
			CATCH(cudaMalloc((void **)&d_attr, sizeof(T) * qty));
			*attributeDevPtrs = (void *)d_attr;
		}
	}

	template <typename T>
	__host__ void Dispatcher::createAttribute(
		DeviceConfig::ObjectType objectType, int handle, unsigned int length, unsigned int qty,
		T defaultValue, void **attributeDevPtrs)
	{
		if (length <= 1)
		{
			T *d_attr;

			CATCH(cudaMalloc((void **)&d_attr, sizeof(T) * qty));

			// Original way to set the default value
			// This will not work because cudaMemset only works for simple types
			// Complex types like class or struct will not work
			// CATCH(cudaMemset(d_attr, defaultValue, sizeof(T) * qty));

			// Get the number of blocks and threads
			dim3 thread, block = 0;
			if (objectType == DeviceConfig::PLACE)
			{
				dim3 *dims = deviceInfo->getPlacesKernelDims(handle);
				thread = dims[0];
				block = dims[1];
			}
			else if (objectType == DeviceConfig::AGENT)
			{
				dim3 *dims = deviceInfo->getAgentsKernelDims(handle);
				thread = dims[0];
				block = dims[1];
			}
			else
			{
				throw MassException("Invalid object type");
			}

			// Set the default value
			setDefaultValueKernel<T><<<block, thread>>>(d_attr, defaultValue, qty);

			*attributeDevPtrs = (void *)d_attr;
		}
	}

	template <typename T>
	__host__ void Dispatcher::createAttribute(unsigned int length, unsigned int qty, void **attributeDevPtrs, size_t *pitchIn)
	{
		if (length > 1)
		{
			T *d_attr = NULL;
			CATCH(cudaMallocPitch(&d_attr, pitchIn, length * sizeof(T), qty));
			*attributeDevPtrs = (void *)d_attr;
		}
	}

	template <typename T>
	__host__ void Dispatcher::createAttribute(
		DeviceConfig::ObjectType objectType, int handle, unsigned int length, unsigned int qty,
		T defaultValue, void **attributeDevPtrs, size_t *pitchIn)
	{
		if (length > 1)
		{
			T *d_attr = NULL;
			CATCH(cudaMallocPitch(&d_attr, pitchIn, length * sizeof(T), qty));

			// Get the number of blocks and threads
			dim3 thread, block = 0;
			if (objectType == DeviceConfig::PLACE)
			{
				dim3 *dims = deviceInfo->getPlacesKernelDims(handle);
				thread = dims[0];
				block = dims[1];
			}
			else if (objectType == DeviceConfig::AGENT)
			{
				dim3 *dims = deviceInfo->getAgentsKernelDims(handle);
				thread = dims[0];
				block = dims[1];
			}
			else
			{
				throw MassException("Invalid object type");
			}

			// Set the default value
			setDefaultValue2DKernel<T><<<block, thread>>>(d_attr, defaultValue, qty, *pitchIn, length);

			*attributeDevPtrs = (void *)d_attr;
		}
	}

	template <typename T>
	T *Dispatcher::downloadAttributes(void *d_attributeDevPtrs, size_t pitch, unsigned int length, unsigned int qty)
	{
		// If the attribute is a 1D array
		if (length <= 1)
		{
			T *h_attribute = new T[qty];
			CATCH(cudaMemcpy(h_attribute, d_attributeDevPtrs, sizeof(T) * qty, D2H));
			return h_attribute;
		}
		// If the attribute is a 2D array
		else
		{
			T *h_attribute = new T[qty * length];
			CATCH(cudaMemcpy2D(h_attribute, length * sizeof(T), d_attributeDevPtrs, pitch, length * sizeof(T), qty, D2H));
			return h_attribute;
		}
	}

} // namespace mass
