#pragma once

#include <cuda_runtime.h>
#include "settings.h"
#include "MassException.h"
#include "PlaceAttributes.h"
#include "Agent.h"

// easier for end users to understand than the __host__ __device__ meaning.
#define MASS_FUNCTION __host__ __device__

namespace mass
{
    class Agent;
    /**
     * Place v2 is a new version of the Place class that is intended to boost performance
     * by extracting the place state from the place class.
     * As a result, the Place v2 class does not contain the PlaceState object anymore.
     * But you can still get attributes from it using getAttribute() function. See
     * functions below.
     */
    class Place
    {
    public:
        /**
         * The constructor called on the GPU device.
         * The index of the Place is passed as an argument.
         *
         * @param index The index of the Place.
         */
        MASS_FUNCTION Place(int index);
        MASS_FUNCTION virtual ~Place() {}

        /**
         * Called by MASS while executing Places.callAll().
         * This is intended to be a switch statement where each user-implemented
         * function is mapped to a funcID, and is passed 'args' when called.
         *
         * @param functionId The ID of the function to be called.
         * @param arg        A pointer to the arguments passed to the function.
         */
        __device__ virtual void callMethod(int functionId, void *arg = NULL) = 0;

        /**
         * Returns the index of this Place.
         *
         * @return The index of this Place.
         */
        __device__ size_t getIndex() const;

        /**
         * Returns the number of `Place` object. Currently this info is stored
         * as a constant (and the only one) in the device, which we are assuming
         * there is only one `Places` that is created by users during the simulation.
         * If there will be multiple `Places` in the future, we will need to
         * maybe store this info in each `Place` object of each `Places` object.
         *
         * The way we do it now is to read the constant from the device. In this way,
         * we do not need to store the info in each `Place` object, which can save
         * a lot of memory.
         *
         * @return The number of `Place` object in a `Places` object.
         */
        __device__ size_t getNumPlaces() const;

        /**--------------------------------------------------------
         * Belows are getters for pre-defined attributes of Place.
         --------------------------------------------------------*/

        /**
         * Returns the neighbors' indices of this Place.
         * The length of the returned array is MAX_NEIGHBORS.
         * If MAX_NEIGHBORS is 1, an single integer is returned.
         * To access the single integer, use *(return value).
         * To access the array, use (return value)[i].
         * 
         * @return The neighbors' indices of this Place.
         */
        __device__ int *getNeighbors() const;

        /**
         * Returns the pointer to the neighbors of this Place.
         * The length of the returned array is MAX_NEIGHBORS.
         * If MAX_NEIGHBORS is 1, an single pointer is returned.
         * To access the single integer, use *(return value).
         * To access the array, use (return value)[i].
         * 
         * @return The pointer to the neighbors of this Place.
         */
        __device__ Place **getNeighborsPtr() const;

        /**
         * Returns the neighbors' relative offsets of this Place.
         * The length of the returned array is MAX_NEIGHBORS.
         * If MAX_NEIGHBORS is 1, an single integer is returned.
         * To access the single integer, use *(return value).
         * To access the array, use (return value)[i].
         * 
         * @return The neighbors' relative offsets of this Place.
         */
        __device__ int *getNeighborOffsets() const;

        /**
         * Returns the Agent residing in this Place.
         * The length of the returned array is MAX_AGENTS.
         * If MAX_AGENTS is 1, an single pointer is returned.
         * To access the single pointer, use *(return value).
         * To access the array, use (return value)[i].
         * 
         * @return The Agent residing in this Place.
         */
        __device__ Agent **getAgents() const;

        /**
         * Gets the number of Agents currently residing in this place.
         *
         * @return The number of Agents currently residing in this place.
         */
        __device__ size_t getAgentPopulation() const;

        /**
         * Returns the potential Agents that are going to migrate to this Place.
         * The length of the returned array is MAX_POTENTIAL_AGENTS.
         * If MAX_POTENTIAL_AGENTS is 1, an single pointer is returned.
         * To access the single pointer, use *(return value).
         * To access the array, use (return value)[i].
         * 
         * @return The potential Agents that are going to migrate to this Place.
        */
        __device__ Agent** getPotentialAgents() const;

        /**
         * Gets the number of potential Agents that are going to migrate to this place.
         * 
         * @return The number of potential Agents that are going to migrate to this place.
        */
        __device__ size_t getPotentialPopulation() const;

        /** ---------------------------------------------------
         * End of getters for pre-defined attributes of Place.
         ----------------------------------------------------*/

        /**
         * Get an attribute of `this Place` by attribute tag (name set by user when adding an attribute).
         * For more infomartion about the tag and length, please refer to `Places::setAttribute()`.
         *
         * @param tag The tag of the attribute.
         * @param length The length of the attribute (length set when adding an attribute).
         *
         * @return A pointer to the attribute, whether 1D or 2D.
         */
        template <typename T>
        __device__ T *getAttribute(int tag, int length) const;

        /**
         * Get an attribute of `another Place` by attribute tag (name set by user when adding an attribute).
         * For more infomartion about the tag and length, please refer to `Places::setAttribute()`.
         *
         * @param desIndex The index of the destination Place.
         * @param tag The tag of the attribute.
         * @param length The length of the attribute (length set when adding an attribute).
         *
         * @return A pointer to the attribute, whether 1D or 2D.
         */
        template <typename T>
        __device__ T *getAttribute(size_t desIndex, int tag, int length) const;

        /**
         * Update all attributes info. This function is being used when user call `Places::finalizeAttributes()`
         * where new attributes info will be updated from host to device, ans this function is used to update
         * the information of all attributes on device based on given info from host.
         *
         * @param attributeTags An int array that contains the tags of all attributes.
         * @param attributeDevPtrs A void pointer array that contains the device pointers of all attributes.
         * @param attributePitch A size_t array that contains the pitch of all attributes.
         * @param nAttributes The number of attributes.
         */
        __device__ void updateAllAttributes(int *attributeTags, void **attributeDevPtrs, size_t *attributePitch,
                                            int nAttributes);

        /**
         * Adds a resident Agent to this place. Returns true if the adding was successful.
         * Adding can fail if the place has reached its maximum occupancy.
         *
         * @param       agent The Agent to be added.
         * @return      True if the adding was successful, false otherwise.
         */
        __device__ bool addAgent(Agent *agent);

        /**
         * Removes the specified Agent from the array of resident Agents in this place.
         *
         * @param       agent The Agent to be removed.
         */
        __device__ bool removeAgent(Agent *agent);

        /**
         * This is a utility function used within the library.
         * Adds an agent to an array of agents who expressed an intention to
         * migrate to this place. The agents which will be officially accepted
         * for migration by a place during the Agents::manageAll() call.
         *
         * @param agent    The Agent to be added.
         *
         * @note This function "unofficially" adds an agent to the place.
         * It adds the agent to Place->POTENTIAL_AGENTS attribute, which will be
         * further procesed by Place::resolveMigrationConflicts() to determine
         * which Agent(s) will be "officially" added to the place by Place::addAgent().
         */
        __device__ void addMigratingAgent(Agent *agent);

        /**
         * The default migration conflict resolution algorithm selects the agent
         * with the lowest index, in case several agents intend to migrate to the
         * same place. This method can be overloaded in the derived Place class if
         * the application requires a different conflict resolution mechanism.
         *
         * We resolve conflicts by selecting the agent with the lowest index.
         * If more than 1 Agent can reside in a place, we select the first N Agents
         * with the lower index
         */
        __device__ virtual void resolveMigrationConflicts();

        /**
         * Clears all neighbors of this place.
         * Clears the neighbors, neighborOffsets, and neighborsPtr attributes.
         */
        __device__ void clearNeighbors();

        // The memory order to be used in all calculation
        enum MemoryOrder
        {
            COL_MAJOR,
            ROW_MAJOR
        };

    protected:
        size_t index;            // The index of this Place
        int nAttributes;         // The number of attributes of all Place objects
        int *attributeTags;      // The tags of all attributes
        void **attributeDevPtrs; // The device pointers of all attributes,
                                 // each pointer in this array points to the first element of the attribute
        size_t *attributePitch;  // The pitch of all attributes, if the attribute is a 2D array,
                                 // the pitch is recorded, otherwise, it is 0

        /**
         * Finds the index of an int value in an int array.
         *
         * @param intArr The int array to be searched.
         * @param n The length of the int array.
         * @param val The int value to be found.
         *
         * @return The index of the int value in the int array, or -1 if not found.
         */
        __device__ int find(int *intArr, int n, int val) const;
    };

    template <typename T>
    __device__ T *Place::getAttribute(int tag, int length) const
    {
        int i = find(attributeTags, nAttributes, tag);
        if (i == -1)
        {
            return NULL;
        }

        // If the length is greater than 0, then it is a 2D array
        // Otherwise, it is a 1D array

        // If it is a 2D array, we need to get the row first
        if (attributePitch[i] > 0)
        {
            // Get the row
            T *row = (T *)((char *)attributeDevPtrs[i] + index * attributePitch[i]);

            return row;
        }
        // If it is a 1D array, we can just return array[index]
        else
        {
            T *array = static_cast<T *>(attributeDevPtrs[i]);
            return &array[index];
        }
    }

    template <typename T>
    __device__ T *Place::getAttribute(size_t des_index, int tag, int length) const
    {
        int i = find(attributeTags, nAttributes, tag);
        if (i == -1)
        {
            return NULL;
        }

        // If the length is greater than 0, then it is a 2D array
        // Otherwise, it is a 1D array

        // If it is a 2D array, we need to get the row first
        if (attributePitch[i] > 0)
        {
            // Get the row
            T *row = (T *)((char *)attributeDevPtrs[i] + des_index * attributePitch[i]);

            return row;
        }
        // If it is a 1D array, we can just return array[index]
        else
        {
            T *array = static_cast<T *>(attributeDevPtrs[i]);
            return &array[des_index];
        }
    }
}