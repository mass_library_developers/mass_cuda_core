
#pragma once

#include <sstream>
#include <exception>
#include <string>

namespace mass
{

	/**
	 *  This is the standard exception to be thrown in the mass library.
	 *  At this point we assume that all MassExceptions are fatal, i.e.
	 *  the state cannot be recovered and the program using the library must
	 *  end (implication: we don't attempt to clear memory before failing.
	 *  reasoning: if we do, it leads to infinite loop of expetions in case
	 *  memory is corrupted)
	 */
	class MassException : public std::exception
	{
	public:
		template<typename... Args>
        MassException(const std::string &format, Args... args)
        {
            std::ostringstream ss;
            char buffer[256];
            snprintf(buffer, sizeof(buffer), format.c_str(), args...);
            ss << buffer;
            message = ss.str();
        }

		virtual const char *what() const noexcept override
		{
			return message.c_str();
		}

		virtual ~MassException() throw();

	private:
		std::string message;
	};

} /* namespace mass */
