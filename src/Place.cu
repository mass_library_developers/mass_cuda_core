#include "Place.h"

namespace mass
{
    MASS_FUNCTION Place::Place(int index)
        : index(index), nAttributes(0), attributeTags(NULL), attributeDevPtrs(NULL), attributePitch(NULL){};

    __device__ size_t Place::getIndex() const { return index; }

    __device__ size_t Place::getNumPlaces() const { return nPlaces; }

    __device__ void Place::updateAllAttributes(int *attributeTags, void **attributeDevPtrs, size_t *attributePitch, int nAttributes)
    {
        this->nAttributes = nAttributes;
        this->attributeTags = attributeTags;
        this->attributeDevPtrs = attributeDevPtrs;
        this->attributePitch = attributePitch;
    }

    __device__ int Place::find(int *intArr, int n, int val) const
    {
        for (int i = 0; i < n; i++)
        {
            if (intArr[i] == val)
            {
                return i;
            }
        }
        return -1;
    }

    __device__ bool Place::addAgent(Agent *agent)
    {
        // Get the attribute "AGENTS"
        // which is an array of pointers to Agent residing in this Place
        Agent **agents = getAttribute<Agent *>(PlacePreDefinedAttr::AGENTS, MAX_AGENTS);

        // Check if agent already exists here
        for (int i = 0; i < MAX_AGENTS; i++)
        {
            if (agents[i] == agent)
            {
                return false;
            }
        }

        // Init
        size_t newAgentPop = 0;
        bool added = false;

        for (int i = 0; i < MAX_AGENTS; i++)
        {
            // If the index of this attribute array is nullptr
            // it indicates that this index is empty,
            // where we can add the agent
            if (agents[i] == nullptr)
            {
                if (!added)
                {
                    agents[i] = agent;
                    newAgentPop++;
                    added = true;
                }
            }
            else
            {
                newAgentPop++;
            }
        }

        // Update the attribute "AGENT_POPS"
        size_t *agentPop = getAttribute<size_t>(PlacePreDefinedAttr::AGENT_POPS, 1);
        *agentPop = newAgentPop;

        return added;
    }

    __device__ bool Place::removeAgent(Agent *agent)
    {
        size_t newAgentPop = 0;
        bool removed = false;

        // Get the attribute "AGENTS"
        // which is an array of pointers to Agent residing in this Place
        Agent **agents = getAttribute<Agent *>(PlacePreDefinedAttr::AGENTS, MAX_AGENTS);

        for (int i = 0; i < MAX_AGENTS; i++)
        {
            // If the index of this attribute array is not nullptr
            // it indicates that this index is not empty
            if (agents[i] != nullptr)
            {
                newAgentPop++;

                // Check if it is the agent
                if (agents[i] == agent)
                {
                    // Remove the agent
                    agents[i] = nullptr;
                    removed = true;
                    newAgentPop--;
                }
            }
        }

        // Update the attribute "AGENT_POPS"
        size_t *agentPop = getAttribute<size_t>(PlacePreDefinedAttr::AGENT_POPS, 1);
        *agentPop = newAgentPop;

        return removed;
    }

    __device__ int *Place::getNeighbors() const
    {
        return getAttribute<int>(PlacePreDefinedAttr::NEIGHBORS, MAX_NEIGHBORS);
    }
    __device__ Place **Place::getNeighborsPtr() const
    {
        return getAttribute<Place *>(PlacePreDefinedAttr::NEIGHBOR_PTRS, MAX_NEIGHBORS);
    }
    __device__ int *Place::getNeighborOffsets() const
    {
        return getAttribute<int>(PlacePreDefinedAttr::NEIGHBOR_OFFSETS, MAX_NEIGHBORS);
    }
    __device__ Agent **Place::getAgents() const
    {
        return getAttribute<Agent *>(PlacePreDefinedAttr::AGENTS, MAX_AGENTS);
    }

    __device__ size_t Place::getAgentPopulation() const
    {
        return *(getAttribute<size_t>(PlacePreDefinedAttr::AGENT_POPS, 1));
    }

    __device__ Agent **Place::getPotentialAgents() const
    {
        return getAttribute<Agent *>(PlacePreDefinedAttr::POTENTIAL_AGENTS, MAX_POTENTIAL_AGENTS);
    }
    __device__ size_t Place::getPotentialPopulation() const
    {
        return *(getAttribute<size_t>(PlacePreDefinedAttr::POTENTIAL_AGENT_POPS, 1));
    }

    __device__ void Place::addMigratingAgent(Agent *agent)
    {
        // Get the attribute "POTENTIAL_AGENT_POPS"
        int *potentialAgentPops = getAttribute<int>(PlacePreDefinedAttr::POTENTIAL_AGENT_POPS, 1);

        // If the potentialAgentPops is full, we no longer accept new agents
        if (*potentialAgentPops >= MAX_POTENTIAL_AGENTS)
        {
            return;
        }

        // In case this function will be called by multiple Place (threads)
        // at the same time in the kernel, we need to use atomicAdd to avoid race condition
        int index = atomicAdd(potentialAgentPops, 1);

        // Get the attribute "POTENTIAL_AGENTS"
        // which is an array of pointers to Agent expected to migrate to this Place
        Agent **potentialAgents = getAttribute<Agent *>(PlacePreDefinedAttr::POTENTIAL_AGENTS, MAX_POTENTIAL_AGENTS);

        // Add the agent to the index
        potentialAgents[index] = agent;
    }

    __device__ void Place::resolveMigrationConflicts()
    {
        // The real algorithm is simply sort the potentialNextAgents array and
        // select the first N agents
        // @note by Warren: currently using the simply insertion sort
        // which is efficient for small array
        // if we design to allow large number of agents to reside in a place
        // we should use more efficient sorting algorithm

        // Get the attribute "POTENTIAL_AGENTS"
        // which is an array of pointers to Agent expected to migrate to this Place
        Agent **potentialNextAgents = getAttribute<Agent *>(PlacePreDefinedAttr::POTENTIAL_AGENTS, MAX_POTENTIAL_AGENTS);

        // Loop through the potentialNextAgents array and sort it
        for (int i = 1; i < MAX_POTENTIAL_AGENTS; i++)
        {
            Agent *key = potentialNextAgents[i];
            int j = i - 1;

            while (j >= 0 && (potentialNextAgents[j] == nullptr ||
                              (key != nullptr && potentialNextAgents[j]->getIndex() > key->getIndex())))
            {
                potentialNextAgents[j + 1] = potentialNextAgents[j];
                j = j - 1;
            }
            potentialNextAgents[j + 1] = key;
        }

        // If more than 1 Agent can reside in a place, we select the fist N Agents
        // with the lower index
        // If only 1 agent can reside in a place
        // we select the agent with the lowest index
        for (int i = 0; i < MAX_AGENTS; i++)
        {
            if (potentialNextAgents[i] != nullptr)
            {
                addAgent(potentialNextAgents[i]);
            }
            else
            {
                break; // Early termination if no more agents
            }
        }

        // printf("Place %d clearing potentialNextAgents\n", state->index);
        // Clean potentialNextAgents array
        for (int i = 0; i < MAX_POTENTIAL_AGENTS; i++)
        {
            potentialNextAgents[i] = nullptr;
        }
        // Reset POTENTIAL_AGENT_POPS
        size_t *potentialAgentPops = getAttribute<size_t>(PlacePreDefinedAttr::POTENTIAL_AGENT_POPS, 1);
        *potentialAgentPops = 0;
    }

    __device__ void Place::clearNeighbors()
    {
        // Get attribute NEIGHBORS and NEIGHBOR_OFFSETS
        int *neighbors = getAttribute<int>(PlacePreDefinedAttr::NEIGHBORS, MAX_NEIGHBORS);
        int *neighborOffsets = getAttribute<int>(PlacePreDefinedAttr::NEIGHBOR_OFFSETS, MAX_NEIGHBORS);
        Place **neighborPtrs = getAttribute<Place *>(PlacePreDefinedAttr::NEIGHBOR_PTRS, MAX_NEIGHBORS);

        for (int i = 0; i < MAX_NEIGHBORS; i++)
        {
            neighbors[i] = -1;
            neighborOffsets[i] = 0;
            neighborPtrs[i] = nullptr;
        }
    }

}