.SILENT: ;

# Project Variables
PROJECT_DIR			:= $(shell pwd)
BUILD_DIR			= $(PROJECT_DIR)/build
LIB_DIR				= $(PROJECT_DIR)/lib
BIN_DIR				= $(PROJECT_DIR)/bin

# MASS build include/lib
MASS_INCLUDE		= -I$(PROJECT_DIR)/src
MASS_LIB			= $(LIB_DIR)/mass/*.a

# MASS install include/lib locations
LIBS_DIR			= $(PROJECT_DIR)/libs
INCLUDE_DIR			= $(PROJECT_DIR)/include

# CUDA Variables
CUDA_ROOT_DIR		= /usr/local/cuda
CUDA_LIB_DIR		= -L$(CUDA_ROOT_DIR)/lib64 -L/usr/local/cuda/samples/common/lib/linux/x86_64
CUDA_INCLUDE_DIR	= -I$(CUDA_ROOT_DIR)/include
NVCC_FLAGS			= -Wno-deprecated-gpu-targets -rdc=true -std=c++14 -DAGENT_BLOCK_SIZE=512 -DPLACE_BLOCK_SIZE=512

# Boost
BOOST_VERSION		= 1.84.0
BOOST_TAG			= $(shell echo -n $(BOOST_VERSION) | tr . _)
BOOST_INCLUDE		= -isystem $(LIB_DIR)/boost-$(BOOST_VERSION)/include # Include boost as a system include directory (suppresses warnings)
BOOST_LIB			= $(LIB_DIR)/boost-$(BOOST_VERSION)/lib/*.a

# GoogleTest Variables
GOOGLE_TEST_VERSION	= 1.14.0
TEST_LIB_DIR		= -L$(LIB_DIR)/gtest/lib64
TEST_INCLUDE_DIR	= -I$(LIB_DIR)/gtest/include

build::
	echo "Building MASS library..."
	mkdir -p $(BUILD_DIR)/mass
	mkdir -p $(LIB_DIR)/mass
	cd $(BUILD_DIR)/mass && nvcc $(NVCC_FLAGS) $(NVCC_OPTS) $(CUDA_INCLUDE_DIR) $(BOOST_INCLUDE) -c $(PROJECT_DIR)/src/*.c*
	ar ru $(LIB_DIR)/mass/mass_cuda.a build/mass/*.o
	ranlib $(LIB_DIR)/mass/mass_cuda.a
	echo "MASS library build complete."

clean::
	rm -rf build
	rm -rf bin
	rm -rf lib
	rm -rf test/lib

develop:: clean install-google-test install-boost build
	mkdir -p build
	mkdir -p bin
	mkdir -p lib

install:: clean install-google-test install-boost build test
	mkdir -p $(LIBS_DIR)
	mkdir -p $(INCLUDE_DIR)/mass
	cp $(LIB_DIR)/mass/mass_cuda.a $(LIBS_DIR)/
	cp src/*.h $(INCLUDE_DIR)/mass
	cp -R $(LIB_DIR)/boost-$(BOOST_VERSION)/lib/* $(LIBS_DIR)/
	cp -R $(LIB_DIR)/boost-$(BOOST_VERSION)/include/boost $(INCLUDE_DIR)/boost
	make clean

install-boost:
	mkdir -p $(BIN_DIR)
	mkdir -p $(LIB_DIR)
	mkdir -p $(BUILD_DIR)
	rm -rf $(LIB_DIR)/boost && mkdir -p $(LIB_DIR)/boost-$(BOOST_VERSION)
	rm -rf $(BUILD_DIR)/boost && mkdir -p $(BUILD_DIR)/boost
	cd $(BUILD_DIR)/boost && wget -O boost-$(BOOST_VERSION).tgz https://boostorg.jfrog.io/artifactory/main/release/$(BOOST_VERSION)/source/boost_$(BOOST_TAG).tar.gz
	cd $(BUILD_DIR)/boost && tar -zxvf boost-$(BOOST_VERSION).tgz
	rm $(BUILD_DIR)/boost/boost-$(BOOST_VERSION).tgz
	cd $(BUILD_DIR)/boost/boost_$(BOOST_TAG) && ./bootstrap.sh --prefix=$(LIB_DIR)/boost-$(BOOST_VERSION) --with-libraries=program_options,log
	cd $(BUILD_DIR)/boost/boost_$(BOOST_TAG) && ./b2 install
	rm -rf $(BUILD_DIR)/boost

install-google-test:
	mkdir -p $(BUILD_DIR)
	rm -rf $(BUILD_DIR)/gtest
	mkdir -p $(BUILD_DIR)/gtest
	cd $(BUILD_DIR)/gtest && git clone https://github.com/google/googletest.git -b v$(GOOGLE_TEST_VERSION)
	mkdir -p $(BUILD_DIR)/gtest/googletest/build
	cd $(BUILD_DIR)/gtest/googletest/build && cmake -DCMAKE_INSTALL_PREFIX="$(LIB_DIR)/gtest" -DCMAKE_BUILD_TYPE=Release ..
	cd $(BUILD_DIR)/gtest/googletest/build && make && make install
	rm -rf $(BUILD_DIR)/gtest

test::
	echo "Building test..."
	mkdir -p $(BUILD_DIR)/test
	cd $(BUILD_DIR)/test && nvcc $(NVCC_FLAGS) $(NVCC_OPTS) $(CUDA_INCLUDE_DIR) $(TEST_INCLUDE_DIR) $(MASS_INCLUDE) $(BOOST_INCLUDE) -c `find $(PROJECT_DIR)/test -name "*.c*"`
	nvcc $(NVCC_FLAGS) -lcurand -lgtest $(CUDA_LIB_DIR) $(TEST_LIB_DIR) $(MASS_LIB) $(BOOST_LIB) build/test/*.o -o bin/test
	./bin/test $(TEST_PARAMS)
