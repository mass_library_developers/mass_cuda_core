#pragma once

#include "Agent.h"
#include "Place.h"

class TestAgent : public mass::Agent
{
    // Forwards declaration
    // class Place;
public:
    MASS_FUNCTION TestAgent(int index) : mass::Agent(index) {}
    MASS_FUNCTION ~TestAgent() {}

    enum FUNCTION_ID
    {
        PRINT_RESIDE_PLACE_IDX = 0,
        CHECK_RESIDE_PLACE_IDX,

        SET_INT_1,
        CHECK_INT_1,

        CHECK_CHAR_1,

        SET_INT_2D,
        CHECK_INT_2D,

        CHECK_CHAR_2D,

        CHECK_INIT_10_AGENT_PLACE,
        TEST_MIGRATE_2_PLACE_FIRST_NEIGHBOR,
        CHECK_MIGRATION_STATUS,

        KILL_LAST_9_AGENTS,
        CHECK_TERMINATION_STATUS,

        SPAWN_AGENTS_TOP,
        KILL_ROW_AGENTS,
        CHECK_SPAWN_KILL_STATUS,

        CHECK_MIGRATE_TO_SAME_PLACE_STATUS,

        KILL_EVEN_AGENTS,
    };

    enum ATTRIBUTE_ID
    {
        INT_1 = 0,
        CHAR_1,
        INT_2D,
        CHAR_2D
    };

    __device__ virtual void callMethod(int functionId, void *arg = NULL)
    {
        switch (functionId)
        {
        case PRINT_RESIDE_PLACE_IDX:
            printResidePlaceIdx();
            break;
        case CHECK_RESIDE_PLACE_IDX:
            checkResidePlaceIdx((int *)arg);
            break;
        case SET_INT_1:
            setInt1((int *)arg);
            break;
        case CHECK_INT_1:
            checkInt1((int *)arg);
            break;
        case CHECK_CHAR_1:
            checkChar1((char *)arg);
            break;
        case SET_INT_2D:
            setInt2D((int *)arg);
            break;
        case CHECK_INT_2D:
            checkInt2D((int *)arg);
            break;
        case CHECK_CHAR_2D:
            checkChar2D((char *)arg);
            break;
        case CHECK_INIT_10_AGENT_PLACE:
            testInit10AgentPlace();
            break;
        case TEST_MIGRATE_2_PLACE_FIRST_NEIGHBOR:
            migrate2FirstNeighbor();
            break;
        case CHECK_MIGRATION_STATUS:
            checkMigrationStatus((int *)arg);
            break;
        case KILL_LAST_9_AGENTS:
            killLast9Agents();
            break;
        case CHECK_TERMINATION_STATUS:
            checkTerminationStatus();
            break;
        case SPAWN_AGENTS_TOP:
            spawnAgentsToTop();
            break;
        case KILL_ROW_AGENTS:
            killRowAgents((int *)arg);
            break;
        case CHECK_SPAWN_KILL_STATUS:
            checkSpawnAndKillStatus((int *)arg);
            break;
        case CHECK_MIGRATE_TO_SAME_PLACE_STATUS:
            checkMigration2SamePlaceStatus();
            break;
        case KILL_EVEN_AGENTS:
            killEvenAgents();
            break;
        default:
            break;
        }
    }

    __device__ void printResidePlaceIdx()
    {
        size_t index = getIndex();
        mass::Place *residePlace = getPlace();

        printf("Agent %lu resides in place %lu\n", index, residePlace->getIndex());
    }

    __device__ void checkResidePlaceIdx(int *placeSize)
    {
        size_t index = getIndex();
        mass::Place *residePlace = getPlace();
        assert(residePlace->getIndex() == index % *placeSize);
    }

    __device__ void setInt1(int *value)
    {
        int index = getIndex();

        // Get attribute INT_1
        int *int1 = getAttribute<int>(INT_1, 1);

        // Set value
        *int1 = *value;
    }

    __device__ void checkInt1(int *value)
    {
        int index = getIndex();

        // Get attribute INT_1
        int *int1 = getAttribute<int>(INT_1, 1);

        // Check value
        assert(*int1 == *value);
    }

    __device__ void checkChar1(char *value)
    {
        char index = getIndex();

        // Get attribute CHAR_1
        char *char1 = getAttribute<char>(CHAR_1, 1);

        // Check value
        assert(*char1 == *value);
    }

    __device__ void setInt2D(int *length)
    {
        int index = getIndex();

        // Get attribute INT_2D
        int *int2D = getAttribute<int>(INT_2D, *length);

        // Set value
        for (int i = 0; i < *length; i++)
        {
            int2D[i] = i;
        }
    }

    __device__ void checkInt2D(int *length)
    {
        int index = getIndex();

        // Get attribute INT_2D
        int *int2D = getAttribute<int>(INT_2D, *length);

        // Check value
        for (int i = 0; i < *length; i++)
        {
            assert(int2D[i] == i);
        }
    }

    __device__ void checkChar2D(char *length)
    {
        int index = getIndex();

        // Get attribute CHAR_2D
        char *char2D = getAttribute<char>(CHAR_2D, *length);

        // Check value
        for (int i = 0; i < *length; i++)
        {
            assert(char2D[i] == 'W');
        }
    }

    __device__ void testInit10AgentPlace()
    {
        int index = getIndex();
        mass::Place *residePlace = getPlace();
        assert(residePlace->getIndex() == index);
    }

    __device__ void migrate2FirstNeighbor()
    {
        size_t index = getIndex();

        // Get the reside place attribute
        mass::Place *residePlace = getPlace();
        // printf("Agent %lu resides in place %lu\n", index, (*residePlace)->getIndex());

        // Get the first element of the reside Place's neighbor,
        // this is where we want to migrate to
        mass::Place **neighborsPtr = residePlace->getNeighborsPtr();

        // printf("Agent %lu migrates to place %lu - %p\n", index, places[neighbors[0]].getIndex(), &places[neighbors[0]]);

        // Migrate to the first neighbor
        migrate(neighborsPtr[0]);
    }

    __device__ void checkMigrationStatus(int *step)
    {
        // Step should be the number of migration we have performed in the test
        // For example, for Agent 0
        // Step 0: Agent 0 resides in place 0
        // Step 1: Agent 0 migrates to place 10 (0 + 10)
        // Step 2: Agent 0 migrates to place 20 (10 + 10)
        // ...

        size_t index = getIndex();
        mass::Place *residePlace = getPlace();

        // Check if the agent has been migrated to the bottom place
        assert(residePlace->getIndex() == index + *step * 10);

        // We also need to check if the Agent is in the Resident Agent list of the Place
        Agent **residentAgents = residePlace->getAgents();

        if (MAX_AGENTS == 1)
        {
            assert(*residentAgents == this);
        }
        else
        {
            bool found = false;
            for (int i = 0; i < MAX_AGENTS; i++)
            {
                if (residentAgents[i] == this)
                {
                    found = true;
                    break;
                }
            }
            assert(found);
        }
    }

    __device__ void killLast9Agents()
    {
        size_t index = getIndex();

        if (index > 0)
        {
            terminate();
        }
    }

    __device__ void checkTerminationStatus()
    {
        size_t index = getIndex();

        if (index == 0)
        {
            assert(isAlive());
        }
        else
        {
            assert(!isAlive());
        }
    }

    __device__ void spawnAgentsToTop()
    {
        size_t index = getIndex();

        // Get the reside Place's (should be the top in this test) resident Agent list
        // If the Agent list is empty,
        // then we can spawn a new Agent to the top

        // Get the current reside Place
        mass::Place *residePlace = getPlace();

        // Get reside Place's neighbor index list
        int *neighbors = residePlace->getNeighbors();
        // Get reside Place's neighbor ptr list
        mass::Place **neighborsPtr = residePlace->getNeighborsPtr();

        // In this test, we know the top Place is the first element in the neighbor list
        // Get the top Place's resident Agent population
        size_t *topPlaceResidentAgentPopulation = getAttribute<size_t>(neighbors[0], mass::PlacePreDefinedAttr::AGENT_POPS, 1);

        // printf(
        //     "Agent %lu resides in place %lu, top place is %d, agent pop: %lu\n",
        //     index, residePlace->getIndex(), neighbors[0], *topPlaceResidentAgentPopulation
        // );

        // If the top Place is empty, spawn an Agent to the top Place
        if (*topPlaceResidentAgentPopulation == 0)
        {
            spawn(1, neighborsPtr[0]);
        }
    }

    __device__ void killRowAgents(int *row)
    {
        size_t index = getIndex();

        // Get the reside Place
        mass::Place *residePlace = getPlace();
        if (residePlace)
        {
            // Get the index of the reside Place
            size_t placeIdx = residePlace->getIndex();

            // If the Agent is in the same row as the input row, terminate
            if (placeIdx / 10 == *row)
            {
                terminate();
            }
        }
    }

    __device__ void checkSpawnAndKillStatus(int *row)
    {
        // Get the reside Place
        mass::Place *residePlace = getPlace();

        // If this Agent has reside Place
        if (residePlace)
        {
            // Get Agents reside in the Place
            Agent **residentAgents = residePlace->getAgents();
            // The Place should has the Agent
            assert(*residentAgents == this);

            // Get the index of the reside Place
            size_t placeIdx = residePlace->getIndex();
            // The Agent should be in the input row -1 (0-based index)
            assert(placeIdx / 10 == *row - 1);
        }
    }

    __device__ void checkMigration2SamePlaceStatus(){
        /**
         * Belows are hard-coded values for the test
        */
        size_t index = getIndex();
        mass::Place *residePlace = getPlace();

        if (index == 0)
        {
            assert(residePlace->getIndex() == 4);
        }
        else if (index == 1)
        {
            assert(residePlace->getIndex() == 1);
        }
        else if (index == 2)
        {
            assert(residePlace->getIndex() == 2);
        }
        else if (index == 3)
        {
            assert(residePlace->getIndex() == 3);
        }
        else if (index == 4)
        {
            assert(residePlace->getIndex() == 5);
        }
        else if (index == 5)
        {
            assert(residePlace->getIndex() == 6);
        }
        else if (index == 6)
        {
            assert(residePlace->getIndex() == 7);
        }
        else if (index == 7)
        {
            assert(residePlace->getIndex() == 8);
        }
        else{
            assert(false);
        }
    }

    __device__ void killEvenAgents(){
        size_t index = getIndex();

        if (index % 2 == 0)
        {
            terminate();
        }
    }
};