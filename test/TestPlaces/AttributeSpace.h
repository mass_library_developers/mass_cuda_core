#pragma once

#include "Place.h"

class AttributeSpacePlaceV2 : public mass::Place
{
public:
    MASS_FUNCTION AttributeSpacePlaceV2(int index) : Place(index){};
    MASS_FUNCTION ~AttributeSpacePlaceV2() {}

    __device__ virtual void callMethod(int functionId, void *arg = NULL)
    {
        switch (functionId)
        {
        case PRINT_ATTR:
            printAttr();
            break;
        case PRINT_NEXT_PLACE_ATTR:
            printNextPlaceAttr();
            break;
        case SET_AGENT_POP:
            setAgentPop();
            break;
        case CHECK_AGENT_POP:
            checkAgentPop();
            break;
        case SET_BOOL_1:
            setBool1Attribute();
            break;
        case CHECK_BOOL_1:
            checkBool1Attribute();
            break;
        case CHECK_CHAR_1:
            checkChar1Attribute((char *)arg);
            break;
        case SET_INT_2D:
            setInt2DAttribute((int *)arg);
            break;
        case CHECK_INT_2D:
            checkInt2DAttribute((int *)arg);
            break;
        case CHECK_CHAR_2D:
            checkChar2DAttribute((int *)arg);
            break;
        default:
            break;
        }
    }

    enum FUNCTION_ID
    {
        PRINT_ATTR,
        PRINT_NEXT_PLACE_ATTR,

        SET_AGENT_POP,
        CHECK_AGENT_POP,

        SET_BOOL_1,
        CHECK_BOOL_1,

        CHECK_CHAR_1,

        SET_INT_2D,
        CHECK_INT_2D,

        CHECK_CHAR_2D,
    };

    enum ATTR_ID
    {
        BOOL_1,
        CHAR_1,
        INT_2D,
        CHAR_2D,
    };

    __device__ void printAttr()
    {
        int index = getIndex();
        /*
        template <typename T>
        // Get an attribute by attribute tag
        __device__ T *getAttribute(int tag, int length) const
        */
        int *agentPop = getAttribute<int>(mass::PlacePreDefinedAttr::AGENT_POPS, 1);

        printf("Place index: %d, Agent population: %d\n", index, *agentPop);
    }

    __device__ void printNextPlaceAttr()
    {
        int index = getIndex();
        int nextPlaceIndex = index + 1;
        size_t totalPlace = getNumPlaces();

        if (nextPlaceIndex < totalPlace)
        {
            int *agentPop = getAttribute<int>(nextPlaceIndex, mass::PlacePreDefinedAttr::AGENT_POPS, 1);
            printf("Index: %d, printing next place: %d, Agent population: %d\n", index, nextPlaceIndex, *agentPop);
        }
        else
        {
            printf("Index: %d, next place index: %d, out of bound\n", index, nextPlaceIndex);
        }
    }

    __device__ void setAgentPop()
    {
        int index = getIndex();
        int *agentPop = getAttribute<int>(mass::PlacePreDefinedAttr::AGENT_POPS, 1);
        *agentPop = index;
    }

    __device__ void checkAgentPop()
    {
        int index = getIndex();
        int *agentPop = getAttribute<int>(mass::PlacePreDefinedAttr::AGENT_POPS, 1);
        assert(*agentPop == index);
    }

    __device__ void setBool1Attribute()
    {
        int index = getIndex();

        // Get the attribute BOOL_1
        bool *bool1 = getAttribute<bool>(BOOL_1, 1);

        // Set the attribute BOOL_1
        if (index % 2 == 0)
        {
            *bool1 = true;
        }
        else
        {
            *bool1 = false;
        }
    }

    __device__ void checkBool1Attribute()
    {
        int index = getIndex();

        // Get the attribute BOOL_1
        bool *bool1 = getAttribute<bool>(BOOL_1, 1);

        // Check the attribute BOOL_1
        if (index % 2 == 0)
        {
            assert(*bool1 == true);
        }
        else
        {
            assert(*bool1 == false);
        }
    }

    __device__ void checkChar1Attribute(char *char1)
    {
        int index = getIndex();

        // Get the attribute CHAR_1
        char *char1Attr = getAttribute<char>(CHAR_1, 1);

        // Check the attribute CHAR_1
        assert(*char1Attr == *char1);

        // printf("Index: %d, CHAR_1: %c\n", index, *char1Attr);
    }

    __device__ void setInt2DAttribute(int *length)
    {
        int index = getIndex();

        // Get the attribute INT_2D
        int *int2D = getAttribute<int>(INT_2D, *length);

        // Set the attribute INT_2D
        for (int i = 0; i < *length; i++)
        {
            int2D[i] = index + i;
        }
    }

    __device__ void checkInt2DAttribute(int *length)
    {
        int index = getIndex();

        // Get the attribute INT_2D
        int *int2D = getAttribute<int>(INT_2D, *length);

        // Check the attribute INT_2D
        for (int i = 0; i < *length; i++)
        {
            assert(int2D[i] == index + i);
        }
    }

    __device__ void checkChar2DAttribute(int *length)
    {
        int index = getIndex();

        // Get the attribute CHAR_2D
        char *char2D = getAttribute<char>(CHAR_2D, *length);

        // Check the attribute CHAR_2D
        for (int i = 0; i < *length; i++)
        {
            assert(char2D[i] == 'W');

            // printf("Index: %d, CHAR_2D: %c\n", index, char2D[i]);
        }
    }
};
