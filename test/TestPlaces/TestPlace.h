#pragma once

#include "Place.h"

class TestPlace : public mass::Place
{
public:
	MASS_FUNCTION TestPlace(int index) : mass::Place(index) {};
	MASS_FUNCTION ~TestPlace() {};

	enum FINCTION_ID
	{
		CHECK_INDEX,
		CHECK_INDEX_PLUS,
		CHECK_NEIGHBOR_COUNT,
		CLEAR_NEIGHBORS,

		SET_NEIGHBOR_OFFSET,
		CHECK_NEIGHBOR_COUNT_LOCAL_OFFSETS,
		PRINT_NEIGHBOR_OFFSET,

		PRINT_RESIDE_AGENTS,

		SET_CENTER_PLACE_AS_NEIGHBOR,

		SET_POTENTIAL_AGENT_POPS,
	};

	enum ATTRIBUTE_ID
	{
		ATTR
	};

	__device__ virtual void callMethod(int functionId, void *arg = NULL);

private:
	// Used to test callAll without args
	__device__ void checkIndex();
	// Used to test callAll with args
	__device__ void checkIndexPlus(void *);

	// Used to set neighbor offsets
	__device__ void setNeighborOffset();
	__device__ void setNeighborOffsetCenterPlace();
	// Used to clear neighbors
	__device__ void clearNeighborhood();
	// Used to print neighbor offsets
	__device__ void printNeighborOffset();

	// Used to test exchangeAll - check neighbor count after exchange
	__device__ void checkNeighborCount();
	// Used to test exchangeAll - check neighbor count after exchange with local offsets
	__device__ void checkNeighborCountLocalOffsets();
	
	// Used to print reside agents
	__device__ void printResideAgents();

	// Used to set potential agent pops
	__device__ void setPotentialAgentPops();
};
