#include <stdio.h> //remove after debugging
#include <assert.h>

#include "cudaUtil.h"
#include "TestPlace.h"
// #include "TestPlaces/TestPlace.h"

using namespace std;
using namespace mass;

__device__ void TestPlace::callMethod(int functionId, void *arg)
{
	switch (functionId)
	{
	case CHECK_INDEX:
		checkIndex();
		break;
	case CHECK_INDEX_PLUS:
		checkIndexPlus(arg);
		break;
	case SET_NEIGHBOR_OFFSET:
		setNeighborOffset();
		break;
	case CLEAR_NEIGHBORS:
		clearNeighborhood();
		break;
	case PRINT_NEIGHBOR_OFFSET:
		printNeighborOffset();
		break;
	case CHECK_NEIGHBOR_COUNT:
		checkNeighborCount();
		break;
	case CHECK_NEIGHBOR_COUNT_LOCAL_OFFSETS:
		checkNeighborCountLocalOffsets();
		break;
	case PRINT_RESIDE_AGENTS:
		printResideAgents();
		break;
	case SET_CENTER_PLACE_AS_NEIGHBOR:
		setNeighborOffsetCenterPlace();
		break;
	case SET_POTENTIAL_AGENT_POPS:
		setPotentialAgentPops();
		break;
	default:
		break;
	}
}

__device__ void TestPlace::checkIndex()
{
	// Get place index
	size_t index = getIndex();

	// Get thread index
	int idx = getGlobalIdx_1D_1D();

	assert(idx == index);
}

__device__ void TestPlace::checkIndexPlus(void *n)
{
	// Cast n to int
	int cn = *(int *)n;

	// Get place index
	int index = getIndex();

	// Get thread index
	int idx = getGlobalIdx_1D_1D();

	assert(idx + cn == index + cn);
}

__device__ void TestPlace::setNeighborOffset()
{
	// Get place index
	int index = getIndex();

	// Get attribute NEIGHBOR_OFFSETS
	int *neighborOffsets = getNeighborOffsets();

	// If index is at even place, only have right neighbor
	if (index % 2 == 0)
	{
		neighborOffsets[0] = 1;
	}
	// If index is at odd place, only have left neighbor
	else
	{
		neighborOffsets[0] = -1;
	}
}

__device__ void TestPlace::setNeighborOffsetCenterPlace()
{
	/**
	 * This is a special function
	 * to migrate every Agent to the center Place
	 * for the specific test case.
	 * All index are hard-coded for the test case.
	 */
	// Get place index
	int index = getIndex();

	// Get attribute NEIGHBOR_OFFSETS
	int *neighborOffsets = getNeighborOffsets();

	if (index == 0)
	{
		neighborOffsets[0] = 4;
	}
	else if (index == 1)
	{
		neighborOffsets[0] = 3;
	}
	else if (index == 2)
	{
		neighborOffsets[0] = 2;
	}
	else if (index == 3)
	{
		neighborOffsets[0] = 1;
	}
	else if (index == 5)
	{
		neighborOffsets[0] = -1;
	}
	else if (index == 6)
	{
		neighborOffsets[0] = -2;
	}
	else if (index == 7)
	{
		neighborOffsets[0] = -3;
	}
	else if (index == 8)
	{
		neighborOffsets[0] = -4;
	}
	else
	{
		neighborOffsets[0] = 0;
	}
}

__device__ void TestPlace::clearNeighborhood()
{
	clearNeighbors();
}

__device__ void TestPlace::printNeighborOffset()
{
	// Get place index
	int index = getIndex();

	// Get attribute NEIGHBOR_OFFSETS
	int *neighborOffsets = getNeighborOffsets();

	// Hard-coded for the test case
	printf("Place %d has neighbor offset [%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d]\n",
		   index,
		   neighborOffsets[0], neighborOffsets[1], neighborOffsets[2],
		   neighborOffsets[3], neighborOffsets[4], neighborOffsets[5],
		   neighborOffsets[6], neighborOffsets[7], neighborOffsets[8],
		   neighborOffsets[9], neighborOffsets[10], neighborOffsets[11]);
}

__device__ void TestPlace::checkNeighborCount()
{
	// Get place index
	int index = getIndex();

	// Get attribute NEIGHBORS
	int *neighbors = getNeighbors();

	int count = 0;

	for (int i = 0; i < MAX_NEIGHBORS; i++)
	{
		if (neighbors[i] != -1)
		{
			count++;
		}
	}

	// printf("checkNeighborCount: Place %d has %d neighbors (Block %d, Thread %d)\n", index, count, blockIdx.x, threadIdx.x);

	if (index == 0 || index == getNumPlaces() - 1)
	{
		assert(count == 1);
	}
	else
	{
		assert(count == 2);
	}
}

__device__ void TestPlace::checkNeighborCountLocalOffsets()
{
	// Get place index
	int index = getIndex();

	// Get attribute NEIGHBORS
	int *neighbors = getNeighbors();

	int count = 0;
	for (int i = 0; i < MAX_NEIGHBORS; i++)
	{
		if (neighbors[i] != -1)
		{
			count++;
		}
	}

	int neighborIndex = neighbors[0];
	int neighborIndexNext = neighbors[1];

	assert(count == 1);
	if (index % 2 == 0)
	{
		assert(neighborIndex == index + 1);
	}
	else
	{
		assert(neighborIndex == index - 1);
	}
	assert(neighborIndexNext == -1);
}

__device__ void TestPlace::printResideAgents()
{
	// Get place index
	size_t index = getIndex();

	// Get attribute RESIDE_AGENTS
	Agent **resideAgents = getAgents();

	if (MAX_AGENTS == 1)
	{
		if (*resideAgents != NULL)
		{
			printf("Place %lu has Agent %lu - Agent Status [%d] - Agent linked to this Place [%d]\n", index, (*resideAgents)->getIndex(), (*resideAgents)->isAlive(), (*resideAgents)->getPlace() == this);
		}
		else
		{
			printf("Place %lu has no Agent\n", index);
		}
	}
	else
	{
		for (int i = 0; i < MAX_AGENTS; i++)
		{
			if (resideAgents[i] != NULL)
			{
				printf("Place %lu has Agent %lu - Agent Status [%d] - Agent linked to this Place [%d]\n", index, (*resideAgents)->getIndex(), (*resideAgents)->isAlive(), (*resideAgents)->getPlace() == this);
			}
			else
			{
				printf("Place %lu has no Agent @ Agent array %d\n", index, i);
			}
		}
	}
}

__device__ void TestPlace::setPotentialAgentPops()
{
	// Get place index
	size_t index = getIndex();

	// Get attribute POTENTIAL_AGENT_POPS
	size_t *potentialAgentPops = getAttribute<size_t>(mass::PlacePreDefinedAttr::POTENTIAL_AGENT_POPS, 1);

	*potentialAgentPops = index;
}
