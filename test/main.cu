#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <gtest/gtest.h>

#include "Mass.h"
#include "TestPlaces/TestPlace.h"
#include "TestPlaces/AttributeSpace.h"
#include "TestAgents/TestAgent.h"

using namespace std;

TEST(MASS_Misc, relativeIndexToOffset)
{
    /**
     * Test the essential function: relativeIndexToOffset of Places
     */

    // 1D
    // Row major
    int nDims = 1;
    int *dimensions = new int[10]{};
    int *relativeIndex = new int[nDims]{-1};
    int offset;

    for (int i = 0; i < 10; i++)
    {
        relativeIndex[0] = i;
        int expectedOffset = i;
        offset = mass::Places::relativeIndexToOffset(relativeIndex, nDims, dimensions, mass::Place::ROW_MAJOR);
        ASSERT_EQ(offset, expectedOffset);

        int j = i * -1;
        relativeIndex[0] = j;
        expectedOffset = j;
        offset = mass::Places::relativeIndexToOffset(relativeIndex, nDims, dimensions, mass::Place::ROW_MAJOR);
        ASSERT_EQ(offset, expectedOffset);
    }

    // Column major
    for (int i = 0; i < 10; i++)
    {
        relativeIndex[0] = i;
        int expectedOffset = i;
        offset = mass::Places::relativeIndexToOffset(relativeIndex, nDims, dimensions, mass::Place::COL_MAJOR);
        ASSERT_EQ(offset, expectedOffset);

        int j = i * -1;
        relativeIndex[0] = j;
        expectedOffset = j;
        offset = mass::Places::relativeIndexToOffset(relativeIndex, nDims, dimensions, mass::Place::COL_MAJOR);
        ASSERT_EQ(offset, expectedOffset);
    }

    delete[] dimensions;
    delete[] relativeIndex;

    // 2D
    // Row major
    nDims = 2;
    int *dimensions2D = new int[2]{10, 10};

    int *relativeIndex2D = new int[nDims]{0, 1}; // right
    int expectedOffset = 1;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{0, -1}; // left
    expectedOffset = -1;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{-1, 0}; // up
    expectedOffset = -10;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{1, 0}; // down
    expectedOffset = 10;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{-1, -1}; // up left
    expectedOffset = -11;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{-1, 1}; // up right
    expectedOffset = -9;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{1, -1}; // down left
    expectedOffset = 9;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{1, 1}; // down right
    expectedOffset = 11;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::ROW_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    // Column major
    relativeIndex2D = new int[nDims]{1, 0}; // right
    expectedOffset = 1;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{-1, 0}; // left
    expectedOffset = -1;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{0, -1}; // up
    expectedOffset = -10;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{0, 1}; // down
    expectedOffset = 10;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{-1, -1}; // up left
    expectedOffset = -11;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{1, -1}; // up right
    expectedOffset = -9;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{-1, 1}; // down left
    expectedOffset = 9;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    relativeIndex2D = new int[nDims]{1, 1}; // down right
    expectedOffset = 11;
    offset = mass::Places::relativeIndexToOffset(relativeIndex2D, nDims, dimensions2D, mass::Place::COL_MAJOR);
    ASSERT_EQ(offset, expectedOffset);

    delete[] dimensions2D;
    delete[] relativeIndex2D;
}

TEST(MASS_Places, EssentialFunctions)
{
    /**
     * Test the essential function: callAll and exchangeAll of PlaceV2
     */
    int size = 10;
    int nDims = 2;
    int placesSize[] = {size, size};

    mass::Mass::init();

    // Test createPlaces
    mass::Places *places = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    /* Test callAll */

    // Test callAll without args
    places->callAll(TestPlace::CHECK_INDEX);

    // Test callAll with args
    int n = 1;
    places->callAll(TestPlace::CHECK_INDEX_PLUS, &n, sizeof(int));

    /* Test exchangeAll */
    vector<int *> d;
    int left[2] = {0, -1};
    int right[2] = {0, 1};
    d.push_back(left);
    d.push_back(right);

    // // Test exchangeAll without function
    places->exchangeAll(&d);
    places->callAll(TestPlace::CHECK_NEIGHBOR_COUNT);
    mass::logger::debug("Finished callAll without function");

    // // Clear neighbors information
    places->callAll(TestPlace::CLEAR_NEIGHBORS);
    mass::logger::debug("Cleared neighbors");

    // Test exchangeAll with function
    places->exchangeAll(&d, TestPlace::CHECK_NEIGHBOR_COUNT);
    mass::logger::debug("Finished callAll with function");

    // Clear neighbors information
    places->callAll(TestPlace::CLEAR_NEIGHBORS);
    mass::logger::debug("Cleared neighbors");

    // Test exchangeAll using local offsets without function
    places->callAll(TestPlace::SET_NEIGHBOR_OFFSET);
    places->exchangeAll(1);
    places->callAll(TestPlace::CHECK_NEIGHBOR_COUNT_LOCAL_OFFSETS);
    mass::logger::debug("Finished callAll with local offsets");

    // Clear neighbors information
    places->callAll(TestPlace::CLEAR_NEIGHBORS);
    mass::logger::debug("Cleared neighbors");

    // Test exchangeAll using local offsets with function
    places->callAll(TestPlace::SET_NEIGHBOR_OFFSET);
    places->exchangeAll(1, TestPlace::CHECK_NEIGHBOR_COUNT_LOCAL_OFFSETS);
    mass::logger::debug("Finished callAll with local offsets and function");

    mass::Mass::finish();
}

TEST(MASS_Places, Attributes)
{
    /**
     * Test the setAttribute function of PlaceV2
     */
    int size = 100;
    int nDims = 2;
    int placesSize[] = {size, size};

    mass::Mass::init();

    // Initialize places
    mass::Places *places = mass::Mass::createPlaces<AttributeSpacePlaceV2>(
        0, nDims, placesSize, mass::Place::ROW_MAJOR);

    // Only for debugging
    const std::vector<int> &tags = places->getAttributeTags();
    const std::vector<void *> &devPtrs = places->getAttributeDevPtrs();
    const std::vector<size_t> &pitch = places->getAttributePitch();

    for (int i = 0; i < tags.size(); i++)
    {
        mass::logger::debug("Tag: %d, devPtr: %p, pitch: %d", tags[i], devPtrs[i], pitch[i]);
    }
    // End of debugging

    // Test the pre-defined attributes (AGENT_POPS)
    places->callAll(AttributeSpacePlaceV2::SET_AGENT_POP);
    // places->callAll(AttributeSpacePlaceV2::PRINT_NEXT_PLACE_ATTR);
    places->callAll(AttributeSpacePlaceV2::CHECK_AGENT_POP);

    // Test the user-defined attributes
    // 1. Without default value, set value manually
    places->setAttribute<bool>(AttributeSpacePlaceV2::BOOL_1, 1);
    places->finalizeAttributes();
    places->callAll(AttributeSpacePlaceV2::SET_BOOL_1);
    places->callAll(AttributeSpacePlaceV2::CHECK_BOOL_1);

    // 2. With default value
    char c = 'W';
    places->setAttribute<char>(AttributeSpacePlaceV2::CHAR_1, 1, c);
    places->finalizeAttributes();
    places->callAll(AttributeSpacePlaceV2::CHECK_CHAR_1, &c, sizeof(char));

    // 3. 2D attribute array (attribute length > 1), without default value, set value manually
    int int2dLength = 5;
    places->setAttribute<int>(AttributeSpacePlaceV2::INT_2D, int2dLength);
    places->finalizeAttributes();
    places->callAll(AttributeSpacePlaceV2::SET_INT_2D, &int2dLength, sizeof(int));
    places->callAll(AttributeSpacePlaceV2::CHECK_INT_2D, &int2dLength, sizeof(int));

    // 4. 2D attribute array (attribute length > 1), with default value
    places->setAttribute<char>(AttributeSpacePlaceV2::CHAR_2D, int2dLength, c);
    places->finalizeAttributes();
    places->callAll(AttributeSpacePlaceV2::CHECK_CHAR_2D, &int2dLength, sizeof(int));

    mass::Mass::finish();
}

TEST(MASS_Agents, CreateAgents)
{
    /**
     * Test the createAgents and callAll of AgentV2
     *
     * MASS is initialized and terminated in each test case because
     * we currently only allow one Agent residing in one Place at res1 time.
     * To avoid the conflict between test cases, we create res1 new MASS instance for each test case.
     */

    int size = 10;
    int nDims = 2;
    int placesSize[] = {size, size};
    int totalPlace = size * size;

    /**
     * Test createAgents using Places's handle
     */
    // Test createAgents where the number of Agent equals the number of Place
    mass::Mass::init();
    // Create Places
    mass::Places *places = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    // Test createAgents using Places's handle
    mass::Agents *agents = mass::Mass::createAgents<TestAgent>(
        0, size * size, places->getHandle());
    // Print each Agent's resident Place's index
    agents->callAll(TestAgent::CHECK_RESIDE_PLACE_IDX, &totalPlace, sizeof(int));

    mass::Mass::finish();

    // Test createAgents where the number of Agent is greater than the number of Place
    mass::Mass::init();
    // Create Places
    mass::Places *places_2 = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    mass::Agents *agents_more = mass::Mass::createAgents<TestAgent>(
        0, size * size * 2, places_2->getHandle());
    // Print each Agent's resident Place's index
    agents_more->callAll(TestAgent::CHECK_RESIDE_PLACE_IDX, &totalPlace, sizeof(int));

    mass::Mass::finish();

    // Test createAgents where the number of Agent is less than the number of Place
    mass::Mass::init();
    // Create Places
    mass::Places *places_3 = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    mass::Agents *agents_less = mass::Mass::createAgents<TestAgent>(
        0, size * size / 2, places_3->getHandle());
    // Print each Agent's resident Place's index
    agents_less->callAll(TestAgent::CHECK_RESIDE_PLACE_IDX, &totalPlace, sizeof(int));

    mass::Mass::finish();

    /**
     * Test createAgents using index array
     */
    // When the number of Agent equals the number of Place
    mass::Mass::init();
    // Create Places
    mass::Places *places_4 = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    // Create Agents using index array
    int *indexArray = new int[totalPlace];
    for (int i = 0; i < totalPlace; i++)
    {
        indexArray[i] = i;
    }

    mass::Agents *agents_4 = mass::Mass::createAgents<TestAgent>(
        0, totalPlace, places_4->getHandle(), 0, indexArray);
    // Print each Agent's resident Place's index
    agents_4->callAll(TestAgent::CHECK_RESIDE_PLACE_IDX, &totalPlace, sizeof(int));

    free(indexArray);
    mass::Mass::finish();

    // When the number of Agent is greater than the number of Place
    mass::Mass::init();
    // Create Places
    mass::Places *places_5 = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    // Create Agents using index array
    int *indexArray_2 = new int[totalPlace * 2];
    for (int i = 0; i < totalPlace * 2; i++)
    {
        indexArray_2[i] = i % (totalPlace);
    }
    mass::Agents *agents_5 = mass::Mass::createAgents<TestAgent>(
        0, totalPlace * 2, places_5->getHandle(), 0, indexArray_2);

    // Print each Agent's resident Place's index
    // agents_5->callAll(TestAgent::PRINT_RESIDE_PLACE_IDX);
    // cout << "Total Place: " << totalPlace << endl;
    agents_5->callAll(TestAgent::CHECK_RESIDE_PLACE_IDX, &totalPlace, sizeof(int));

    free(indexArray_2);
    mass::Mass::finish();
}

TEST(MASS_Agents, Attributes)
{
    /**
     * Test attributes related functions of AgentV2
     */
    int size = 10;
    int nDims = 2;
    int placesSize[] = {size, size};

    mass::Mass::init();

    // Create Places
    mass::Places *places = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    // Create Agents
    mass::Agents *agents = mass::Mass::createAgents<TestAgent>(
        0, size * size, places->getHandle());

    // Test set attribute

    // 1. Without default value, set value manually
    agents->setAttribute<int>(TestAgent::INT_1, 1);
    agents->finalizeAttributes();
    int value = 1;
    agents->callAll(TestAgent::SET_INT_1, &value, sizeof(int));
    agents->callAll(TestAgent::CHECK_INT_1, &value, sizeof(int));

    // 2. With default value
    char c = 'W';
    agents->setAttribute<char>(TestAgent::CHAR_1, 1, c);
    agents->finalizeAttributes();
    agents->callAll(TestAgent::CHECK_CHAR_1, &c, sizeof(char));

    // 3. 2D attribute array (attribute length > 1), without default value, set value manually
    int int2dLength = 5;
    agents->setAttribute<int>(TestAgent::INT_2D, int2dLength);
    agents->finalizeAttributes();
    agents->callAll(TestAgent::SET_INT_2D, &int2dLength, sizeof(int));
    agents->callAll(TestAgent::CHECK_INT_2D, &int2dLength, sizeof(int));

    // 4. 2D attribute array (attribute length > 1), with default value
    agents->setAttribute<char>(TestAgent::CHAR_2D, int2dLength, c);
    agents->finalizeAttributes();
    agents->callAll(TestAgent::CHECK_CHAR_2D, &int2dLength, sizeof(int));

    mass::Mass::finish();
}

TEST(MASS_Agents, ManageAll)
{
    /**
     * Test the manageAll function of AgentV2
     * Including Agent kill/migrate/spawn
     */
    int size = 10;
    int nDims = 2;
    int placesSize[] = {size, size};

    mass::Mass::init();

    // Create Places
    mass::Places *places = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    // Create Agents with 10 Agent, they should be placed @ the first 10 Places
    mass::Agents *agents = mass::Mass::createAgents<TestAgent>(
        0, size, places->getHandle());

    // Check if the Agents are placed correctly
    agents->callAll(TestAgent::CHECK_INIT_10_AGENT_PLACE);

    /**
     * Migrate each Agent to the south (bottom) Place.
     * Since we have res1 10x10 Places, the index of Place the Agent resides in
     * should be increased by 10.
     * And we can perform this operation 9 times
     *
     * To perform this operation, we first need to set the bottom Place
     * of each Place as the neighbor of the current Place. So that we
     * can use this information to migrate the Agent to the bottom Place.
     */

    // Set the bottom Place of each Place as the neighbor of the current Place
    vector<int *> d;
    int bottom[2] = {1, 0}; // Row-major: [next row, same column]
    d.push_back(bottom);
    // Set to Places
    places->exchangeAll(&d);

    // Perform this operation 9 times
    for (int step = 1; step < 10; step++)
    {
        // Use Agents.callAll to migrate each Agent to the bottom Place
        agents->callAll(TestAgent::TEST_MIGRATE_2_PLACE_FIRST_NEIGHBOR);
        agents->manageAll();

        // Check if the Agents are migrated correctly
        agents->callAll(TestAgent::CHECK_MIGRATION_STATUS, &step, sizeof(int));

        // cout << "Migration - step " << step << " passed" << endl;
    }

    /**
     * END of Migrate
     */

    /**
     * Test terminate/spawn Agent
     * Although we have  dispatcher->terminateAgents() in Agents::manageAll()
     * This function is actually empty.
     * Because Agent.kill is res1 very simple operation, it can be execute within
     * the callAll() function, no need to have res1 specific kernel function for it.
     */

    // We firstly update the neighbor info of Place
    // so that its neighbor info can be used to spawn new Agents
    d.clear();
    int top[2] = {-1, 0}; // Row-major: [previous row, same column]
    d.push_back(top);
    // Set to Places
    places->exchangeAll(&d);

    for (int i = 9; i > 0; i--)
    {
        // For every alive Agent, spawn res1 new Agent to the top Place
        agents->callAll(TestAgent::SPAWN_AGENTS_TOP);
        agents->manageAll();
        // places->callAll(TestPlace::PRINT_RESIDE_AGENTS);

        // We now should have 2 rows of Agents residing in the Places
        // Terminate the second row of Agents
        agents->callAll(TestAgent::KILL_ROW_AGENTS, &i, sizeof(int));
        agents->manageAll();
        // places->callAll(TestPlace::PRINT_RESIDE_AGENTS);

        // Check if the Agents are spawned/killed correctly
        agents->callAll(TestAgent::CHECK_SPAWN_KILL_STATUS, &i, sizeof(int));

        // cout << "Spawn/Kill - step " << i << " passed" << endl;
    }

    /**
     * END of terminate/spawn Agent
     */

    mass::Mass::finish();
}

TEST(MASS_Agents, ResolveMigrationConflicts)
{
    /**
     * Test the resolveMigrationConflicts function of AgentV2
     * The current algorithm: if multiple Agents want to migrate to the same Place,
     * we only allow MAX_POTENTIAL_AGENTS Agent to migrate to that Place.
     * MAX_POTENTIAL_AGENTS is defined by user while compiling MASS.
     */

    // Create res1 3x3 Places for testing
    int size = 3;
    int nDims = 2;
    int placesSize[] = {size, size};

    mass::Mass::init();

    // Create Places
    mass::Places *places = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    /**
     * Create 8 Agents, and place them in the surrounding Places like this:
     * 1 1 1
     * 1 0 1
     * 1 1 1
     */
    int *placeIdx = new int[8]{0, 1, 2, 3, 5, 6, 7, 8};
    mass::Agents *agents = mass::Mass::createAgents<TestAgent>(
        0, (size * size - 1), places->getHandle(), size * size, placeIdx);

    // Use this specific function to set the center Place as the neighbor of all other Places
    places->callAll(TestPlace::SET_CENTER_PLACE_AS_NEIGHBOR);
    places->exchangeAll(1);

    // places->callAll(TestPlace::PRINT_RESIDE_AGENTS);

    // We do this 10 times to make sure the resolveMigrationConflicts function works correctly
    for (int i = 0; i < 10; i++)
    {
        // Use Agents.callAll to migrate each Agent to the bottom Place
        agents->callAll(TestAgent::TEST_MIGRATE_2_PLACE_FIRST_NEIGHBOR);
        agents->manageAll();

        // places->callAll(TestPlace::PRINT_RESIDE_AGENTS);
        agents->callAll(TestAgent::CHECK_MIGRATE_TO_SAME_PLACE_STATUS);
    }

    mass::Mass::finish();
}

TEST(MASS_Places, DownloadAttributes)
{
    /**
     * Test the downloadAttributes function of Places.
     */
    int size = 10;
    int nDims = 2;
    int placesSize[] = {size, size};

    mass::Mass::init();

    // Create Places
    mass::Places *places = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    /**
     * Test excption
     * Test downloadAttributes with invalid tag
     */

    try
    {
        places->downloadAttributes<int>(2111, 1);
    }
    catch (const std::exception &e)
    {
        // mass::logger::debug("Caught exception: %s", e.what());
        assert(strcmp(e.what(), "Attribute with tag 2111 does not exist in Places handle 0") == 0);
    }

    /**
     * Test 1D attribute array (attribute length = 1)
     */

    // Download the default attribute POTENTIAL_AGENT_POPS
    size_t *res1 = places->downloadAttributes<size_t>(mass::PlacePreDefinedAttr::POTENTIAL_AGENT_POPS, 1);
    // Validate the result
    for (int i = 0; i < size * size; i++)
    {
        assert(res1[i] == 0);
    }
    free(res1);

    // Use Places.callAll() to update the POTENTIAL_AGENT_POPS attribute
    places->callAll(TestPlace::SET_POTENTIAL_AGENT_POPS);

    size_t *res2 = places->downloadAttributes<size_t>(mass::PlacePreDefinedAttr::POTENTIAL_AGENT_POPS, 1);
    for (int i = 0; i < size * size; i++)
    {
        assert(res2[i] == i);
    }
    free(res2);

    /**
     * Test 2D attribute array (attribute length > 1)
     */

    // Download the default attribute NEIGHBOR_OFFSETS
    int *res3 = places->downloadAttributes<int>(mass::PlacePreDefinedAttr::NEIGHBOR_OFFSETS, MAX_NEIGHBORS);

    // Validate the result
    for (int i = 0; i < size * size; i++)
    {
        for (int j = 0; j < MAX_NEIGHBORS; j++)
        {
            assert(res3[i * MAX_NEIGHBORS + j] == 0);
        }
    }
    free(res3);

    // Use Places.callAll() to update the NEIGHBOR_OFFSETS attribute
    places->callAll(TestPlace::SET_NEIGHBOR_OFFSET);

    int *res4 = places->downloadAttributes<int>(mass::PlacePreDefinedAttr::NEIGHBOR_OFFSETS, MAX_NEIGHBORS);

    // Validate the result
    for (int i = 0; i < size * size; i++)
    {
        for (int j = 0; j < MAX_NEIGHBORS; j++)
        {
            if (j == 0)
            {
                if (i % 2 == 0)
                {
                    assert(res4[i * MAX_NEIGHBORS + j] == 1);
                }
                else
                {
                    assert(res4[i * MAX_NEIGHBORS + j] == -1);
                }
            }
        }
    }
    free(res4);

    mass::Mass::finish();
}

TEST(MASS_Agents, DownloadAttributes)
{
    /**
     * Test the downloadAttributes function of Agents.
     */
    int size = 10;
    int nDims = 2;
    int placesSize[] = {size, size};

    mass::Mass::init();

    // Create Places
    mass::Places *places = mass::Mass::createPlaces<TestPlace>(
        0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

    // Create Agents
    mass::Agents *agents = mass::Mass::createAgents<TestAgent>(
        0, size * size, places->getHandle());

    /**
     * Test excption
     * Test downloadAttributes with invalid tag
     */

    try
    {
        agents->downloadAttributes<int>(2111, 1);
    }
    catch (const std::exception &e)
    {
        // mass::logger::debug("Caught exception: %s", e.what());
        assert(strcmp(e.what(), "Attribute with tag 2111 does not exist in Agents handle 0") == 0);
    }

    /**
     * Test 1D attribute array (attribute length = 1)
     */

    // Download the default attribute IS_ALIVE
    bool *res1 = agents->downloadAttributes<bool>(mass::AgentPreDefinedAttr::IS_ALIVE, 1);
    // Validate the result
    for (int i = 0; i < size * size; i++)
    {
        assert(res1[i] == 1);
    }
    free(res1);

    // Use Agents.callAll() to update the IS_ALIVE attribute
    agents->callAll(TestAgent::KILL_EVEN_AGENTS);
    agents->manageAll();

    bool *res2 = agents->downloadAttributes<bool>(mass::AgentPreDefinedAttr::IS_ALIVE, 1);
    for (int i = 0; i < size * size; i++)
    {
        if (i % 2 == 0)
            assert(res2[i] == 0);
        else
            assert(res2[i] == 1);
    }
    free(res2);

    /**
     * Test 2D attribute array (attribute length > 1)
     * We don't have default 2D attribute array for Agents
     * So we will test with user-defined attribute
     */

    // Create an attribute INT_2D with length = 5 and default value = -1
    int attr_len = 5;
    agents->setAttribute<int>(TestAgent::INT_2D, attr_len, -1);
    agents->finalizeAttributes();

    // Download the default attribute INT_2D
    int *res3 = agents->downloadAttributes<int>(TestAgent::INT_2D, attr_len);

    // Validate the result
    for (int i = 0; i < size * size; i++)
    {
        for (int j = 0; j < attr_len; j++)
        {
            assert(res3[i * attr_len + j] == -1);
        }
    }
    free(res3);

    // Use Places.callAll() to update the NEIGHBOR_OFFSETS attribute
    agents->callAll(TestAgent::SET_INT_2D, &attr_len, sizeof(int));

    int *res4 = agents->downloadAttributes<int>(TestAgent::INT_2D, attr_len);

    // Validate the result
    // Case if kind of special here:
    // We have 100 Agents, and even Agents were killed before
    // So only 50 odd Agents are alive, and they will be updated the INT_2D attribute
    for (int i = 0; i < size * size; i++)
    {
        for (int j = 0; j < attr_len; j++)
        {
            if (i % 2 == 0){
                assert(res4[i * attr_len + j] == -1);
            }
            else{
                assert(res4[i * attr_len + j] == j);
            }
        }
    }
    free(res4);

    mass::Mass::finish();
}

int main(int argc, char **argv)
{
    mass::logger::setLogLevel(boost::log::trivial::info);
    testing::InitGoogleTest(&argc, argv);
    // testing::FLAGS_gtest_filter = "MASS_Agents.ManageAll";

    return RUN_ALL_TESTS();
}
