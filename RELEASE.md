We follow a manual git-flow release process which includes the following steps.


```bash
# Update main and develop branches
git checkout main
git pull
git checkout develop
git pull

# Checkout new release branch
git checkout -b release/v<XX.YY.ZZ>

# Optional: Merge work from other branches you worked on into the release branch
# For example, you worked on agents and places branches
git merge --no-ff agents
git merge --no-ff places

# Optional: add and commit any README updates related to the release.

# Merge release to main
git checkout main
git merge --no-ff release/v<XX.YY.ZZ>

# Tag release
git tag -a v<XX.YY.ZZ> -am "release message".

# Merge release to develop
git checkout develop
git merge --no-ff release/v<XX.YY.ZZ>

# Finished with release branch, push it for posterity, or optionally delete it.
# To delete
git branch -d release/v<XX.YY.ZZ>
# To push
git push origin release/v<XX.YY.ZZ>

# Update remote
git push origin main
git push origin develop
git push --tags
```